# ![alt text](https://framagit.org/AlicVB/exotice/raw/master/docs/exotice.png "création d'exercice")
Application web de création et de gestion d'exercices interactifs.
## exemples d'exercices
![alt text](https://framagit.org/AlicVB/exotice/raw/master/docs/exotice_livre2.png "création d'exercice")

## détails
exoTICE est une application web écrite en php/javascript.

Elle permet la mise en place d'exercices interactifs tels que :

* Q.C.M

* relier des éléments

* déplacer des élements

* écrire du texte

* ...

avec une mise en page poussée.

## téléchargement
vous pouvez télécharger [l'archive courante](https://framagit.org/AlicVB/exotice/repository/archive.zip?ref=master)

## aide
consultez le [wiki](https://framagit.org/AlicVB/exotice/wikis/home) (construction en cours...)