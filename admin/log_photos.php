<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  include("../core/maj.php");
  $test = (file_exists("../TEST"));
  
  if (isset($_GET['uid']) && isset($_GET['cat']) && isset($_GET['livre']))
  {
    // on cherche le chemin du livre
    $livre = $_GET['livre'];
    $uid = $_GET['uid'];
    $cat = $_GET['cat'];
  }
  else
  {
    echo "erreur d'appel de la page !";
    exit;
  }
  //les actions éventuelles
  if (isset($_GET['action']) && isset($_GET['exo']))
  {
    //on vérifie déjà que la photo de l'exo existe bien dans les logs
    $lphoto = "../log_exo/$uid/$cat/$livre/".$_GET['exo'];
    if (!file_exists("$lphoto.jpg"))
    {
      echo "exo introuvable ! ";
      exit;
    }
    if ($_GET['action'] == "ok")
    {
      $ess = 1;
      if (file_exists("$lphoto.txt"))
      {
        $vv = explode("\n", file_get_contents("$lphoto.txt"));
        if (count($vv)>1) $ess = $vv[1];
      }
      file_put_contents("$lphoto.txt", "OK\n$ess");
    }
    
    else if ($_GET['action'] == "ko")
    {
      $ess = 1;
      if (file_exists("$lphoto.txt"))
      {
        $vv = explode("\n", file_get_contents("$lphoto.txt"));
        if (count($vv)>1) $ess = $vv[1];
      }
      file_put_contents("$lphoto.txt", "KO\n$ess");
    }
    
    else if ($_GET['action'] == "reset")
    {
      if (file_exists("$lphoto.txt")) unlink("$lphoto.txt");
      if (file_exists("$lphoto.jpg")) unlink("$lphoto.jpg");
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- administration</title>
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <link rel="stylesheet" href="admin.css">
  <script type="text/javascript" src="logs.js"></script>
</head>

<body>
  <div id="bandeau">
    <div id="bandeau2">&nbsp;</div>
    <a href="index.php"><img class="bimg2" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
    <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  </div>
  <div id='photo_titre'>
    <?php
      // on cherche les infos du livre
      if (file_exists("../livres/$cat/$livre/livre.txt"))
      {
        $livret = explode("\n", file_get_contents("../livres/$cat/$livre/livre.txt"));
        $nom = $livret[0];
        $coul = $livret[1];
        if ($coul == "transparent" || $coul == "#878787") $coul = "black";
        $photobook = trim($livret[5]);
      }
    ?>
  </div>
  <div id='photo_liste'>
    <?php    
      // on parcoure tous les exos existants
      $exos = explode("\n", file_get_contents("../livres/$cat/$livre/liste.txt"));
      for ($l=0; $l<count($exos); $l++)
      {
        if (trim($exos[$l]) != "")
        {
          //on cherche l'image modèle
          $dosl = "../livres/$cat/$livre/img/";
          $modele = "../livres/$cat/$livre/img/$exos[$l]";
          if (file_exists($dosl.$exos[$l].".jpg")) $modele = $exos[$l].".jpg";
          else if (file_exists($dosl.$exos[$l].".png")) $modele = $exos[$l].".png";
          else if (file_exists($dosl.$exos[$l].".svg")) $modele = $exos[$l].".svg";
          else continue;
          //et celle de correction si elle existe
          $corr = "";
          if (file_exists($dosl.$exos[$l].".corr.jpg")) $corr = $exos[$l].".corr.jpg";
          else if (file_exists($dosl.$exos[$l].".corr.png")) $corr = $exos[$l].".corr.png";
          else if (file_exists($dosl.$exos[$l].".corr.svg")) $corr = $exos[$l].".corr.svg";

          //on cherche la photo
          if (file_exists("../log_exo/$uid/$cat/$livre/$exos[$l].jpg")) $photo = "../log_exo/$uid/$cat/$livre/$exos[$l].jpg?dummy=".time();
          else $photo = "";
          
          //et l'état de la correction
          $etat = -1;
          $nb_essais = 0;
          if (file_exists("../log_exo/$uid/$cat/$livre/$exos[$l].txt"))
          {
            $ll = explode("\n", file_get_contents("../log_exo/$uid/$cat/$livre/$exos[$l].txt"));
            if ($ll[0] == "??") $etat = 1;
            else if ($ll[0] == "OK") $etat = 2;
            else $etat = 0;
            if (count($ll)>1) $nb_essais = trim($ll[1]);
          }
          else $etat = -1;
          if ($photo == "") $etat = -1;
          
          //et on construit le div
          echo "<div>";
          echo "<div class='photo_".$etat."'>";
          //les images
          if ($etat>=0) echo "<img class='photo_reponse' src=\"".$photo."\" />";
          if ($etat>=0 && $corr != "") echo "<img class='photo_modele' src=\"".$dosl.$corr."\" />";
          else echo "<img class='photo_modele' src=\"".$dosl.$modele."\" />";
          
          //et le nombre d'essais
          echo "<div class='essais_div'>";
          if ($photobook == 0)
          {
            echo "<img src='../icons/penholder.svg' alt='pas de limite' />";
          }
          else
          {
            for ($i=0; $i<$nb_essais; $i++)
            {
              echo "<img class='essais_gris' src='../icons/draw-freehand.svg' />";
            }
            for ($i=$nb_essais; $i<$photobook; $i++)
            {
              echo "<img src='../icons/draw-freehand.svg' />";
            }
          }
          echo "</div>";
          //et l'icone par dessus si besoin
          if ($etat == 2)
          {
            echo "<img class='photo_icone' src='../icons/dialog-apply.svg' />";
          }
          else if ($etat == 0)
          {
            echo "<img class='photo_icone' src='../icons/window-close.svg' />";
          }
          
          echo "</div>";
          //la barre de controles
          echo "<div class='photo_controles'>";
          if ($etat != -1 && $etat != 2) echo "<a href='log_photos.php?cat=$cat&livre=$livre&uid=$uid&action=ok&exo=$exos[$l]'><img src='../icons/dialog-apply.svg' title='JUSTE'/></a>";
          else echo "<img class='photo_gris' src='../icons/dialog-apply.svg' title='JUSTE'/>";
          if ($etat != -1 && $etat != 0) echo "<a href='log_photos.php?cat=$cat&livre=$livre&uid=$uid&action=ko&exo=$exos[$l]'><img src='../icons/window-close.svg' title='FAUX'/></a>";
          else echo "<img class='photo_gris' src='../icons/window-close.svg' title='FAUX'/>";
          if ($etat != -1) echo "&nbsp;&nbsp;<a href='log_photos.php?cat=$cat&livre=$livre&uid=$uid&action=reset&exo=$exos[$l]'><img src='../icons/draw-eraser.svg' title='remise à zéro'/></a>";
          else echo "&nbsp;&nbsp;<img class='photo_gris' src='../icons/draw-eraser.svg' title='remise à zéro'/>";
          echo "</div>";
          echo "</div>";
        }
      }
    ?>
  </div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div>
</body>
</html>
