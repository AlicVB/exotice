<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
$test = (file_exists("../TEST"));
if ($test)
{
  echo "*action impossible en mode test !";
  exit;
}

function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}

function recursiveRemoveDirectory($directory)
{
    foreach(myglob("{$directory}", "fic+dos") as $file)
    {
        if(is_dir($file)) { 
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}

function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    
    return $str;
}

  if (isset($_GET['io']) && $_GET['io'] == "charge" && isset($_GET['cat']))
  {
    $fn = time().".zip";
    $fw = fopen($fn, "wb");
    $ret = "";
    $cat = $_GET['cat'];
    
    for ($i=0; $i<count($_FILES['l_import']['name']) ; $i++)
    {
      if ("{$_FILES['l_import']['error'][$i]}" == "1") $ret .= "Erreur : fichier trop gros !\n";
      else
      {
        $fr = fopen($_FILES['l_import']['tmp_name'][$i], "rb");
        $b = fread($fr, filesize($_FILES['l_import']['tmp_name'][$i]));
        fwrite($fw, $b);
        fclose($fr);
      }
    }
    fclose($fw);
    //on dézippe
    $zip = new ZipArchive;
    $res = $zip->open($fn);
    if ($res === TRUE)
    {
      $livre = time();
      while (file_exists("../livres/$cat/$livre")) $livre += 1;
      mkdir("../livres/$cat/$livre", 0777, true);
      $zip->extractTo("../livres/$cat/$livre");
      $zip->close();
      // on vérifie et on enlève les fichiers qu'on ne veut pas
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      $vv = myglob("../livres/$cat/$livre", "fic+dos");
      for ($i=0; $i<count($vv); $i++)
      {
        $mm = finfo_file($finfo, $vv[$i]);
        $ff = basename($vv[$i]);
        if ($mm == "text/plain" && ($ff == "liste.txt" || $ff == "livre.txt" || $ff == "version")) continue;
        if (is_dir($vv[$i]) && ($ff == "img" || $ff == "sons" || $ff == "exos" || $ff == "videos")) continue;
        echo "fichier suspect : $vv[$i] SUPPRIME!\n";
        if (is_dir($vv[$i])) recursiveRemoveDirectory($vv[$i]);
        else unlink($vv[$i]);
      }
      if (file_exists("../livres/$cat/$livre/img"))
      {
        $vv = myglob("../livres/$cat/$livre/img", "fic+dos");
        for ($i=0; $i<count($vv); $i++)
        {
          $mm = finfo_file($finfo, $vv[$i]);
          if (substr($mm, 0, 6) == "image/") continue;
          echo "fichier suspect : $vv[$i] SUPPRIME!\n";
          if (is_dir($vv[$i])) recursiveRemoveDirectory($vv[$i]);
          else unlink($vv[$i]);
        }
      }
      if (file_exists("../livres/$cat/$livre/sons"))
      {
        $vv = myglob("../livres/$cat/$livre/sons", "fic+dos");
        for ($i=0; $i<count($vv); $i++)
        {
          $mm = finfo_file($finfo, $vv[$i]);
          if (substr($mm, 0, 6) == "audio/") continue;
          echo "fichier suspect : $vv[$i] SUPPRIME!\n";
          if (is_dir($vv[$i])) recursiveRemoveDirectory($vv[$i]);
          else unlink($vv[$i]);
        }
      }
      if (file_exists("../livres/$cat/$livre/videos"))
      {
        $vv = myglob("../livres/$cat/$livre/videos", "fic+dos");
        for ($i=0; $i<count($vv); $i++)
        {
          $mm = finfo_file($finfo, $vv[$i]);
          if (substr($mm, 0, 6) == "video/") continue;
          echo "fichier suspect : $vv[$i] SUPPRIME!\n";
          if (is_dir($vv[$i])) recursiveRemoveDirectory($vv[$i]);
          else unlink($vv[$i]);
        }
      }
      if (file_exists("../livres/$cat/$livre/exos"))
      {
        $vv = myglob("../livres/$cat/$livre/exos", "fic+dos");
        for ($i=0; $i<count($vv); $i++)
        {
          if (is_dir($vv[$i]))
          {
            $vvv = myglob("$vv[$i]", "fic+dos");
            for ($j=0; $j<count($vvv); $j++)
            {
              $mm = finfo_file($finfo, $vvv[$j]);
              $ff = basename($vvv[$j]);
              if ($mm == "text/plain" && ($ff == "exo.txt" || $ff == "exo_sav.txt" || $ff == "exo.inc.txt" || $ff == "version")) continue;
              if ($mm == "text/html" && ($ff == "exo.inc.txt")) continue;
              echo "fichier suspect : $vvv[$j] SUPPRIME!\n";
              if (is_dir($vvv[$j])) recursiveRemoveDirectory($vvv[$j]);
              else unlink($vvv[$j]);
            }
          }
          else
          {
            echo "fichier suspect : $vv[$i] SUPPRIME!\n";
            unlink($vv[$i]);
          }
        }
      }
      // on ajoute le livre en bas de la liste
      $txt = file_get_contents("../livres/$cat/liste.txt");
      if ($txt != "") $txt .= "\n";
      $txt .= $livre;
      file_put_contents("../livres/$cat/liste.txt", $txt);
    }
    else $ret .= "Erreur : impossible de lire le fichier...\n";
    unlink($fn);
    echo $ret;
  }
  else if (isset($_POST["copy"]) && isset($_POST["dest"]) && isset($_POST["src"]))
  {
    $dest = $_POST["dest"];
    $src = $_POST["src"];
    $src2 = dirname(dirname($src));
    //on détermine le dossier du nouvel exercice
    $exo = 1;
    while (file_exists("$dest/exos/$exo")) $exo++;
    $dest2 = "$dest/exos/$exo";
    //on copie les fichier "classiques"
    if (!file_exists($dest2)) mkdir("$dest2", 0777, true);
    $fics = myglob("$src");
    for($i=0; $i<count($fics); $i++)
    {
      copy($fics[$i], "$dest2/".basename($fics[$i]));
    }
    
    //et on s'occupe des images et des sons
    preg_match_all("/src=\"([^\"]*)\"/", file_get_contents("$dest2/exo.inc.txt"), $matches);
    for ($i=0; $i<count($matches[1]); $i++)
    {
      $img = $matches[1][$i];
      if (file_exists("$dest/$img")) continue;
      if (!file_exists("$src2/$img")) continue;
      copy("$src2/$img", "$dest/$img");
    }
    // on ajoute le livre en bas de la liste
    $txt = file_get_contents("$dest/liste.txt");
    if ($txt != "") $txt .= "\n";
    $txt .= $exo;
    file_put_contents("$dest/liste.txt", $txt);
  }
  else if (isset($_POST["hidden"]) && isset($_POST["cat"]) && isset($_POST["v"])) 
  {
    //on veut juste sauvegarder les infos du livre
    header("Content-Type: text/plain"); // Utilisation d'un header pour spécifier le type de contenu de la page. Ici, il s'agit juste de texte brut (text/plain). 
    $cat = $_POST["cat"];
    $v = $_POST["v"];
    if (isset($_POST["livre"]))
    {
      $livre = $_POST["livre"];
      file_put_contents("../livres/$cat/$livre/hide.txt", $v, LOCK_EX);
    }
    else
    {
      file_put_contents("../livres/$cat/hide.txt", $v, LOCK_EX);
    }
  }
  else if (isset($_POST["cat"]) && isset($_POST["v"])) 
  {
    //on veut juste sauvegarder les infos du livre
    header("Content-Type: text/plain"); // Utilisation d'un header pour spécifier le type de contenu de la page. Ici, il s'agit juste de texte brut (text/plain). 
    $cat = $_POST["cat"];
    $v = $_POST["v"];
    if (isset($_POST["livre"]))
    {
      $livre = $_POST["livre"];
      file_put_contents("../livres/$cat/$livre/livre.txt", $v, LOCK_EX);
      //on met à jour le fichier de version
      copy("../VERSION", "../livres/$cat/$livre/version");
    }
    else
    {
      file_put_contents("../livres/$cat/cat.txt", $v, LOCK_EX);
    }
  }
?>
