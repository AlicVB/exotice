 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

//initialisation de tout ce qu'il faut
function start()
{
  //on enlève l'action de l'url
  if (window.location.href.lastIndexOf("&action=") != -1)
  {
    let pos = window.location.href.lastIndexOf("&action=");
    history.replaceState('suppr action', document.title, window.location.href.substr(0, pos));
  }
  
  //on met à jour les couleurs des visibilités
  visi_change("", document.getElementById("visi__ALL_"), false);
}

//mise à jour et sauvegarde des propriétés de visibilité
function visi_change(cat, e, sauve)
{
  //si "personne" est coché, on désactive les autres
  if (e.id == "visi__ALL_")
  {
    if (!e.checked) e.parentNode.style.backgroundColor = "silver";
    else e.parentNode.style.backgroundColor = "#f17566";
    
    let els = document.getElementsByClassName("visi");
    for (let i=0; i<els.length; i++)
    {
      if (els[i].firstChild != e)
      {
        els[i].firstChild.disabled = (e.checked);
        if (e.checked) els[i].style.backgroundColor = "silver";
        else visi_change("", els[i].firstChild, false);
      }
    }
  }
  else
  {
    if (e.checked) e.parentNode.style.backgroundColor = "#c8f578";
    else e.parentNode.style.backgroundColor = "#f17566";
  }
  
  //si besoin, on enregistre
  if (cat != "" && sauve)
  {
    let tx = "";
    let els = document.getElementsByClassName("visi");
    for (let i=0; i<els.length; i++)
    {
      if (els[i].firstChild.id == "visi__ALL_")
      {
        if (els[i].firstChild.checked) tx +=els[i].firstChild.value + "\n";
      }
      else if (!els[i].firstChild.checked) tx +=els[i].firstChild.value + "\n";
    }
    
    let xhr = new XMLHttpRequest();
    let ligne = "hidden&cat=" + cat + "&v=" + tx;
    xhr.open("POST", "livre_sauve.php" , true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(ligne);
  }
}

//sauvegarde des changements sur les infos de catégorie
function cat_change(cat)
{
  //on construit la chaine totale à mettre dans le fichier cat.txt
  let txt = document.getElementById("ctitre").value;
  txt += "\n" + document.getElementById("ccoul").value;

  let xhr = new XMLHttpRequest();
  let ligne = "cat=" + cat + "&v=" + txt;
  xhr.open("POST", "livre_sauve.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

//supression de livre (confirmation)
function livre_supprime(cat, livre)
{
  let r = confirm("Etes-vous sûr(e) de vouloir supprimer le livre " + livre + " ?");
  if (r == true) window.location.assign("cat.php?cat=" + cat + "&livre=" + livre + "&action=remove");
}

//ajout de livre (demande du nom)
function livre_new(cat)
{
  let txt = prompt("nom du livre", "");
  if (!txt || txt == "") return;

  window.location.assign("livre.php?livre=" + encodeURIComponent(txt.replace(/\//g, "_")) + "&cat=" + cat + "&action=new");
}

//ajout de photobook (demande du nom)
function photo_new(cat)
{
  let txt = prompt("nom du photobook", "");
  if (!txt || txt == "") return;

  window.location.assign("photo.php?livre=" + encodeURIComponent(txt.replace(/\//g, "_")) + "&cat=" + cat + "&action=new");
}

//bascule de la visibilité des livres
function livre_visi_switch(e, cat, livre)
{
  let tx = "";
  if (e.getAttribute("value") == "0") tx = "_ALL_";

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (e.getAttribute("value") == "0")
      {
        e.src = "../icons/show_no.svg";
        e.title = "visible pour personne -- cliquer pour rendre visible à tous";
        e.setAttribute("value", "2");
      }
      else
      {
        e.src = "../icons/show_ok.svg";
        e.title = "visible pour tous -- cliquer pour masquer à tous";
        e.setAttribute("value", "0");
      }
    }
  };
  let ligne = "hidden&cat=" + cat +"&livre=" + livre + "&v=" + tx;
  xhr.open("POST", "livre_sauve.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

//import d'un nouveau livre (fichier zip)
function livre_import(cat)
{
  document.getElementById("c_import").cat = cat;
  document.getElementById("c_import").click();
}

//routines pour l'import
function c_import_change(el)
{
  if (el.files.length == 0) return;
  let file = el.files[0];

  let fr = new FileReader();
  fr.onload = function(e) {
        splitAndSendFile(new Uint8Array(e.target.result), file, el.cat);
    };
  fr.readAsArrayBuffer(file);
}
//routines pour l'import (découpage de fichiers si ils sont trop gros)
function splitAndSendFile(dataArray, file, cat)
{
  let fd = new FormData();
  let blob;
  for (let i=0; i < dataArray.length; i += 1800000)
  {
    blob = new Blob([dataArray.subarray(i, i + 1800000)]);
    fd.append("l_import[]", blob, file.name + ".part" + (i / 1e6));
  }

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
      {
        if (xhr.responseText.trim() != "") alert(xhr.responseText);
        location.reload(true);
      }
    };
  xhr.open("POST", "livre_sauve.php?io=charge&cat=" + cat);
  xhr.send(fd);
}
