<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

include("../core/maj.php");
$test = (file_exists("../TEST"));

function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}

function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    $str = preg_replace("/[^[:alnum:]]/u", '_', $str); // on ne garde que les char alpha_num
    return $str;
}
function free_path($fic, $isdir=false)
{
  $fic = dirname($fic)."/".wd_remove_accents(basename($fic));
  if (!file_exists($fic)) return $fic;
  if ($isdir)
  {
    $p1 = $fic;
    $p2 = "";
  }
  else
  {
    $p1 = substr($fic, 0, -4);
    $p2 = substr($fic, -4);
  }
  $i = 1;
  while (file_exists($p1."_".$i.$p2))
  {
    $i++;
  }
  return $p1."_".$i.$p2;
}
function cat_creation($cat)
{
  // si le livre n'existe pas encore, il faut en créer un nouveau
  $dos = "../livres/$cat";
  if (!file_exists($dos))
  {
    $cat = basename(free_path($dos, true));
    //on l'ajoute à la liste des livres
    $txt = file_get_contents("../livres/liste.txt");
    if ($txt != "") $txt .= "\n";
    $txt .= $cat;
    file_put_contents("../livres/liste.txt", $txt);
    $dos = "../livres/$cat";
  }
  //on crée tout ce qu'il manque
  if (!file_exists("$dos")) mkdir("$dos", 0777, true);
  
  return $cat;
}
function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ($file = readdir($dir)))
    {
      if (( $file != '.' ) && ( $file != '..' ))
      {
        if ( is_dir($src.'/'.$file) )
        {
          recurse_copy($src.'/'.$file, $dst.'/'.$file);
        }
        else
        {
          copy($src.'/'.$file, $dst.'/'.$file);
        }
      }
    }
    closedir($dir);
}

  //il faut au moins spécifier la catégorie !
  if (!isset($_GET['cat']))
  {
    echo "Aucune catégorie spécifiée !";
    exit;
  }
  $cat = $_GET['cat'];
  $titre = $cat;
  //si c'est une création...
  if (isset($_GET['action']) && $_GET['action'] == "new")
  {
    if (!$test) $cat = cat_creation(urldecode($_GET['cat']));
    else
    {
      echo "mode TEST : création de catégorie impossible !";
      exit;
    }
  }
  
  $dos = "../livres/$cat";
  if (!$test)
  {
    if (!file_exists("$dos")) mkdir("$dos", 0777, true);
    
    //si la liste n'existe pas, on la crée
    if (!file_exists("$dos/liste.txt"))
    {
      $vv = myglob("$dos" , "dos");
      $txt = "";
      for ($i=0; $i<count($vv); $i++)
      {
        if ($txt != "") $txt .= "\n";
        $txt .= basename($vv[$i]);
      }
      file_put_contents("$dos/liste.txt", $txt);
    }
  }
  else if (!file_exists("$dos") || !file_exists("$dos/liste.txt"))
  {
    echo "mode TEST : création de catégorie impossible !";
    exit;
  }
  
  //on traite d'abord des actions à faire
  if (isset($_GET['action']) && isset($_GET['livre']))
  {
    if (!$test || $_GET['action'] == "export")
    {
      $livre = $_GET['livre'];
  
      switch ($_GET['action'])
      {
        case "copy":
          $n = 1;
          while (file_exists("$dos/$livre"."_".$n)) $n++;
          recurse_copy("$dos/$livre", "$dos/$livre"."_".$n);
          $txt = file_get_contents("$dos/liste.txt");
          if ($txt != "") $txt .= "\n";
          $txt .= $livre."_".$n;
          file_put_contents("$dos/liste.txt", $txt);
          break;
        case "remove":
          if (file_exists("$dos/$livre")) recursiveRemoveDirectory("$dos/$livre");
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $txt = "";
          for ($i=0; $i<count($livres); $i++)
          {
            if ($livres[$i] != $livre)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
          }
          file_put_contents("$dos/liste.txt", $txt);
          break;
        case "up":
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $pos = array_search($livre, $livres);
          if ($pos > 0)
          {
            $txt = "";
            for ($i=0; $i<$pos-1; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $livre;
            $txt .= "\n".$livres[$pos-1];
            for ($i=$pos+1; $i<count($livres); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            file_put_contents("$dos/liste.txt", $txt);
          }
          break;
        case "down":
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $pos = array_search($livre, $livres);
          if ($pos < count($livres)-1)
          {
            $txt = "";
            for ($i=0; $i<$pos; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $livres[$pos+1];
            $txt .= "\n".$livre;          
            for ($i=$pos+2; $i<count($livres); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            file_put_contents("$dos/liste.txt", $txt);
          }
          break;
        case "export":
          $zip = new ZipArchive(); // Load zip library
          $zip_name = time().".zip"; // Zip name
          if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
          {
            echo "impossible de créer l'archive...";
            exit;
          }
          //on ajoute tout ce qu'il faut
          $zip->addFile("$dos/$livre/livre.txt", "livre.txt");
          $zip->addFile("$dos/$livre/liste.txt", "liste.txt");
          $zip->addFile("$dos/$livre/version", "version");
          $zip->addEmptyDir("img");
          $zip->addEmptyDir("sons");
          $zip->addEmptyDir("exos");
          $zip->addEmptyDir("videos");
          $ff = myglob("$dos/$livre/img");
          for ($i=0; $i<count($ff); $i++) $zip->addFile($ff[$i], "img/".basename($ff[$i]));
          $ff = myglob("$dos/$livre/sons");
          for ($i=0; $i<count($ff); $i++) $zip->addFile($ff[$i], "sons/".basename($ff[$i]));
          $ff = myglob("$dos/$livre/videos");
          for ($i=0; $i<count($ff); $i++) $zip->addFile($ff[$i], "videos/".basename($ff[$i]));
          $ff = myglob("$dos/$livre/exos", "dos");
          for ($i=0; $i<count($ff); $i++)
          {
            $exo = basename($ff[$i]);
            $zip->addEmptyDir("exos/$exo");
            $zip->addFile("$ff[$i]/exo.inc.txt", "exos/$exo/exo.inc.txt");
            $zip->addFile("$ff[$i]/exo_sav.txt", "exos/$exo/exo_sav.txt");
            $zip->addFile("$ff[$i]/exo.txt", "exos/$exo/exo.txt");
            $zip->addFile("$ff[$i]/version", "exos/$exo/version");
          }
          $zip->close();
          if(file_exists($zip_name))
          {
            // push to download the zip
            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename=\"$livre.zip\"");
            readfile($zip_name);
            // remove zip file is exists in temp path
            unlink($zip_name);
            exit;
          }
          else
          {
            echo "Erreur dans la création de l'archive...";
            exit;
          }
          break;
      }
    }
    else
    {
      echo "mode TEST : impossible d'effectuer l'action : ".$_GET['action']." !";
    }
  }
  
  $coul = "transparent";
  //on essaie de lire le fichier descriptif
  if (file_exists("$dos/cat.txt"))
  {
    $infos = explode("\n", file_get_contents("$dos/cat.txt"));
    if (count($infos)>0) $titre = $infos[0];
    if (count($infos)>1) $coul = $infos[1];
  }
  else if (!$test)
  {
    file_put_contents("$dos/cat.txt", "$titre\n$coul");
  }
  else
  {
    echo "mode TEST : il manque le fichier cat.txt !";
    exit;
  }
  
  //on s'occupe de la visibilité
  $hidden = array();
  if (file_exists("$dos/hide.txt")) $hidden = explode("\n", file_get_contents("$dos/hide.txt"));
  else file_put_contents("$dos/hide.txt", "");

?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- categorie</title>
  <link rel="stylesheet" href="livre.css">
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <script type="text/javascript" src="../libs/jscolor.min.js"></script>
  <script type="text/javascript" src="cat.js"></script>
</head>

<body onload="start()">
  <div id="bandeau">
    <div id="bandeau2">
      <a href="creation.php"><img class="bimg" src="../icons/edit-undo.svg" title="Retour à la liste"/></a>
    </div>
    <a href="index.php"><img class="bimg" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg2" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>

  <div class="col">
  <div class="titre">infos catégorie</div>
  <table>
    <tr>
      <td class="td1">Nom de la catégorie</td>
      <td class="td2"><input type="text" id="ctitre" size="30" value="<?php echo $titre ?>" onchange="cat_change('<?php echo $cat ?>')"/></td>
    </tr>
    <tr>
      <td class="td1">Couleur</td>
      <td class="td2"><input class="jscolor {hash:true}" type="text" id="ccoul" value="<?php echo $coul ?>" onchange="cat_change('<?php echo $cat ?>')" /></td>
    </tr>
    <tr>
      <td class="td1">Visibilité</td>
      <td class="td2 groupes">
<?php
    //case "personne" (hide = ALL)
    echo "<span class=\"visi\"><input onchange=\"visi_change('$cat', this, true)\" id=\"visi__ALL_\" type=\"checkbox\" value=\"_ALL_\" ";
    if (in_array("_ALL_", $hidden)) echo "checked ";
    echo "/><label for=\"visi__ALL_\">PERSONNE</label></span>";
    
    //et les cases de chaque groupe
    if (file_exists("../groupes.txt"))
    {
      $gg = explode("\n", file_get_contents("../groupes.txt"));
      for ($i=0; $i<count($gg); $i++)
      {
        $vv = explode("|", $gg[$i]);
        if (count($vv)>2)
        {
          echo "<span class=\"visi\"><input onchange=\"visi_change('$cat', this, true)\" id=\"visi_$vv[2]\" type=\"checkbox\" value=\"$vv[2]\" ";
          if (!in_array($vv[2], $hidden)) echo "checked ";
          echo "/><label for=\"visi_$vv[2]\">$vv[0]</label></span>";
        }
      }
    }
?>
      </td>
    </tr>
  </table>
  </div>
  <div class="col">
    <div class="titre">livres</div>
  <table>
<?php
    $livres = explode("\n", file_get_contents("$dos/liste.txt"));
    for ($i=0; $i<count($livres); $i++)
    {
      //on lit les détails du livre
      if (file_exists("$dos/$livres[$i]/livre.txt"))
      {
        $livre = $livres[$i];
        $et = "";
        $ecoul = "transparent";
        $infos = explode("\n", file_get_contents("$dos/$livre/livre.txt"));
        $photobook = false;
        if (count($infos)>0) $et = $infos[0];
        if (count($infos)>1) $ecoul = $infos[1];
        if (count($infos)>5 && trim($infos[5]) != "") $photobook = true;
        // on écrit le bloc qui correspond
        echo "<tr style=\"background-color:$ecoul;\">\n";
        if ($photobook) echo "<td class=\"td2\"><img class=\"eimg\" src=\"../icons/camera-photo.svg\" title=\"Ceci est un photobook\" />";
        else echo "<td class=\"td2\"><img class=\"eimg\" src=\"../icons/livre.svg\" title=\"Ceci est un livret d'exercices\" />";
        if ($photobook) echo "<a class=\"ea\" href=\"photo.php?cat=$cat&livre=$livre\" title=\"livres/$cat/$livre\"><b>$et</b></a></td>\n";
        else echo "<a class=\"ea\" href=\"livre.php?cat=$cat&livre=$livre\" title=\"livres/$cat/$livre\"><b>$et</b></a></td>\n";
        echo "<td class=\"td3\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer le livre\" onclick=\"livre_supprime('$cat', '$livre')\"/>";
        echo "<a href=\"cat.php?cat=$cat&livre=$livre&action=copy\"><img class=\"eimg\" src=\"../icons/tab-new.svg\" title=\"dupliquer le livre\"/></a>\n";
        echo "<a href=\"cat.php?cat=$cat&livre=$livre&action=export\"><img class=\"eimg\" src=\"../icons/system-software-installer.svg\" title=\"exporter le livre\"/></a>\n";
        echo "<a href=\"cat.php?cat=$cat&livre=$livre&action=up\"><img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter le livre\"/></a>\n";
        echo "<a href=\"cat.php?cat=$cat&livre=$livre&action=down\"><img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre le livre\"/></a>\n";
        
        //on regarde la visibilité
        $hide = 0;
        if (file_exists("$dos/$livre/hide.txt"))
        {
          $vv = explode("\n", file_get_contents("$dos/$livre/hide.txt"));
          for ($j=0; $j<count($vv); $j++)
          {
            if (trim($vv[$j]) == "_ALL_") $hide = 2;
            else if (trim($vv[$j]) != "") $hide = 1;
            else continue;
            break;
          }
        }
        if ($hide == 0) echo "<img value=\"0\" onclick=\"livre_visi_switch(this, '$cat', '$livre')\" class=\"eimg\" src=\"../icons/show_ok.svg\" title=\"visible pour tous -- cliquer pour masquer à tous\"/>";
        else if ($hide == 1) echo "<img onclick=\"livre_visi_switch(this, '$cat', '$livre')\" value=\"1\" class=\"eimg\" src=\"../icons/show_semi.svg\" title=\"visible pour certains -- cliquer pour rendre visible à tous\"/>";
        else echo "<img onclick=\"livre_visi_switch(this, '$cat', '$livre')\" value=\"2\" class=\"eimg\" src=\"../icons/show_no.svg\" title=\"visible pour personne -- cliquer pour rendre visible à tous\"/>";
        
        echo "</td></tr>\n";
      }
    }
    echo "</table>";
    echo "<div class=\"enew\"><button onclick=\"livre_new('$cat')\">nouveau livre</button>&nbsp;&nbsp;";
    echo "<button onclick=\"livre_import('$cat')\">importer un livre</button>";
    echo "&nbsp;&nbsp;<button onclick=\"photo_new('$cat')\">nouveau photobook</button>";
    echo "</div>";
    echo "</div>\n";
?>
  <form enctype="multipart/form-data" style="display:none;">
    <input name="c_import" type="file" id="c_import" accept=".zip" onchange="c_import_change(this)"/>
  </form>
  <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
