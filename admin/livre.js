 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

function readCookie(name) {
	let nameEQ = name + "=";
	let ca = document.cookie.split(';');
	for(let i=0;i < ca.length;i++) {
		let c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function iimg_load(e)
{
  //on veut que l'image rentre dans un carré de 200x200
  if (e.offsetWidth>e.offsetHeight) e.style.width = "150px";
  else e.style.height = "150px";
}

function infos_img_change(e)
{
  e.form.submit();
}

function infos_change(cat, livre, photobook=false)
{
  //on construit la chaine totale à mettre dans le fichier livre.txt
  let txt = document.getElementById("ititre").value;
  txt += "\n" + document.getElementById("icoul").value;
  txt += "\n" + document.getElementById("iaut").value;
  if (document.getElementById("iimg") && document.getElementById("iimg").src != "") txt += "\nimg/" + document.getElementById("iimg").src.split(/[\\/]/).pop();
  else txt += "\n";
  txt += "\n" + document.getElementById("inotes").value.replace(/(?:\r\n|\r|\n)/g, 'ⱡ');
  if (photobook) txt += "\n"+document.getElementById("iessais").value;
  else txt += "\n";
  txt += "\n\n\n\n\n\n";
  txt += document.getElementById("idetails").value;

  let xhr = new XMLHttpRequest();
  let ligne = "cat=" + cat + "&livre=" + livre + "&v=" + txt;
  xhr.open("POST", "livre_sauve.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);

  //on sauvegarde aussi en local le nom de l'auteur
  document.cookie = "auteur=" + document.getElementById("iaut").value + "; expires=Sun, 28 Feb 8888 00:00:00 UTC";
}

function start(aut)
{
  if (aut == "")
  {
    let rep = readCookie("auteur");
    if (rep) document.getElementById("iaut").value = rep;
    document.getElementById("iaut").onchange();
  }
  //on enlève l'action de l'url
  if (window.location.href.lastIndexOf("&action=") != -1)
  {
    let pos = window.location.href.lastIndexOf("&action=");
    history.replaceState('suppr action', document.title, window.location.href.substr(0, pos));
  }
  
  //on met à jour les couleurs des visibilités
  livre_visi_change("", "", document.getElementById("visi__ALL_"), false);
}

function livre_visi_change(cat, livre, e, sauve)
{
  //si "personne" est coché, on désactive les autres
  if (e.id == "visi__ALL_")
  {
    if (!e.checked) e.parentNode.style.backgroundColor = "silver";
    else e.parentNode.style.backgroundColor = "#f17566";
    
    let els = document.getElementsByClassName("visi");
    for (let i=0; i<els.length; i++)
    {
      if (els[i].firstChild != e)
      {
        els[i].firstChild.disabled = (e.checked);
        if (e.checked) els[i].style.backgroundColor = "silver";
        else livre_visi_change("", "", els[i].firstChild, false);
      }
    }
  }
  else
  {
    if (e.checked) e.parentNode.style.backgroundColor = "#c8f578";
    else e.parentNode.style.backgroundColor = "#f17566";
  }
  
  //si besoin, on enregistre
  if (cat != "" && livre != "" && sauve)
  {
    let tx = "";
    let els = document.getElementsByClassName("visi");
    for (let i=0; i<els.length; i++)
    {
      if (els[i].firstChild.id == "visi__ALL_")
      {
        if (els[i].firstChild.checked) tx +=els[i].firstChild.value + "\n";
      }
      else if (!els[i].firstChild.checked) tx +=els[i].firstChild.value + "\n";
    }
    
    let xhr = new XMLHttpRequest();
    let ligne = "hidden&cat=" + cat + "&livre=" + livre + "&v=" + tx;
    xhr.open("POST", "livre_sauve.php" , true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(ligne);
  }
}

function exo_visi_switch(e, cat, livre, exo)
{
  let tx = "";
  if (e.getAttribute("value") == "0") tx = "_ALL_";

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (e.getAttribute("value") == "0")
      {
        e.src = "../icons/show_no.svg";
        e.title = "visible pour personne -- cliquer pour rendre visible à tous";
        e.setAttribute("value", "2");
      }
      else
      {
        e.src = "../icons/show_ok.svg";
        e.title = "visible pour tous -- cliquer pour masquer à tous";
        e.setAttribute("value", "0");
      }
    }
  };
  let ligne = "io=sauve&fic=hide&cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&v=" + tx;
  xhr.open("POST", "crea_exo/io.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function add_select(e, dos)
{
  if (e.value != "")
  {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
      {
        // on met les bonnes valeurs aux bons endroits
        window.location.reload(true);
      }
    };
    let ligne = "copy=&dest=" + dos + "&src=" + e.value;
    xhr.open("POST", "livre_sauve.php" , true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(ligne);
  }
}

function exo_supprime(cat, livre, exo)
{
  let r = confirm("Etes-vous sûr(e) de vouloir supprimer l'exercice " + exo + " ?");
  if (r == true) window.location.assign("livre.php?cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&action=remove");
}

function photo_supprime(cat, livre, exo)
{
  let r = confirm("Etes-vous sûr(e) de vouloir supprimer la photo " + exo + " ?");
  if (r == true) window.location.assign("photo.php?cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&action=remove");
}

function photo_add_text_item()
{
  let txt = prompt("texte des items, séparés par des ; sans espaces", "");
  if (!txt || txt == "") return;
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      // on met les bonnes valeurs aux bons endroits
      window.location.reload(true);
    }
  };
  let ligne = "action=add_text&items=" + txt;
  xhr.open("POST", window.location.href , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
