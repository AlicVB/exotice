<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

include("../core/maj.php");
$test = (file_exists("../TEST"));
  
function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}

function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ($file = readdir($dir)))
    {
      if (( $file != '.' ) && ( $file != '..' ))
      {
        if ( is_dir($src.'/'.$file) )
        {
          recurse_copy($src.'/'.$file, $dst.'/'.$file);
        }
        else
        {
          copy($src.'/'.$file, $dst.'/'.$file);
        }
      }
    }
    closedir($dir);
}
function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    $str = preg_replace("/[^[:alnum:].]/u", '_', $str); // on ne garde que les char alpha_num
    return $str;
}
function free_path($fic, $isdir=false)
{
  $fic = dirname($fic)."/".wd_remove_accents(basename($fic));
  if (!file_exists($fic)) return $fic;
  if ($isdir)
  {
    $p1 = $fic;
    $p2 = "";
  }
  else
  {
    $p1 = substr($fic, 0, -4);
    $p2 = substr($fic, -4);
  }
  $i = 1;
  while (file_exists($p1."_".$i.$p2))
  {
    $i++;
  }
  return $p1."_".$i.$p2;
}
function livre_creation($cat, $livre)
{
  // si le livre n'existe pas encore, il faut en créer un nouveau
  $dos = "../livres/$cat/$livre";
  if (!file_exists($dos))
  {
    $livre = basename(free_path($dos, true));
    //on l'ajoute à la liste des livres
    $txt = file_get_contents("../livres/$cat/liste.txt");
    if ($txt != "") $txt .= "\n";
    $txt .= $livre;
    file_put_contents("../livres/$cat/liste.txt", $txt);
    $dos = "../livres/$cat/$livre";
  }
  //on crée tout ce qu'il manque
  if (!file_exists("$dos")) mkdir("$dos", 0777, true);
  if (!file_exists("$dos/img")) mkdir("$dos/img", 0777, true);
  if (!file_exists("$dos/sons")) mkdir("$dos/sons", 0777, true);
  if (!file_exists("$dos/videos")) mkdir("$dos/videos", 0777, true);
  if (!file_exists("$dos/version")) copy("../VERSION", "$dos/version");
  
  return $livre;
}
  
  //il faut au minimum la cat et le livre
  if (!isset($_GET['cat']) || !isset($_GET['livre']))
  {
    echo "L'url semble incomplete...";
    exit;
  }
  $cat = $_GET['cat'];
  $livre = $_GET['livre'];
  //création de livre...
  if (isset($_GET['action']) && $_GET['action'] == "new")
  {
    if (!$test) $livre = livre_creation($cat, urldecode($_GET['livre']));
    else
    {
      echo "mode TEST : création de livre impossible !";
      exit;
    }
  }
  
  $dos = "../livres/$cat/$livre";
  if (!$test)
  {
    if (!file_exists("$dos")) mkdir("$dos", 0777, true);
    if (!file_exists("$dos/videos")) mkdir("$dos/videos", 0777, true);
  }
  else if (!file_exists("$dos") || !file_exists("$dos/videos"))
  {
    echo "mode TEST : il manque des dossiers !";
    exit;
  }

  //si la liste n'existe pas, on la crée
  if (!file_exists("$dos/liste.txt") && !$test)
  {
    $vv = myglob("$dos/img" , "fic");
    $txt = "";
    for ($i=0; $i<count($vv); $i++)
    {
      if ($txt != "") $txt .= "\n";
      $txt .= basename($vv[$i]);
    }
    file_put_contents("$dos/liste.txt", $txt);
  }
  
  //on traite d'abord des actions à faire
  if (isset($_GET['action']) && isset($_GET['exo']))
  {
    if (!$test)
    {
      $exo = $_GET['exo'];
      $dos = "../livres/$cat";
      $dos1 = "$dos/$livre";
      $dos .= "/$livre/img";
      $dos2 = "$dos/$exo";
      $fimg = "";
      if (file_exists($dos2.".jpg")) $fimg = $dos2.".jpg";
      else if (file_exists($dos2.".png")) $fimg = $dos2.".png";
      else if (file_exists($dos2.".svg")) $fimg = $dos2.".svg";
      
      switch ($_GET['action'])
      {
        case "remove":
          if (file_exists($fimg)) unlink($fimg);
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $txt = "";
          for ($i=0; $i<count($exos); $i++)
          {
            if ($exos[$i] != $exo)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
          }
          file_put_contents("$dos1/liste.txt", $txt);
          break;
        case "up":
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $pos = array_search($exo, $exos);
          if ($pos > 0)
          {
            $txt = "";
            for ($i=0; $i<$pos-1; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $exo;
            $txt .= "\n".$exos[$pos-1];
            for ($i=$pos+1; $i<count($exos); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            file_put_contents("$dos1/liste.txt", $txt);
          }
          break;
        case "down":
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $pos = array_search($exo, $exos);
          if ($pos < count($exos)-1)
          {
            $txt = "";
            for ($i=0; $i<$pos; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $exos[$pos+1];
            $txt .= "\n".$exo;          
            for ($i=$pos+2; $i<count($exos); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            file_put_contents("$dos1/liste.txt", $txt);
          }
          break;
        case "saveimg":
          $nbf = count($_FILES['iimg']['name']);
          for ($i=0; $i<$nbf; $i++)
          {
            $dest = $_FILES['iimg']['name'][$i];
            $dest = free_path("$dos1/img/{$dest}");
            copy($_FILES['iimg']['tmp_name'][$i], $dest);
            if (strlen($dest)<=9 || substr($dest,-9,5) != ".corr")
              file_put_contents("$dos1/liste.txt", "\n".substr(basename($dest), 0, -4), FILE_APPEND);
          }
          break;
        case "savecimg":
          $dest = $dos1."/img/".$exo.".corr".substr($_FILES['iimg']['name'],-4);
          if (file_exists($dest)) unlink($dest);
          copy($_FILES['iimg']['tmp_name'], $dest);
          break;
      }
    }
    else
    {
      echo "mode TEST : impossible d'effectuer l'action : ".$_GET['action']." !";
    }
  }
  if (isset($_POST['action']))
  {
    if (!$test)
    {
      $dos = "../livres/$cat";
      $dos1 = "$dos/$livre";
      
      switch ($_POST['action'])
      {
        case "add_text":
          if (!isset($_POST['items'])) break;
          $items = explode(";", $_POST['items']);
          $nbf = count($items);
          for ($i=0; $i<$nbf; $i++)
          {
            if (trim($items[$i]) != "")
            {
              file_put_contents("$dos1/liste.txt", "\n".trim($items[$i]), FILE_APPEND);
            }
          }
          break;
      }
    }
    else
    {
      echo "mode TEST : impossible d'effectuer l'action : ".$_GET['action']." !";
    }
  }
  
  $titre = $_GET['livre'];
  $dos = "../livres/$cat/$livre";
  $aut = "";
  $coul = "#6D7BCF";
  $details = "";
  $notes = "";
  $img = "";
  $essais = 1;
  //on essaie de lire le fichier descriptif
  if (file_exists("$dos/livre.txt"))
  {
    $infos = explode("\n", file_get_contents("$dos/livre.txt"));
    if (count($infos)>0) $titre = $infos[0];
    if (count($infos)>1) $coul = $infos[1];
    if (count($infos)>2) $aut = $infos[2];
    if (count($infos)>3) $img = $infos[3];
    if (count($infos)>4) $notes = str_replace("ⱡ", "\n", $infos[4]);
    if (count($infos)>5) $essais = $infos[5];
    for ($i=11; $i<count($infos); $i++)
    {
      if ($i>11) $details .= "\n";
      $details .= $infos[$i];
    }
  }
  else if (!$test)
  {
    file_put_contents("$dos/livre.txt", "$titre\n$coul\n\n\n\n1\n\n\n\n\n");
  }
  else
  {
    echo "mode TEST : impossible de créer le fichier livre.txt !";
    exit;
  }
  
  //on s'occupe de la visibilité
  $hidden = array();
  if (file_exists("$dos/hide.txt")) $hidden = explode("\n", file_get_contents("$dos/hide.txt"));
  else file_put_contents("$dos/hide.txt", "");
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- livres</title>
  <link rel="stylesheet" href="livre.css">
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <script type="text/javascript" src="../libs/jscolor.min.js"></script>
  <script type="text/javascript" src="livre.js"></script>
</head>

<body onload="start('<?php echo $aut; ?>')">
  <div id="bandeau">
    <div id="bandeau2">
      <a href="cat.php?cat=<?php echo $cat ?>"><img class="bimg" src="../icons/edit-undo.svg" title="Retour à la catégorie"/></a>
    </div>
    <a href="index.php"><img class="bimg" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg2" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>  
<?php
    echo "<div class=\"col\">\n";
    echo "<div class=\"titre\">infos livre</div>\n";
    echo "<table>";
    echo "<tr>";
    echo "<td class=\"td1\">Titre du livre</td>";
    echo "<td class=\"td2\"><input type=\"text\" id=\"ititre\" size=\"30\" value=\"$titre\" onchange=\"infos_change('$cat', '$livre', true)\"/></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Auteur</td>";
    echo "<td class=\"td2\"><input type=\"text\" id=\"iaut\" size=\"30\" value=\"$aut\" onchange=\"infos_change('$cat', '$livre', true)\"/></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Couleur de fond</td>";
    echo "<td class=\"td2\"><input class=\"jscolor {hash:true}\" type=\"text\" id=\"icoul\" value=\"$coul\" onchange=\"infos_change('$cat', '$livre', true)\" /></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Nombre d'essais</td>";
    echo "<td class=\"td2\"><input type=\"number\" min='0' max='10' id=\"iessais\" value=\"$essais\" onchange=\"infos_change('$cat', '$livre', true)\" /> 0 pour aucune limite</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Détails</td>";
    echo "<td class=\"td2\"><textarea id=\"idetails\" onchange=\"infos_change('$cat', '$livre', true)\">$details</textarea></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Annotations<br/>diverses</td>";
    echo "<td class=\"td2\"><textarea id=\"inotes\" onchange=\"infos_change('$cat', '$livre', true)\">$notes</textarea></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Visibilité</td>";
    echo "<td class=\"td2 groupes\">";
    //case "personne" (hide = ALL)
    echo "<span class=\"visi\"><input onchange=\"livre_visi_change('$cat', 'livre', this, true)\" id=\"visi__ALL_\" type=\"checkbox\" value=\"_ALL_\" ";
    if (in_array("_ALL_", $hidden)) echo "checked ";
    echo "/><label for=\"visi__ALL_\">PERSONNE</label></span>";
    
    //et les cases de chaque groupe
    if (file_exists("../groupes.txt"))
    {
      $gg = explode("\n", file_get_contents("../groupes.txt"));
      for ($i=0; $i<count($gg); $i++)
      {
        $vv = explode("|", $gg[$i]);
        if (count($vv)>2)
        {
          echo "<span class=\"visi\"><input onchange=\"livre_visi_change('$cat', '$livre', this, true)\" id=\"visi_$vv[2]\" type=\"checkbox\" value=\"$vv[2]\" ";
          if (!in_array($vv[2], $hidden)) echo "checked ";
          echo "/><label for=\"visi_$vv[2]\">$vv[0]</label></span>";
        }
      }
    }
    echo "</td>";
    echo "</tr>";
    echo "</table>";
    echo "</div>\n";
    echo "<div class=\"col\">\n";
    echo "  <div class=\"titre\">photos modèles</div>\n";
    echo "<div class='photo_liste'>";
    $exos = explode("\n", file_get_contents("$dos/liste.txt"));
    for ($i=0; $i<count($exos); $i++)
    {
      //on lit les détails de l'exo
      $fimg = "";
      if (file_exists("$dos/img/$exos[$i].jpg")) $fimg = "$dos/img/$exos[$i].jpg";
      else if (file_exists("$dos/img/$exos[$i].png")) $fimg = "$dos/img/$exos[$i].png";
      else if (file_exists("$dos/img/$exos[$i].svg")) $fimg = "$dos/img/$exos[$i].svg";
      $fcorr = "";
      if (file_exists("$dos/img/$exos[$i].corr.jpg")) $fcorr = "$dos/img/$exos[$i].corr.jpg";
      else if (file_exists("$dos/img/$exos[$i].corr.png")) $fcorr = "$dos/img/$exos[$i].corr.png";
      else if (file_exists("$dos/img/$exos[$i].corr.svg")) $fcorr = "$dos/img/$exos[$i].corr.svg";
      
      if ($fimg != "" || trim($exos[$i]) != "")
      {
        if ($fimg != "") echo "<div><img class='photo_item' src='$fimg' />\n";
        else if (trim($exos[$i]) != "") echo "<div><span class='photo_item'>".trim($exos[$i])."</span>";

        if (file_exists($fcorr)) echo "<img class='photo_corr' src='$fcorr' />";
        
        echo "<div class='photo_actions'><a href=\"javascript:photo_supprime('$cat', '$livre', '$exos[$i]')\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer l'exo\"/></a><br>";
        echo "<a href=\"photo.php?cat=$cat&livre=$livre&exo=$exos[$i]&action=up\"><img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter l'exo\"/></a><br>";
        echo "<a href=\"photo.php?cat=$cat&livre=$livre&exo=$exos[$i]&action=down\"><img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre l'exo\"/></a><br>";
        echo "<form class='photo_cimg_form' action='photo.php?cat=$cat&livre=$livre&action=savecimg&exo=$exos[$i]' method=\"POST\" enctype=\"multipart/form-data\">";
        echo "<label for='photo_cimg_$i'><img class='eimg' src='../icons/add_image_corr.svg' title='Ajouter des corrections images'/></label>";
        echo "<input type=\"file\" accept=\".jpg,.png,.svg\" id=\"photo_cimg_$i\" name=\"iimg\" onchange=\"infos_img_change(this)\"/></form>";
        echo "</div></div>";
      }
    }
    echo "</div>";
    echo "<form id=\"photo_iimg_form\" action=\"photo.php?cat=$cat&livre=$livre&action=saveimg&exo=\" method=\"POST\" enctype=\"multipart/form-data\">";
    echo "<label for='photo_iimg'><img class='photo_add_icone' src='../icons/add_image.svg' title='Ajouter des items images'/></label>";
    echo "<input type=\"file\" accept=\".jpg,.png,.svg\" multiple=\"1\" id=\"photo_iimg\" name=\"iimg[]\" onchange=\"infos_img_change(this)\"/></form>";
    echo "<img class='photo_add_icone' src='../icons/add_texte.svg' title='Ajouter des items textes' onclick='photo_add_text_item()' />";
    echo "</div>";
    echo "</div>\n";
?>
  <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
