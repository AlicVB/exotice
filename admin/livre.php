<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

include("../core/maj.php");
$test = (file_exists("../TEST"));
  
function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}

function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ($file = readdir($dir)))
    {
      if (( $file != '.' ) && ( $file != '..' ))
      {
        if ( is_dir($src.'/'.$file) )
        {
          recurse_copy($src.'/'.$file, $dst.'/'.$file);
        }
        else
        {
          copy($src.'/'.$file, $dst.'/'.$file);
        }
      }
    }
    closedir($dir);
}
function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    $str = preg_replace("/[^[:alnum:].]/u", '_', $str); // on ne garde que les char alpha_num
    return $str;
}
function free_path($fic, $isdir=false)
{
  $fic = dirname($fic)."/".wd_remove_accents(basename($fic));
  if (!file_exists($fic)) return $fic;
  if ($isdir)
  {
    $p1 = $fic;
    $p2 = "";
  }
  else
  {
    $p1 = substr($fic, 0, -4);
    $p2 = substr($fic, -4);
  }
  $i = 1;
  while (file_exists($p1."_".$i.$p2))
  {
    $i++;
  }
  return $p1."_".$i.$p2;
}
function livre_creation($cat, $livre)
{
  // si le livre n'existe pas encore, il faut en créer un nouveau
  $dos = "../livres/$cat/$livre";
  if (!file_exists($dos))
  {
    $livre = basename(free_path($dos, true));
    //on l'ajoute à la liste des livres
    $txt = file_get_contents("../livres/$cat/liste.txt");
    if ($txt != "") $txt .= "\n";
    $txt .= $livre;
    file_put_contents("../livres/$cat/liste.txt", $txt);
    $dos = "../livres/$cat/$livre";
  }
  //on crée tout ce qu'il manque
  if (!file_exists("$dos")) mkdir("$dos", 0777, true);
  if (!file_exists("$dos/exos")) mkdir("$dos/exos", 0777, true);
  if (!file_exists("$dos/img")) mkdir("$dos/img", 0777, true);
  if (!file_exists("$dos/sons")) mkdir("$dos/sons", 0777, true);
  if (!file_exists("$dos/videos")) mkdir("$dos/videos", 0777, true);
  if (!file_exists("$dos/version")) copy("../VERSION", "$dos/version");
  
  return $livre;
}
  
  //il faut au minimum la cat et le livre
  if (!isset($_GET['cat']) || !isset($_GET['livre']))
  {
    echo "L'url semble incomplete...";
    exit;
  }
  $cat = $_GET['cat'];
  $livre = $_GET['livre'];
  //création de livre...
  if (isset($_GET['action']) && $_GET['action'] == "new")
  {
    if (!$test) $livre = livre_creation($cat, urldecode($_GET['livre']));
    else
    {
      echo "mode TEST : création de livre impossible !";
      exit;
    }
  }
  
  $dos = "../livres/$cat/$livre";
  if (!$test)
  {
    if (!file_exists("$dos")) mkdir("$dos", 0777, true);
    if (!file_exists("$dos/videos")) mkdir("$dos/videos", 0777, true);
  }
  else if (!file_exists("$dos") || !file_exists("$dos/videos"))
  {
    echo "mode TEST : il manque des dossiers !";
    exit;
  }

  //si la liste n'existe pas, on la crée
  if (!file_exists("$dos/liste.txt") && !$test)
  {
    $vv = myglob("$dos/exos" , "dos");
    $txt = "";
    for ($i=0; $i<count($vv); $i++)
    {
      if ($txt != "") $txt .= "\n";
      $txt .= basename($vv[$i]);
    }
    file_put_contents("$dos/liste.txt", $txt);
  }
  
  //on traite d'abord des actions à faire
  if (isset($_GET['action']) && isset($_GET['exo']))
  {
    if (!$test)
    {
      $exo = $_GET['exo'];
      $dos = "../livres/$cat";
      $dos1 = "$dos/$livre";
      $dos .= "/$livre/exos";
      $dos2 = "$dos/$exo";
      
      switch ($_GET['action'])
      {
        case "remove":
          if (file_exists($dos2)) recursiveRemoveDirectory($dos2);
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $txt = "";
          for ($i=0; $i<count($exos); $i++)
          {
            if ($exos[$i] != $exo)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
          }
          file_put_contents("$dos1/liste.txt", $txt);
          break;
        case "rmimg":
          if (file_exists("$dos1/{$_GET['exo']}")) unlink("$dos1/{$_GET['exo']}");
          $infos = explode("\n", file_get_contents("$dos1/livre.txt"));
          if (count($infos)>3) $infos[3] = "";
          file_put_contents("$dos1/livre.txt", implode("\n", $infos));
          break;
        case "saveimg":
          $dest = free_path("$dos1/img/{$_FILES['iimg']['name']}");
          copy($_FILES['iimg']['tmp_name'], $dest);
          $infos = explode("\n", file_get_contents("$dos1/livre.txt"));
          if (count($infos)>3) $infos[3] = "img/".basename($dest);
          file_put_contents("$dos1/livre.txt", implode("\n", $infos));
          break;
        case "copie":
          $n = 1;
          while (file_exists("$dos/$n")) $n++;
          recurse_copy($dos2, "$dos/$n");
          $txt = file_get_contents("$dos1/liste.txt");
          if ($txt != "") $txt .= "\n";
          $txt .= "$n";
          file_put_contents("$dos1/liste.txt", $txt);
          break;
        case "up":
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $pos = array_search($exo, $exos);
          if ($pos > 0)
          {
            $txt = "";
            for ($i=0; $i<$pos-1; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $exo;
            $txt .= "\n".$exos[$pos-1];
            for ($i=$pos+1; $i<count($exos); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            file_put_contents("$dos1/liste.txt", $txt);
          }
          break;
        case "down":
          $exos = explode("\n", file_get_contents("$dos1/liste.txt"));
          $pos = array_search($exo, $exos);
          if ($pos < count($exos)-1)
          {
            $txt = "";
            for ($i=0; $i<$pos; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $exos[$pos+1];
            $txt .= "\n".$exo;          
            for ($i=$pos+2; $i<count($exos); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $exos[$i];
            }
            file_put_contents("$dos1/liste.txt", $txt);
          }
          break;
      }
    }
    else
    {
      echo "mode TEST : impossible d'effectuer l'action : ".$_GET['action']." !";
    }
  }
  
  $titre = $_GET['livre'];
  $dos = "../livres/$cat/$livre";
  $aut = "";
  $coul = "#6D7BCF";
  $details = "";
  $notes = "";
  $img = "";
  //on essaie de lire le fichier descriptif
  if (file_exists("$dos/livre.txt"))
  {
    $infos = explode("\n", file_get_contents("$dos/livre.txt"));
    if (count($infos)>0) $titre = $infos[0];
    if (count($infos)>1) $coul = $infos[1];
    if (count($infos)>2) $aut = $infos[2];
    if (count($infos)>3) $img = $infos[3];
    if (count($infos)>4) $notes = str_replace("ⱡ", "\n", $infos[4]);
    for ($i=11; $i<count($infos); $i++)
    {
      if ($i>11) $details .= "\n";
      $details .= $infos[$i];
    }
  }
  else if (!$test)
  {
    file_put_contents("$dos/livre.txt", "$titre\n$coul\n\n\n\n\n\n\n\n\n");
  }
  else
  {
    echo "mode TEST : impossible de créer le fichier livre.txt !";
    exit;
  }
  
  //on s'occupe de la visibilité
  $hidden = array();
  if (file_exists("$dos/hide.txt")) $hidden = explode("\n", file_get_contents("$dos/hide.txt"));
  else file_put_contents("$dos/hide.txt", "");
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- livres</title>
  <link rel="stylesheet" href="livre.css">
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <script type="text/javascript" src="../libs/jscolor.min.js"></script>
  <script type="text/javascript" src="livre.js"></script>
</head>

<body onload="start('<?php echo $aut; ?>')">
  <div id="bandeau">
    <div id="bandeau2">
      <a href="cat.php?cat=<?php echo $cat ?>"><img class="bimg" src="../icons/edit-undo.svg" title="Retour à la catégorie"/></a>
    </div>
    <a href="index.php"><img class="bimg" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg2" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>

<?php
    echo "<div class=\"col\">\n";
    echo "<div class=\"titre\">infos livre</div>\n";
    echo "<table>";
    echo "<tr>";
    echo "<td class=\"td1\">Titre du livre</td>";
    echo "<td class=\"td2\"><input type=\"text\" id=\"ititre\" size=\"30\" value=\"$titre\" onchange=\"infos_change('$cat', '$livre')\"/></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Auteur</td>";
    echo "<td class=\"td2\"><input type=\"text\" id=\"iaut\" size=\"30\" value=\"$aut\" onchange=\"infos_change('$cat', '$livre')\"/></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Couleur de fond</td>";
    echo "<td class=\"td2\"><input class=\"jscolor {hash:true}\" type=\"text\" id=\"icoul\" value=\"$coul\" onchange=\"infos_change('$cat', '$livre')\" /></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Détails</td>";
    echo "<td class=\"td2\"><textarea id=\"idetails\" onchange=\"infos_change('$cat', '$livre')\">$details</textarea></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Image principale</td>";
    echo "<td class=\"td2\">";
    if ($img != "" && file_exists("$dos/$img")) echo "<img id=\"iimg\" src=\"$dos/$img\" title=\"$dos/$img\" onload=\"iimg_load(this)\"/><a href=\"livre.php?cat=$cat&livre=$livre&action=rmimg&exo=$img\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer l'image\"/></a>";
    else echo "<form id=\"iimg_form\" action=\"livre.php?cat=$cat&livre=$livre&action=saveimg&exo=\" method=\"POST\" enctype=\"multipart/form-data\"><input type=\"file\" id=\"iimg\" name=\"iimg\" onchange=\"infos_img_change(this)\"/></form>";
    echo "</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Annotations<br/>diverses</td>";
    echo "<td class=\"td2\"><textarea id=\"inotes\" onchange=\"infos_change('$cat', '$livre')\">$notes</textarea></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td class=\"td1\">Visibilité</td>";
    echo "<td class=\"td2 groupes\">";
    //case "personne" (hide = ALL)
    echo "<span class=\"visi\"><input onchange=\"livre_visi_change('$cat', 'livre', this, true)\" id=\"visi__ALL_\" type=\"checkbox\" value=\"_ALL_\" ";
    if (in_array("_ALL_", $hidden)) echo "checked ";
    echo "/><label for=\"visi__ALL_\">PERSONNE</label></span>";
    
    //et les cases de chaque groupe
    if (file_exists("../groupes.txt"))
    {
      $gg = explode("\n", file_get_contents("../groupes.txt"));
      for ($i=0; $i<count($gg); $i++)
      {
        $vv = explode("|", $gg[$i]);
        if (count($vv)>2)
        {
          echo "<span class=\"visi\"><input onchange=\"livre_visi_change('$cat', '$livre', this, true)\" id=\"visi_$vv[2]\" type=\"checkbox\" value=\"$vv[2]\" ";
          if (!in_array($vv[2], $hidden)) echo "checked ";
          echo "/><label for=\"visi_$vv[2]\">$vv[0]</label></span>";
        }
      }
    }
    echo "</td>";
    echo "</tr>";
    echo "</table>";
    echo "</div>\n";
    echo "<div class=\"col\">\n";
    echo "  <div class=\"titre\">exercices</div>\n";
    echo "<table>";
    $exos = explode("\n", file_get_contents("$dos/liste.txt"));
    for ($i=0; $i<count($exos); $i++)
    {
      //on lit les détails de l'exo
      if (file_exists("$dos/exos/$exos[$i]/exo.txt"))
      {
        $exo = $exos[$i];
        $et = "";
        $econs = "";
        $ecoul = "transparent";
        $infos = explode("\n", file_get_contents("$dos/exos/$exos[$i]/exo.txt"));
        if (count($infos)>0) $et = $infos[0];
        if (count($infos)>1) $econs = $infos[1];
        if (count($infos)>10) $ecoul = $infos[10];
        // on écrit le bloc qui correspond
        echo "<tr style=\"background-color:$ecoul;\">\n";
        echo "<td class=\"td2\"><a class=\"ea\" href=\"crea_exo/infos.php?cat=$cat&livre=$livre&exo=$exo\" title=\"$cat/$livre/exos/$exo\">";
        echo "<b>$et</b></a></td>\n";
        echo "<td class=\"td3\"><a href=\"crea_exo/infos.php?cat=$cat&livre=$livre&exo=$exo\"><img class=\"eimg\" src=\"../icons/dialog-information.svg\" title=\"infos générales de l'exercice\"/></a>";
        echo "<a href=\"crea_exo/exo.php?cat=$cat&livre=$livre&exo=$exo\"><img class=\"eimg\" src=\"../icons/application-x-diagram.svg\" title=\"mise en page de l'exercice\"/></a>";
        echo "</td>";
        echo "<td class=\"td3\"><a href=\"javascript:exo_supprime('$cat', '$livre', '$exo')\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer l'exo\"/></a>\n";
        echo "<a href=\"livre.php?cat=$cat&livre=$livre&exo=$exo&action=copie\"><img class=\"eimg\" src=\"../icons/tab-new.svg\" title=\"copier l'exo\"/></a>\n";
        echo "<a href=\"livre.php?cat=$cat&livre=$livre&exo=$exo&action=up\"><img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter l'exo\"/></a>\n";
        echo "<a href=\"livre.php?cat=$cat&livre=$livre&exo=$exo&action=down\"><img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre l'exo\"/></a>\n";
        
         //on regarde la visibilité
        $hide = 0;
        if (file_exists("$dos/exos/$exo/hide.txt"))
        {
          $vv = explode("\n", file_get_contents("$dos/exos/$exo/hide.txt"));
          for ($j=0; $j<count($vv); $j++)
          {
            if (trim($vv[$j]) == "_ALL_") $hide = 2;
            else if (trim($vv[$j]) != "") $hide = 1;
            else continue;
            break;
          }
        }
        if ($hide == 0) echo "<img value=\"0\" onclick=\"exo_visi_switch(this, '$cat', '$livre', '$exo')\" class=\"eimg\" src=\"../icons/show_ok.svg\" title=\"visible pour tous -- cliquer pour masquer à tous\"/>";
        else if ($hide == 1) echo "<img onclick=\"exo_visi_switch(this, '$cat', '$livre', '$exo')\" value=\"1\" class=\"eimg\" src=\"../icons/show_semi.svg\" title=\"visible pour certains -- cliquer pour rendre visible à tous\"/>";
        else echo "<img onclick=\"exo_visi_switch(this, '$cat', '$livre', '$exo')\" value=\"2\" class=\"eimg\" src=\"../icons/show_no.svg\" title=\"visible pour personne -- cliquer pour rendre visible à tous\"/>";
        
        echo "</td></tr>\n";
      }
    }
    echo "</table>";
    echo "<div class=\"enew\"><button onclick=\"window.location.href='crea_exo/infos.php?cat=$cat&livre=$livre&exo='\">nouvel exercice</button>&nbsp;&nbsp;";
    echo "<select onchange=\"add_select(this, '$dos')\">";
    echo "<option value=\"\" selected>exercice existant...</option>";
    $icats = explode("\n", file_get_contents("../livres/liste.txt"));
    for ($i=0; $i<count($icats); $i++)
    {
      $icat = $icats[$i];
      if (file_exists("../livres/$icats[$i]/cat.txt"))
      {
        $v = explode("\n", file_get_contents("../livres/$icats[$i]/cat.txt"));
        $icat = $v[0];
        $ilivres = explode("\n", file_get_contents("../livres/$icats[$i]/liste.txt"));
        for ($j=0; $j<count($ilivres); $j++)
        {
          $ilivre = $ilivres[$j];
          if (file_exists("../livres/$icats[$i]/$ilivres[$j]/livre.txt"))
          {
            $v = explode("\n", file_get_contents("../livres/$icats[$i]/$ilivres[$j]/livre.txt"));
            $ilivre = $v[0];
            $iexos = explode("\n", file_get_contents("../livres/$icats[$i]/$ilivres[$j]/liste.txt"));
            for ($k=0; $k<count($iexos); $k++)
            {
              $iexo = $iexos[$k];
              if (file_exists("../livres/$icats[$i]/$ilivres[$j]/exos/$iexos[$k]/exo.txt"))
              {
                $v = explode("\n", file_get_contents("../livres/$icats[$i]/$ilivres[$j]/exos/$iexos[$k]/exo.txt"));
                $iexo = $v[0];
                echo "<option value=\"../livres/$icats[$i]/$ilivres[$j]/exos/$iexos[$k]\">$icat - $ilivre - $iexo</option>";
              }          
            }
          }
        }
      }

    }
    echo "</select>";
    echo "</div>";
    echo "</div>\n";
?>
  <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
