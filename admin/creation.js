 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

// initialisation de tout ce qu'il faut
function start()
{
  //on enlève l'action de l'url
  if (window.location.href.lastIndexOf("&action=") != -1)
  {
    let pos = window.location.href.lastIndexOf("&action=");
    history.replaceState('suppr action', document.title, window.location.href.substr(0, pos));
  }
}

//suppression de catégorie (demande de confirmation)
function cat_supprime(cat)
{
  let r = confirm("Etes-vous sûr(e) de vouloir supprimer la catégorie " + cat + " ?");
  if (r == true) window.location.assign("creation.php?cat=" + cat + "&action=remove");
}

//création d'une nouvelle catégorie (demande du nom)
function cat_new(cat)
{
  let txt = prompt("nom de la catégorie", "");
  if (!txt || txt == "") return;

  window.location.assign("cat.php?cat=" + encodeURIComponent(txt.replace(/\//g, "_")) + "&action=new");
}

//bascule de la visibilité de la catégorie
function cat_visi_switch(e, cat)
{
  let tx = "";
  if (e.getAttribute("value") == "0") tx = "_ALL_";

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (e.getAttribute("value") == "0")
      {
        e.src = "../icons/show_no.svg";
        e.title = "visible pour personne -- cliquer pour rendre visible à tous";
        e.setAttribute("value", "2");
      }
      else
      {
        e.src = "../icons/show_ok.svg";
        e.title = "visible pour tous -- cliquer pour masquer à tous";
        e.setAttribute("value", "0");
      }
    }
  };
  let ligne = "hidden&cat=" + cat + "&v=" + tx;
  xhr.open("POST", "livre_sauve.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
