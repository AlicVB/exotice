<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  $test = (file_exists("../TEST"));

  if ($test)
  {
    echo "*action impossible en mode test !";
    exit;
  }
  

function recursiveRemoveDirectory($directory)
{
    foreach(glob("{$directory}/*") as $file)
    {
        if(is_dir($file)) { 
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}
function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    $str = preg_replace("/[^[:alnum:]]/u", '_', $str); // on ne garde que les char alpha_num
    return $str;
}
function free_name($nom, $liste)
{
  $ret = wd_remove_accents($nom);
  $j = 0;
  $ok = false;
  while (!$ok)
  {
    $ok = true;
    $n = $ret;
    if ($j != 0) $n .= "_$j";
    for ($i=0; $i<count($liste); $i++)
    {
      if ($liste[$i] == $n)
      {
        $ok=false;
        $j++;
        break;
      }
    }
  }
  if ($j == 0) return $ret;
  else return $ret."_".$j;
}


  $fic = "../utilisateurs.txt";  
  
  if (isset($_POST['change']))
  {
    if (isset($_POST['user']) && isset($_POST['quoi']) && isset($_POST['val']))
    {
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $txt = "";
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          $vvv[] = $vvv[0];
        }
        if (count($vvv) == 4)
        {
          if ($vvv[3] == $_POST['user'])
          {
            if ($_POST['quoi'] == "nom") $vvv[0] = $_POST['val'];
            else if ($_POST['quoi'] == "groupe") $vvv[1] = $_POST['val'];
            else if ($_POST['quoi'] == "couleur") $vvv[2] = $_POST['val'];
          }
          $txt .= $vvv[0]."|".$vvv[1]."|".$vvv[2]."|".$vvv[3]."\n";
        }
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt, LOCK_EX);
    }
  }
  
  if (isset($_POST['supprime']))
  {
    if (isset($_POST['user']))
    {
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $txt = "";
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          $vvv[] = $vvv[0];
        }
        if (count($vvv) == 4)
        {
          if ($vvv[3] != $_POST['user'])
          {
            $txt .= $vvv[0]."|".$vvv[1]."|".$vvv[2]."|".$vvv[3]."\n";
          }          
        }
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt, LOCK_EX);
      
      //on supprime le log si il existe
      if (file_exists("../log_exo/".$_POST['user'])) recursiveRemoveDirectory("../log_exo/".$_POST['user']);
    }
  }
  
  if (isset($_POST['move']))
  {
    if (isset($_POST['user']) && isset($_POST['sens']))
    {
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $users = array();
      $vv = explode("\n", file_get_contents($fic));
      $p = -1;
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          $vvv[] = $vvv[0];
        }
        if (count($vvv) == 4)
        {
          if ($vvv[3] == $_POST['user']) $p = $i;
          $users[] = $vvv[0]."|".$vvv[1]."|".$vvv[2]."|".$vvv[3]."\n";       
        }
      }
      //et on remet le fichier en ordre
      $txt = "";
      for ($i=0; $i<count($vv); $i++)
      {
        if ($_POST['sens'] == "up" && $i == $p-1) $txt .= $users[$p].$users[$i];
        else if ($_POST['sens'] == "down" && $i == $p+1) $txt .= $users[$i].$users[$p];
        else if ($i != $p) $txt .= $users[$i];
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt, LOCK_EX);
    }
  }

  if (isset($_POST['ajoute']))
  {
    if (isset($_POST['nom']) && isset($_POST['groupe']) && isset($_POST['couleur']))
    {
      // on lit le fichier utilisateurs
      $users = array();
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          $vvv[] = $vvv[0];
        }
        if (count($vvv) == 4)
        {
          $users[] = $vvv[3];
        }
      }
      // on  crée un identifiant unique
      $id = free_name($_POST['nom'], $users);
      $txt = $_POST['nom']."|".$_POST['groupe']."|".$_POST['couleur']."|".$id."\n";
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt, FILE_APPEND);
      
      //on supprime le log si il existe (ancien dossier)
      if (file_exists("../log_exo/$id")) recursiveRemoveDirectory("../log_exo/$id");
      echo $id;
    }
  }
  
//partie sur les groupes

  $fic = "../groupes.txt";  
  
  if (isset($_POST['grpe_change']))
  {
    if (isset($_POST['gid']) && isset($_POST['quoi']) && isset($_POST['val']))
    {
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $txt = "";
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          if ($vvv[2] == $_POST['gid'])
          {
            if ($_POST['quoi'] == "nom") $vvv[0] = $_POST['val'];
            else if ($_POST['quoi'] == "couleur") $vvv[1] = $_POST['val'];
          }
          $txt .= $vvv[0]."|".$vvv[1]."|".$vvv[2]."\n";
        }
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt);
    }
  }
  
  if (isset($_POST['grpe_supprime']))
  {
    if (isset($_POST['gid']))
    {
      //on vérifie qu'il n'y a pas d'utilisateur avec ce groupe
      $vv = explode("\n", file_get_contents("../utilisateurs.txt"));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) >=2)
        {
          if ($vvv[1] == $_POST['gid'])
          {
            echo "*Erreur* Il reste des utilisateurs dans ce groupe!";
            exit;
          }
        }
      }
      
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $txt = "";
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          if ($vvv[2] != $_POST['gid'])
          {
            $txt .= $vvv[0]."|".$vvv[1]."|".$vvv[2]."\n";
          }          
        }
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt);
    }
  }
  
  if (isset($_POST['grpe_move']))
  {
    if (isset($_POST['gid']) && isset($_POST['sens']))
    {
      //on lit le fichier en modifiant si besoin et en recréant le fichier texte
      $groupes = array();
      $vv = explode("\n", file_get_contents($fic));
      $p = -1;
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          if ($vvv[2] == $_POST['gid']) $p = $i;
          $groupes[] = $vvv[0]."|".$vvv[1]."|".$vvv[2]."\n";       
        }
      }
      //et on remet le fichier en ordre
      $txt = "";
      for ($i=0; $i<count($vv); $i++)
      {
        if ($_POST['sens'] == "up" && $i == $p-1) $txt .= $groupes[$p].$groupes[$i];
        else if ($_POST['sens'] == "down" && $i == $p+1) $txt .= $groupes[$i].$groupes[$p];
        else if ($i != $p) $txt .= $groupes[$i];
      }
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt);
    }
  }

  if (isset($_POST['grpe_ajoute']))
  {
    if (isset($_POST['nom']) && isset($_POST['couleur']))
    {
      // on lit le fichier utilisateurs
      $groupes = array();
      $vv = explode("\n", file_get_contents($fic));
      for ($i=0; $i<count($vv); $i++)
      {
        $vvv = explode("|", $vv[$i]);
        if (count($vvv) == 3)
        {
          $groupes[] = $vvv[2];
        }
      }
      // on  crée un identifiant unique
      $nm = $_POST['nom'];
      if ($nm == "_ALL_") $nm = "_ALL_1";
      if ($nm == "_TEST_") $nm = "_TEST_1";
      $id = free_name($_POST['nom'], $groupes);
      $txt = $_POST['nom']."|".$_POST['couleur']."|".$id."\n";
      
      //on réécrit le fichier utilisateurs
      file_put_contents($fic, $txt, FILE_APPEND);
      echo $id;
    }
  }
  
?>
