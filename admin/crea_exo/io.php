<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
$test = (file_exists("../../TEST"));

  header("Content-Type: text/plain"); // Utilisation d'un header pour spécifier le type de contenu de la page. Ici, il s'agit juste de texte brut (text/plain). 

function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '_', $str); // supprime les autres caractères
    
    return $str;
}

function free_path($fic)
{
  $fic = wd_remove_accents($fic);
  if (!file_exists($fic)) return $fic;
  $p1 = substr($fic, 0, -4);
  $p2 = substr($fic, -4);
  $i = 1;
  while (file_exists($p1."_".$i.$p2))
  {
    $i++;
  }
  return $p1."_".$i.$p2;
}



  $ret = "";
  if (isset($_GET["io"]) && isset($_GET["cat"]) && isset($_GET["livre"]))
  {
    if (!$test)
    {
      $io = $_GET["io"];
      $cat = $_GET["cat"];
      $livre = $_GET["livre"];
      $dos = "../../livres/$cat/$livre";
      if ($io == "sauveimg")
      {
        for ($i=0; $i<count($_FILES['cr_img_get']['name']); $i++)
        {
          if ("{$_FILES['cr_img_get']['error'][$i]}" == "1") $ret = "*Erreur : fichier trop gros !";
          else
          {
            $dest = free_path("$dos/img/{$_FILES['cr_img_get']['name'][$i]}");
            copy($_FILES['cr_img_get']['tmp_name'][$i], $dest);
            if (file_exists($dest)) $ret .= basename($dest)."\n";
            else $ret .= "*Erreur: la copie de l'image a échoué !\n";
          }
        }
      }
      else if ($io == "sauvevideo")
      {
        for ($i=0; $i<count($_FILES['cr_video_get']['name']); $i++)
        {
          if ("{$_FILES['cr_video_get']['error'][$i]}" == "1") $ret = "*Erreur : fichier trop gros !";
          else
          {
            if (!file_exists("$dos/videos")) mkdir("$dos/videos", 0777, true);
            $dest = free_path("$dos/videos/{$_FILES['cr_video_get']['name'][$i]}");
            copy($_FILES['cr_video_get']['tmp_name'][$i], $dest);
            if (file_exists($dest)) $ret .= basename($dest)."\n";
            else $ret .= "*Erreur: la copie de la vidéo a échoué !\n";
          }
        }
      }
      else if ($io == "sauveaudio")
      {
        for ($i=0; $i<count($_FILES['cr_audio_get']['name']); $i++)
        {
          if ("{$_FILES['cr_audio_get']['error'][$i]}" == "1") $ret = "*Erreur : fichier trop gros !";
          else
          {
            $dest = free_path("$dos/sons/{$_FILES['cr_audio_get']['name'][$i]}");
            //echo $_FILES['cr_audio_get']['tmp_name'][$i];
            copy($_FILES['cr_audio_get']['tmp_name'][$i], $dest);
            if (file_exists($dest)) $ret .= basename($dest)."\n";
            else $ret .= "*Erreur: la copie de l'audio a échoué !\n";
          }
        }
      }
      else if ($io == "sauveaudioblob")
      {
        $dest = free_path("$dos/sons/rec_".date("ymd_His").".ogg");
        $input = fopen('php://input', 'rb');
        $file = fopen($dest, 'wb');
        stream_copy_to_stream($input, $file);
        fclose($input);
        fclose($file);
        if (file_exists($dest)) $ret = basename($dest);
        else $ret = "*Erreur: l'enregistrement a échoué !";
      }
    }
    else $ret = "*Action impossible en mode test !";
  }
  else if (isset($_POST["fic"]) && isset($_POST["io"]) && isset($_POST["cat"]) && isset($_POST["livre"]) && isset($_POST["exo"])) 
  {
    $io = $_POST["io"];
    $fic = $_POST["fic"];
    $cat = $_POST["cat"];
    $livre = $_POST["livre"];
    $exo = $_POST["exo"];
    $dos = "../../livres/$cat/$livre/exos/$exo";
    $fn = "";
    if ($fic == "inc") $fn = "exo.inc.txt";
    else if ($fic == "sav") $fn = "exo_sav.txt";
    else if ($fic == "exo") $fn = "exo.txt";
    else if ($fic == "hide") $fn = "hide.txt";
    else if ($io == "sauvemodele" || $io == "chargemodele") $fn = $fic;
    else exit;
    if ($io == "charge")
    {
      if (!file_exists("$dos/$fn") && $fic == "exo")
      {
        $def = "basic";
        if (file_exists("default_modele.txt")) $def = file_get_contents("default_modele.txt");
        if (!file_exists("modeles/".$def)) $def = "basic";
        $fic = "modeles/".$def;
        if (file_exists($fic)) $ret = "****".file_get_contents($fic);
      }
      else if (file_exists("$dos/$fn")) $ret = file_get_contents("$dos/$fn");
    }
    else if ($io == "chargemodele")
    {
      if (file_exists("modeles/$fn")) $ret = file_get_contents("modeles/$fn");
    }
    else if ($io == "sauvemodele" && isset($_POST["v"]))
    {
      if (!$test)
      {
        $v = $_POST["v"];
        file_put_contents("modeles/$fn", $v);
      }
      else $ret = "*Action impossible en mode test !";
    }
    else if ($io == "sauve" && isset($_POST["v"]))
    {
      if (!$test)
      {
        $v = $_POST["v"];
        if (get_magic_quotes_gpc()) $v = stripslashes($v); //hack pour éviter que les guillemets soient échappés
        file_put_contents("$dos/$fn", $v);
        
        //attention dans le cas de l'export d'un exo, on a une autre valeur pour la sauvegarde
        if ($fic == "inc" && isset($_POST['v2']))
        {
          $v2 = $_POST["v2"];
          if (get_magic_quotes_gpc()) $v2 = stripslashes($v2); //hack pour éviter que les guillemets soient échappés
          file_put_contents("$dos/exo_sav.txt", $v2);
        }
        
        //on met à jour la version
        copy("../../VERSION", "$dos/version");
      }
      else $ret = "*Action impossible en mode test !";
    }
  }
  echo $ret;
?>
