 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

function cr_rendu_dragover(event)
{
  let r = document.getElementById("cr_rendu");
  if (event.dataTransfer.files && event.dataTransfer.files.length > 0)
  {
    event.preventDefault();
    r.style.borderColor = "red";
    return false;
  }
}
function cr_rendu_dragleave(event)
{
  let r = document.getElementById("cr_rendu");
  event.preventDefault();
  r.style.borderColor = "grey";
  return false;
}
function cr_rendu_drop(event)
{
  event.preventDefault();
  let r = document.getElementById("cr_rendu");
  r.style.borderColor = "grey";
  let dt = event.dataTransfer;
  if (!dt.files) return;
  
  let fdi = new FormData();
  let fds = new FormData();
  let nbi = 0;
  let nbs = 0;
  for (let i=0; i<dt.files.length; i++)
  {
    if (dt.files[i].type.startsWith("image/"))
    {
      fdi.append("cr_img_get[]", dt.files[i], dt.files[i].name);
      nbi++;
    }
    else if (dt.files[i].type.startsWith("audio/"))
    {
      fds.append("cr_audio_get[]", dt.files[i], dt.files[i].name);
      nbs++;
    }
  }
  selection = [];
  if (nbi > 0) cr_img_add_data(fdi);
  if (nbs > 0) cr_audio_add_data(fds);
}

function cr_keydown(event)
{
  if (event.target != document.body) return;
  //console.log(event.keyCode);
  let r = document.getElementById("cr_rendu");
  let mv = 5;
  let e = {};
  if (event.ctrlKey || event.shiftKey) mv = 20;
  switch (event.keyCode)
  {
    case 37: //flèche gauche
      event.preventDefault();
      for (let i=0; i<selection.length; i++)
      {
        let nmv = Math.max(0, parseFloat(selection[i].left) - mv) - parseFloat(selection[i].left);
        selection[i].left = parseFloat(selection[i].left) + nmv;
        rendu_get_superbloc(selection[i]).style.left = selection[i].left*100/443 + "%";
        if (selection[i].tpe == "ligne")
        {
          let elems = document.getElementsByClassName("extrema");
          for (let j=0; j<elems.length; j++)
          {
            elems[j].style.left = parseFloat(elems[j].style.left) + nmv*rendu.width/443 + "px";
          }
          selection[i].x2 += nmv;
        }
      }
      mv_place_pt();
      break;
    case 38: //flèche haut
      event.preventDefault();
      for (let i=0; i<selection.length; i++)
      {
        let nmv = Math.max(0, parseFloat(selection[i].top) - mv) - parseFloat(selection[i].top);
        selection[i].top = parseFloat(selection[i].top) + nmv;
        rendu_get_superbloc(selection[i]).style.top = selection[i].top*100/631 + "%";
        if (selection[i].tpe == "ligne")
        {
          let elems = document.getElementsByClassName("extrema");
          for (let j=0; j<elems.length; j++)
          {
            elems[j].style.top = parseFloat(elems[j].style.top) + nmv*rendu.height/631 + "px";
          }
          selection[i].y2 += nmv;
        }
      }
      mv_place_pt();
      break;
    case 39: //flèche droite
      event.preventDefault();
      for (let i=0; i<selection.length; i++)
      {
        let nmv = Math.min(r.offsetWidth*443/rendu.width - 10 - selection[i].width, parseFloat(selection[i].left) + mv) - parseFloat(selection[i].left);
        selection[i].left = parseFloat(selection[i].left) + nmv;
        rendu_get_superbloc(selection[i]).style.left = selection[i].left*100/443 + "%";
        if (selection[i].tpe == "ligne")
        {
          let elems = document.getElementsByClassName("extrema");
          for (let j=0; j<elems.length; j++)
          {
            elems[j].style.left = parseFloat(elems[j].style.left) + nmv*rendu.width/443 + "px";
          }
          selection[i].x2 += nmv;
        }
      }
      mv_place_pt();
      break;
    case 40: //flèche bas
      event.preventDefault();
      for (let i=0; i<selection.length; i++)
      {
        let nmv = Math.min(r.offsetHeight*631/rendu.height - 10 - selection[i].height, parseFloat(selection[i].top) + mv) - parseFloat(selection[i].top);
        selection[i].top = parseFloat(selection[i].top) + nmv;
        rendu_get_superbloc(selection[i]).style.top = selection[i].top*100/631 + "%";
        if (selection[i].tpe == "ligne")
        {
          let elems = document.getElementsByClassName("extrema");
          for (let j=0; j<elems.length; j++)
          {
            elems[j].style.top = parseFloat(elems[j].style.top) + nmv*rendu.height/631 + "px";
          }
          selection[i].y2 += nmv;
        }
      }
      mv_place_pt();
      break;
    case 46: //suppr
    case 8:  //retour arrière
      event.preventDefault();
      e.value = "2";
      cr_action_change(e);
      break;
    case 67: //C
      if (!event.ctrlKey) return;
      event.preventDefault();
      e.value = "1";
      cr_action_change(e);
      break;
    case 89: //Y
      if (!event.ctrlKey) return;
      event.preventDefault();
      g_restaurer_hist(1);
      return;
    case 90: //Z
      if (!event.ctrlKey) return;
      event.preventDefault();
      g_restaurer_hist(-1);
      return;
    case 65: //A
      if (!event.ctrlKey) return;
      event.preventDefault();
      selection = blocs;
      selection_change();
      return;
    case 83: //S
      if (!event.ctrlKey) return;
      event.preventDefault();
      g_exporter();
      return;
    default:
      return;
  }
  selection_update();
  g_sauver();
}

function cr_coul_nb_change(e, modif)
{
  let elems = document.getElementsByClassName('cr_coul');
  let nb = e.value;
  elems[0].style.display = 'block';
  for (let i=1; i<elems.length; i++)
  {
    if (i>nb) elems[i].style.display = 'none';
    else
    {
      elems[i].style.display = 'block';
      document.getElementById("cr_coul" + i + "_barre").style.display = "inline";
      document.getElementById("cr_coul" + i + "_maj").style.display = "inline";
      document.getElementById("cr_coul" + i + "_suff").style.display = "inline";
      document.getElementById("cr_coul" + i + "_suff_txt").style.display = "inline";
    }
  }
  
  //on met à jour le bloc si besoin
  if (modif)
  {
    cr_coul_change(null);
  }
}
function cr_img_add_data(fd)
{
  //on sauvegarde l'image sélectionnée
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      let vv = xhr.responseText.split("\n");
      for (let i=0; i<vv.length; i++)
      {
        if (vv[i].trim() == "") continue;
        let img_name = vv[i];
        if (img_name.startsWith("*"))
        {
          alert(img_name.substr(1));
          document.getElementById("cr_img_select").value = "";
          continue;
        }
        
        // et la liste des images
        let option = document.createElement("option");
        option.text = img_name;
        option.value = img_name;
        document.getElementById("cr_img_select").add(option);
        document.getElementById("cr_img_select").value = img_name;
        cr_img_add_to_cm(img_name);
        
        // on récupère le bloc sélectionné
        let bloc = null;
        if (i == 0 && selection.length > 0)
        {
          bloc = selection[0];
          if (bloc.tpe != "image") continue;
        }
        else
        {
          bloc = bloc_new("image", "");
        }
        //on récupère les chemins
        let rpath = "img/" + img_name;
        
        bloc.img_rpath = rpath;
        bloc.img_name = img_name;
        
        bloc_create_html(bloc);
        document.getElementById(bloc.id).onload = function () {
          //on met à jour les infos de hauteur
          let h = document.getElementById(bloc.id).getBoundingClientRect().height;
          rendu_get_superbloc(bloc).style.height = h*100/rendu.height + "%";
          bloc.height = h/rendu.height*631;
          document.getElementById("cr_tp_h").value = h/rendu.height*631;
          document.getElementById("cr_img_select").value = img_name;
          mv_place_pt();
          //on sauvegarde
          g_sauver();
          };
        // on met à jour le rendu
        document.getElementById(bloc.id).src = "../../livres/" + cat + "/" + livre + "/" + rpath;
      }
    }
  };
  // We setup our request
  xhr.open("POST", "io.php?io=sauveimg&cat=" + cat + "&livre=" + livre);
  xhr.send(fd);
}
function cr_img_get_change(e)
{
  let fd  = new FormData(e.form);
  cr_img_add_data(fd);
}
function cr_img_select_change(e)
{
  let v = e.title;
  if (!v) return;
  if (selection.length < 1) return;
  let bloc = selection[0];
  if (bloc.tpe != "image") return;
  //on récupère les chemins
  let rpath = "img/" + v;
  
  bloc.img_rpath = rpath;
  bloc.img_name = v;
  
  bloc_create_html(bloc);
  document.getElementById(bloc.id).onload = function () {
    //on met à jour les infos de hauteur
    let h = document.getElementById(bloc.id).getBoundingClientRect().height;
    rendu_get_superbloc(bloc).style.height = h*100/rendu.height + "%";
    bloc.height = h/rendu.height*631;
    document.getElementById("cr_tp_h").value = h/rendu.height*631;
    document.getElementById("cr_img_select").value = v;
    mv_place_pt();
    g_sauver();       
    };
  // on met à jour le rendu
  document.getElementById(bloc.id).src = "../../livres/" + cat + "/" + livre + "/" + rpath;
  document.removeEventListener("mousedown", cm_imgs_quitte);
  let cm = document.getElementById("cm_imgs");
  cm.style.visibility = "hidden";
}
function cr_img_add(e)
{
  document.getElementById("cr_img_get").click();
}
function cr_img_redim(e)
{
  let rect = e.getBoundingClientRect();
  if (rect.height > rect.width)
  {
    e.style.width = "auto";
    e.style.height = "60px";
  }
  else
  {
    e.style.height = "auto";
    e.style.width = "60px";
  }
}
function _img_sort_compare(a, b)
{
  return (a.title.localeCompare(b.title));
}
function cr_img_add_to_cm(path)
{
  //on crée le nv bloc image
  let eimg = document.createElement("img");
  eimg.onload = function() { cr_img_redim(eimg); };
  eimg.onmousedown = function() { cr_img_select_change(eimg); };
  eimg.title = path;
  eimg.src = "../../livres/" + cat + "/" + livre + "/img/" + path;
  
  //on récupère toutes les autres image
  let cm = document.getElementById("cm_imgs")
  let enl = cm.getElementsByTagName("img");
  let elems = [];
  for (let i=0; i<enl.length; i++)
  {
    elems.push(enl[i]);
  }
  for (let i=0; i<enl.length; i++)
  {
    enl[i].parentNode.removeChild(enl[i]);
  }
  elems.push(eimg);
  //on retrie
  elems.sort(_img_sort_compare);
  
  //on recrée le tableau
  let tb = document.createElement("table");
  let tr;
  for (let i=0; i<elems.length; i++)
  {
    if (i % 4 == 0) tr = document.createElement("tr");
    let td = document.createElement("td");
    td.appendChild(elems[i]);
    tr.appendChild(td);
    if (((i+1) % 4) == 0) tb.appendChild(tr);
  }
  if (elems.length % 4 != 0) tb.appendChild(tr);
  while (cm.firstChild)
  {
    cm.removeChild(cm.firstChild);
  }
  cm.appendChild(tb);
}

function cr_audio_get_change(e)
{
  let fd  = new FormData(e.form);
  cr_audio_add_data(fd);
}
function cr_audio_add_data(fd)
{
  //on sauvegarde l'image sélectionnée
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      let vv = xhr.responseText.split("\n");
      for (let i=0; i<vv.length; i++)
      {
        if (vv[i].trim() == "") continue;
        let audio_name = vv[i];
        if (audio_name.startsWith("*"))
        {
          alert(audio_name.substr(1));
          document.getElementById("cr_audio_select").value = "";
          continue;
        }
        // et la liste des images
        let option = document.createElement("option");
        option.text = audio_name;
        option.value = audio_name;
        document.getElementById("cr_audio_select").add(option);
        document.getElementById("cr_audio_select").value = audio_name;
  
        // on récupère le bloc sélectionné
        let bloc = null;
        if (i == 0 && selection.length > 0)
        {
          bloc = selection[0];
          if (bloc.tpe != "audio") return;
        }
        else bloc = bloc_new("audio", "");
        //on récupère les chemins
        bloc.audio_name = "sons/" + audio_name;
        bloc_create_html(bloc);
      }
      //on sauvegarde
      g_sauver();
    }
  };
  // We setup our request
  xhr.open("POST", "io.php?io=sauveaudio&cat=" + cat + "&livre=" + livre);
  xhr.send(fd);
}
function cr_audio_select_change(e)
{
  let v = e.value;
  //document.getElementById("cr_record_div").style.display = "none";
  if (!v) v = "";
  if (v == "******") //enregistrer
  {
    e.value = "";
    record_ini(e);
  }
  else if (v == "****") //parcourir
  {
    e.value = "";
    document.getElementById("cr_audio_get").click();
  }
  else
  {
    if (selection.length < 1) return;
    let bloc = selection[0];
    if (bloc.tpe != "audio") return;
    //on récupère les chemins
    if (v != "") bloc.audio_name = "sons/" + v;
    else bloc.audio_name = "";
    
    bloc_create_html(bloc);
    g_sauver();
  }
}

function cr_video_get_change(e)
{
  let fd  = new FormData(e.form);
  cr_video_add_data(fd);
}
function cr_video_add_data(fd)
{
  //on sauvegarde l'image sélectionnée
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      let vv = xhr.responseText.split("\n");
      for (let i=0; i<vv.length; i++)
      {
        if (vv[i].trim() == "") continue;
        let video_name = vv[i];
        if (video_name.startsWith("*"))
        {
          alert(video_name.substr(1));
          document.getElementById("cr_video_select").value = "";
          continue;
        }
        // et la liste des images
        let option = document.createElement("option");
        option.text = video_name;
        option.value = video_name;
        document.getElementById("cr_video_select").add(option);
        document.getElementById("cr_video_select").value = video_name;
  
        // on récupère le bloc sélectionné
        let bloc = null;
        if (i == 0 && selection.length > 0)
        {
          bloc = selection[0];
          if (bloc.tpe != "video") return;
        }
        else bloc = bloc_new("video", "");
        //on récupère les chemins
        bloc.video_name = "videos/" + video_name;
        bloc.video_type = 99;
        bloc_create_html(bloc);
        rendu_add_bloc(bloc);
      }
      //on sauvegarde
      g_sauver();
    }
  };
  // We setup our request
  xhr.open("POST", "io.php?io=sauvevideo&cat=" + cat + "&livre=" + livre);
  xhr.send(fd);
}
function cr_video_select_change(e)
{
  if (selection.length < 1) return;
  let bloc = selection[0];
  if (bloc.tpe != "video") return;
  
  let v = e.value;
  if (!v) v = "";
  if (v == "1" || v == "2" || v == "3") //youtube, etc...
  {
    bloc.video_type = parseInt(v);
    bloc.video_name = document.getElementById("cr_video_txt").value;
  }
  else if (v == "****") //parcourir
  {
    document.getElementById("cr_video_get").click();
    return;
  }
  else
  {
    bloc.video_type = 99;
    if (v != "") bloc.video_name = "videos/" + v;
    else bloc.video_name = "";
  }
  bloc_create_html(bloc);
  rendu_add_bloc(bloc);
  selection_update();
  g_sauver();
}
function cr_video_txt_change(e)
{
  if (selection.length < 1) return;
  let bloc = selection[0];
  if (bloc.tpe != "video") return;

  let v = e.value;
  if (!v) v = "";
  bloc.video_name = v;
  
  bloc_create_html(bloc);
  rendu_add_bloc(bloc);
  selection_update();
  g_sauver();
}

function cr_new_txt_click(e)
{
  // on récupère le bloc sélectionné
  if (selection.length < 1) return;
  let bloc = selection[0];
  bloc.txt = document.getElementById("cr_txt_ini").value;
  
  // on modifie le code html
  bloc_create_html(bloc);
  
  // et on modifie le rendu itou
  rendu_add_bloc(bloc);
  //on sauvegarde
  g_sauver(); 
}

function cr_coul_change(e)
{
  for (let i=0; i<selection.length; i++)
  {
    let bloc = selection[i];
    switch (bloc.tpe)
    {
      case "radiobtn":
        // on change les options et le rendu
        bloc.radiobtn_coul1 = "#" + document.getElementById("cr_coul1").jscolor;
        bloc.radiobtn_coul2 = "#" + document.getElementById("cr_coul2").jscolor;
        break;
      case "multi":
        // on change juste les options
        bloc.multi_coul = [];
        bloc.multi_maj = [];
        bloc.multi_suff = [];
        bloc.multi_barre = [];
        for (let i=0; i<document.getElementById("cr_coul_nb").value; i++)
        {
          bloc.multi_coul.push("#" + document.getElementById("cr_coul" + (i+1)).jscolor);
          if (document.getElementById("cr_coul" + (i+1) + "_barre").checked) bloc.multi_barre.push(1);
          else bloc.multi_barre.push(0);
          if (document.getElementById("cr_coul" + (i+1) + "_maj").checked) bloc.multi_maj.push(1);
          else bloc.multi_maj.push(0);
          if (document.getElementById("cr_coul" + (i+1) + "_suff").checked)
          {
            document.getElementById("cr_coul" + (i+1) + "_suff_txt").disabled = false;
            bloc.multi_suff.push(document.getElementById("cr_coul" + (i+1) + "_suff_txt").value);
          }
          else
          {
            document.getElementById("cr_coul" + (i+1) + "_suff_txt").disabled = true;
            bloc.multi_suff.push("");
          }
        }
        break;
      default:
        continue;
    }
    //on change le rendu
    bloc_create_html(bloc);
    rendu_add_bloc(bloc);
  }
  //on sauvegarde
  g_sauver();
}

function cr_texte_change(e)
{
  for (let i=0; i<selection.length; i++)
  {
    let bloc = selection[i];
    if (bloc.tpe != "texte") continue;
    bloc.texte_defaut = document.getElementById("cr_texte_defaut").value;
    bloc.texte_l = document.getElementById("cr_texte_l").value;
    bloc.texte_h = document.getElementById("cr_texte_h").value;
    bloc.texte_e = document.getElementById("cr_texte_e").value;
    bloc.texte_c = document.getElementById("cr_texte_c").value;
    bloc.texte_t = document.getElementById("cr_texte_t").value;
    if (document.getElementById("cr_texte_corr").checked) bloc.texte_corr = "1";
    else bloc.texte_corr = "0";
    
    bloc_create_html(bloc);
    
    rendu_add_bloc(bloc); 
  }
  //on sauvegarde
  g_sauver();
}

function cr_font_fam_change(e)
{
  let f = e.value;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_fam = f;
    document.getElementById(selection[i].id).style.fontFamily = f;
  }
  //on sauvegarde
  g_sauver();
}
function cr_font_size_change(e)
{
  let s1 = parseFloat(e.value);
  let s2 = s1*rendu.height/1000;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_size = s1;
    document.getElementById(selection[i].id).style.fontSize = s2 + "px";
    bloc_create_html(selection[i]);
  }
  selection_update();
  //on sauvegarde
  g_sauver();
}
function cr_font_coul_change(jscolor)
{
  let v = "#" + jscolor;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_coul = v;
    document.getElementById(selection[i].id).style.color = v;
  }
  //on sauvegarde
  g_sauver();
}
function cr_font_g_change(e)
{
  let v = e.checked;
  let vv = "0";
  if (v) vv = "1";
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_g = vv;
    if (v) document.getElementById(selection[i].id).style.fontWeight = "bold";
    else document.getElementById(selection[i].id).style.fontWeight = "normal";
  }
  //on sauvegarde
  g_sauver();
}
function cr_font_i_change(e)
{
  let v = e.checked;
  let vv = "0";
  if (v) vv = "1";
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_i = vv;
    if (v) document.getElementById(selection[i].id).style.fontStyle = "italic";
    else document.getElementById(selection[i].id).style.fontStyle = "normal";
  }
  //on sauvegarde
  g_sauver();
}
function cr_font_s_change(e)
{
  let v = e.checked;
  let vv = "0";
  if (v) vv = "1";
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_s = vv;
    selection[i].font_b = "0";
    if (v) document.getElementById(selection[i].id).style.textDecoration = "underline";
    else document.getElementById(selection[i].id).style.textDecoration = "none";
  }
  document.getElementById("cr_font_b").checked = false;
  //on sauvegarde
  g_sauver();
}
function cr_font_b_change(e)
{
  let v = e.checked;
  let vv = "0";
  if (v) vv = "1";
  for (let i=0; i<selection.length; i++)
  {
    selection[i].font_b = vv;
    selection[i].font_s = "0";
    if (v) document.getElementById(selection[i].id).style.textDecoration = "line-through";
    else document.getElementById(selection[i].id).style.textDecoration = "none";
  }
  document.getElementById("cr_font_s").checked = false;
  //on sauvegarde
  g_sauver();
}
function cr_align_change(e)
{
  let v = "0";
  if (e.id == "cr_align_l") v = "1";
  else if (e.id == "cr_align_c") v = "2";
  else if (e.id == "cr_align_r") v = "3";
  else return;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].align = v;
    if (v == "1") document.getElementById(selection[i].id).style.justifyContent = "start";
    else if (v == "2") document.getElementById(selection[i].id).style.justifyContent = "center";
    else if (v == "3") document.getElementById(selection[i].id).style.justifyContent = "end";
  }
  g_sauver();
}

function cr_tp_w_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    if (selection[i].size == "ratio")
    {
      selection[i].height = selection[i].height*v/selection[i].width;
      selection[i].width = v;      
      rendu_get_superbloc(selection[i]).style.width = v*100/443 + "%";
      rendu_get_superbloc(selection[i]).style.height = selection[i].height*100/631 + "%";
    }
    else if (selection[i].size == "manuel")
    {
      selection[i].width = v;
      rendu_get_superbloc(selection[i]).style.width = v*100/443 + "%";
    }
  }
  //on sauvegarde
  mv_place_pt();
  selection_update();
  g_sauver();
}
function cr_tp_h_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    if (selection[i].size == "ratio")
    {
      selection[i].width = selection[i].width*v/selection[i].height;
      selection[i].height = v;      
      rendu_get_superbloc(selection[i]).style.height = v*100/631 + "%";
      rendu_get_superbloc(selection[i]).style.width = selection[i].width*100/443 + "%";
    }
    else if (selection[i].size == "manuel")
    {
      selection[i].height = v;
      rendu_get_superbloc(selection[i]).style.height = v*100/631 + "%";
    }
  }
  //on sauvegarde
  mv_place_pt();
  selection_update();
  g_sauver();
}
function cr_tp_t_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    let mv = selection[i].top - v;
    selection[i].top = v;
    rendu_get_superbloc(selection[i]).style.top = v*100/631 + "%";
    if (selection[i].tpe == "ligne")
    {
      let elems = document.getElementsByClassName("extrema");
      for (let j=0; j<elems.length; j++)
      {
        elems[j].style.top = parseFloat(elems[j].style.top) - mv*rendu.height/631 + "px";
      }
      selection[i].y2 -= mv;
    }
  }
  //on sauvegarde
  mv_place_pt();
  g_sauver();
}
function cr_tp_l_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    let mv = selection[i].left - v;
    selection[i].left = v;
    rendu_get_superbloc(selection[i]).style.left = v*100/443 + "%";
    if (selection[i].tpe == "ligne")
    {
      let elems = document.getElementsByClassName("extrema");
      for (let j=0; j<elems.length; j++)
      {
        elems[j].style.left = parseFloat(elems[j].style.left) - mv*rendu.width/443 + "px";
      }
      selection[i].x2 -= mv;
    }
  }
  //on sauvegarde
  mv_place_pt();
  g_sauver();
}
function cr_tp_r_change(e)
{
  let v = e.value;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].rotation = v;
    rendu_get_superbloc(selection[i]).style.transform = "rotate(" + v + "deg)";
    if (selection[i].tpe == "ligne")
    {
      let dx = Math.sin(v*3.1415/180)*selection[i].height;
      let dy = Math.cos(v*3.1415/180)*selection[i].height;
      selection[i].x2  = parseFloat(selection[i].left) - dx;
      selection[i].y2  = parseFloat(selection[i].top) + dy;
      let elems = document.getElementsByClassName("extrema");
      elems[1].style.left = selection[i].x2*rendu.width/443 + "px";
      elems[1].style.top = selection[i].y2*rendu.height/631 + "px";
    }
  }
  //on sauvegarde
  mv_place_pt();
  g_sauver();
}

function cr_bord_change(e)
{
  let v = e.value;
  for (let i=0; i<selection.length; i++)
  {
    if (selection[i].tpe == "cercle" || selection[i].tpe == "ligne")
    {
      if (v == "double" || (v == "hidden" && selection[i].tpe == "ligne"))
      {
        //non géré, retour à l'ancienne valeur
        selection_update();
        return;
      }
      let svg = document.getElementById("svg_" + selection[i].id);
      svg.style.stroke = selection[i].bord_coul;
      svg.style.removeProperty("stroke-dasharray");
      switch (v)
      {
        case "dashed":
          svg.style.strokeDasharray = (2 + parseFloat(selection[i].bord_size)*rendu.width/443*2) + " " + (parseFloat(selection[i].bord_size)*rendu.width/443*2);
          break;
        case "dotted":
          svg.style.strokeDasharray = "0 " + (parseFloat(selection[i].bord_size)*rendu.width/443*1.5);
          break;
        case "hidden":
          svg.style.removeProperty("stroke");
          break;
      }
      bloc_create_html(selection[i]);
    }
    else document.getElementById(selection[i].id).style.borderStyle = v;
    selection[i].bord = v;
  }
  selection_update();
  //on sauvegarde
  g_sauver();
}
function cr_bord_coul_change(jscolor)
{
  let v = "#" + jscolor;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].bord_coul = v;
    if (selection[i].tpe == "cercle" || selection[i].tpe == "ligne")
    {
      let svg = document.getElementById("svg_" + selection[i].id);
      if (selection[i].bord != "hidden") svg.style.stroke = v;
    }
    else document.getElementById(selection[i].id).style.borderColor = v;
  }
  //on sauvegarde
  g_sauver();
}
function cr_bord_size_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    selection[i].bord_size = v;
    if (selection[i].tpe == "cercle" || selection[i].tpe == "ligne")
    {
      let svg = document.getElementById("svg_" + selection[i].id);
      svg.style.strokeWidth = v*rendu.width/443 + "px";
      switch (selection[i].bord)
      {
        case "dashed":
          svg.style.strokeDasharray = (2 + v*rendu.width/443*2) + " " + (v*rendu.width/443*2);
          break;
        case "dotted":
          svg.style.strokeDasharray = "0 " + (v*rendu.width/443*1.5);
          break;
      }
    }
    else document.getElementById(selection[i].id).style.borderWidth = v*rendu.width/443 + "px";
    bloc_create_html(selection[i]);
  }
  selection_update();
  //on sauvegarde
  g_sauver();
}
function cr_bord_rond_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    selection[i].bord_rond = v;
    document.getElementById(selection[i].id).style.borderRadius = v*rendu.width/443 + "px";
    bloc_create_html(selection[i]);
  }
  //on sauvegarde
  g_sauver();
}

function cr_fond_coul_change(jscolor)
{
  let v = "#" + jscolor;
  //si alpha = 0 alors on le met à 100
  let al = document.getElementById("cr_fond_alpha");
  if (parseFloat(al.value) == 0) al.value = 100;
  let a = parseFloat(al.value);
  for (let i=0; i<selection.length; i++)
  {
    selection[i].fond_coul = v;
    selection[i].fond_alpha = a;
    if (selection[i].tpe == "cercle")
    {
      document.getElementById("svg_" + selection[i].id).style.fill = hex2rgba(v, a);
    }
    else document.getElementById(selection[i].id).style.backgroundColor = hex2rgba(v, a);
  }
  //on sauvegarde
  g_sauver();
}
function cr_fond_alpha_change(e)
{
  let v = parseFloat(e.value);
  for (let i=0; i<selection.length; i++)
  {
    selection[i].fond_alpha = v;
    if (selection[i].tpe == "cercle")
    {
      document.getElementById("svg_" + selection[i].id).style.fill = hex2rgba(selection[i].fond_coul, v);
    }
    else document.getElementById(selection[i].id).style.backgroundColor = hex2rgba(selection[i].fond_coul, v);
  }
  //on sauvegarde
  g_sauver();
}

function cr_inter_change(e)
{
  for (let i=0; i<selection.length; i++)
  {
    if (e.checked && e.id == "cr_inter_0")
    {
      selection[i].inter = "0";
      selection[i].points = "0";
      selection[i].relie_id = "";
      selection[i].relie_cible_de = "";
      relie_maj_cibles();
    }
    else if (e.checked && e.id == "cr_inter_2")
    {
      selection[i].inter = "2";
      selection[i].points = "0";
      selection[i].relie_id = "";
      selection[i].relie_cible_de = "";
      relie_maj_cibles();
    }
    else if (e.checked && e.id == "cr_inter_1")
    {
      selection[i].inter = "1";
      if (selection[i].points == "0" && selection[i].relie_id != "") selection[i].points = "1";
      selection[i].relie_cible_de = "";
      relie_maj_cibles();
    }
    bloc_create_html(selection[i]);
  }
  selection_update();
  //on sauvegarde
  g_sauver();
}

function cr_relie_id_change(e)
{
  let v = e.value;
  //on vérifie les valeurs
  let elems = v.split("|");
  let txt = "";
  for (let i=0; i<elems.length; i++)
  {
    let b = bloc_get_from_id(elems[i]);
    if (!b) txt += "\n" + "l'élément avec l'id " + elems[i] + " est introuvable !";
    else if (b.relie_id != "" && b.relie_id != "*") txt += "\n" + "l'élément avec l'id " + elems[i] + " est déjà une source de relier !";
    else
    {
      switch (b.tpe)
      {
        case "texte":
        case "radio":
        case "radiobtn":
        case "check":
        case "multi":
        case "combo":
        case "cible":
          txt += "\n" + "les éléments de type " + b.tpe + " ne peuvent pas être reliés !";
          break;
      }
      for (let j=0; j<selection.length; j++)
      {
        if (selection[j].id == b.id) txt += "\n" + "l'élément avec l'id " + b.id + " ne peut se relier à lui-même !";
      }
    }
  }
  if (txt != "")
  {
    alert("!! ERREUR !!\n" + txt);
    e.value = "";
    v = "";
    elems = [];
  }
  for (let i=0; i<selection.length; i++)
  {
    let bloc = selection[i];
    bloc.relie_id = v;
    bloc.relie_cible_de = "";
    if (v != "" && bloc.points == "0") bloc.points = "1";
    else if (v == "") bloc.points = "0";
    bloc_create_html(bloc);
  }
  relie_maj_cibles();
  selection_update();
  //on sauvegarde
  g_sauver();
}
function relie_maj_cibles()
{
  for (let i=0; i<blocs.length; i++)
  {
    let bloc = blocs[i];
    let txt = "";
    for (let j=0; j<blocs.length; j++)
    {
      if (blocs[j] == bloc) continue;
      if (blocs[j].inter != "1" || blocs[j].relie_id == "") continue;
      let v = blocs[j].relie_id.split("|");
      for (let k=0; k<v.length; k++)
      {
        if (v[k] == bloc.id)
        {
          if (txt != "") txt += "|";
          txt += blocs[j].id;
          break;
        }
      }
    }
    if (txt == "")
    {
      if (bloc.inter == "1" && bloc.relie_cible_de != "")
      {
        bloc.inter = "0";
        bloc.relie_id = "";
        bloc.points = "0";
      }
      bloc.relie_cible_de = "";
      bloc_create_html(bloc);
    }
    else
    {
      bloc.inter = "1";
      bloc.relie_id = "*";
      bloc.relie_cible_de = txt;
      bloc.points = "0";
      bloc_create_html(bloc);
    }
  }
}

function cr_points_change(e)
{
  let v = e.value;
  for (let i=0; i<selection.length; i++)
  {
    selection[i].points = v;
    bloc_create_html(selection[i]);
  }
  //on sauvegarde
  g_sauver();
  maj_score();
}

function cr_aligne_change(id)
{
  if (selection.length == 0) return;
  // on regrade les extrems
  let rec = [selection[0].left, selection[0].top, selection[0].left + selection[0].width, selection[0].top + selection[0].height];
  for (let i=1; i<selection.length; i++)
  {
    rec[0] = Math.min(rec[0], selection[i].left);
    rec[1] = Math.min(rec[1], selection[i].top);
    rec[2] = Math.max(rec[2], selection[i].left + selection[i].width);
    rec[3] = Math.max(rec[3], selection[i].top + selection[i].height);
  }

  switch (id)
  {
    case "1": //gauche
      for (let i=0; i<selection.length; i++)
      {
        selection[i].left = rec[0];
        rendu_get_superbloc(selection[i]).style.left = rec[0]*100/443 + "%";
        document.getElementById("cr_tp_l").value = rec[0];
      }
      break;
    case "2": //centre h
      let ch = (rec[0] + rec[2]) / 2;
      for (let i=0; i<selection.length; i++)
      {
        selection[i].left = ch - parseFloat(selection[i].width)/2;
        rendu_get_superbloc(selection[i]).style.left = selection[i].left*100/443 + "%";
        document.getElementById("cr_tp_l").value = selection[i].left;
      }
      break;
    case "3": //droite
      for (let i=0; i<selection.length; i++)
      {
        selection[i].left = rec[2] - parseFloat(selection[i].width);
        rendu_get_superbloc(selection[i]).style.left = selection[i].left*100/443 + "%";
        document.getElementById("cr_tp_l").value = selection[i].left;
      }
      break;
    case "4": //haut
      for (let i=0; i<selection.length; i++)
      {
        selection[i].top = rec[1];
        rendu_get_superbloc(selection[i]).style.top = rec[1]*100/631 + "%";
        document.getElementById("cr_tp_t").value = rec[1];
      }
      break;
    case "5": //centre v
      let cv = (rec[1] + rec[3]) / 2;
      for (let i=0; i<selection.length; i++)
      {
        selection[i].top = cv - parseFloat(selection[i].height)/2;
        rendu_get_superbloc(selection[i]).style.top = selection[i].top*100/631 + "%";
        document.getElementById("cr_tp_t").value = selection[i].top;
      }
      break;
    case "6": //bas
      for (let i=0; i<selection.length; i++)
      {
        selection[i].top = rec[3] - parseFloat(selection[i].height);
        rendu_get_superbloc(selection[i]).style.top = selection[i].top*100/631 + "%";
        document.getElementById("cr_tp_t").value = selection[i].top;
      }
      break;
  }
  if (id >= 0) g_sauver();
}
function _repart_compare_h(a, b)
{
  return (a.left - b.left);
}
function _repart_compare_v(a, b)
{
  return (a.top - b.top);
}
function cr_repart_change(id)
{
  if (id == "1")
  {
    //on reordonne la selection en fonction de left
    let news = selection.sort(_repart_compare_h);
    //on calcule l'espace moyen entre les blocs
    let espaces = 0;
    for (let i=0; i<news.length-1; i++)
    {
      espaces += parseFloat(news[i+1].left) - (parseFloat(news[i].left) + parseFloat(news[i].width));
    }
    espaces = espaces/(news.length-1);
    for (let i=1; i<news.length-1; i++)
    {
      news[i].left = parseFloat(news[i-1].left) + parseFloat(news[i-1].width) + espaces;
      rendu_get_superbloc(news[i]).style.left = news[i].left*100/443 + "%";
      document.getElementById("cr_tp_l").value = news[i].left;
    }
  }
  else if (id == "2")
  {
    //on reordonne la selection en fonction de left
    let news = selection.sort(_repart_compare_v);
    //on calcule l'espace moyen entre les blocs
    let espaces = 0;
    for (let i=0; i<news.length-1; i++)
    {
      espaces += parseFloat(news[i+1].top) - (parseFloat(news[i].top) + parseFloat(news[i].height));
    }
    espaces = espaces/(news.length-1);
    for (let i=1; i<news.length-1; i++)
    {
      news[i].top = parseFloat(news[i-1].top) + parseFloat(news[i-1].height) + espaces;
      rendu_get_superbloc(news[i]).style.top = news[i].top*100/631 + "%";
      document.getElementById("cr_tp_t").value = news[i].top;
    }
  }
  if (id >= 0) g_sauver();
}
function cr_plans_change(id)
{
  switch (id)
  {
    case "1": //premier plan
      for (let i=0; i<selection.length; i++)
      {
        //on met en ordre la liste des blocs
        let b = selection[i];
        for (let j=0; j<blocs.length; j++)
        {
          if (b.id == blocs[j].id)
          {
            blocs.splice(j,1);
            break;
          }
        }
        blocs.push(b);
        //on modifie le rendu
        let sb = rendu_get_superbloc(b);
        let rd = sb.parentNode;
        rd.removeChild(sb);
        rd.appendChild(sb);
      }
      break;
    case "2": //arrière plan
      for (let i=0; i<selection.length; i++)
      {
        //on met en ordre la liste des blocs
        let b = selection[i];
        for (let j=0; j<blocs.length; j++)
        {
          if (b.id == blocs[j].id)
          {
            blocs.splice(j,1);
            break;
          }
        }
        blocs.unshift(b);
        //on modifie le rendu
        let sb = rendu_get_superbloc(b);
        let rd = sb.parentNode;
        rd.removeChild(sb);
        rd.insertBefore(sb, rd.childNodes[1]);
      }
      break;
    case "3": //avancer
      break;
    case "4": //reculer
      break;
    default:
      return;
  }
  g_sauver();
}
function cr_action_change(e)
{
  if (e.value == "1") //dupliquer
  {
    let news = [];
    for (let i=0; i<selection.length; i++)
    {
      let nb = JSON.parse(JSON.stringify(selection[i]));
      nb.left += 15;
      nb.top += 15;
      if (nb.tpe == "ligne")
      {
        nb.x2 += 15;
        nb.y2 += 15;
      }
      last_id++;
      nb.id = last_id;
      news.push(nb);
      blocs.push(nb);
      htmls.push("");
      bloc_create_html(nb);
      let option = document.createElement("option");
      option.text = nb.id + " (" + nb.tpe + ")";
      option.value = nb.id;
      document.getElementById("cr_bloc_liste").add(option);
      rendu_add_bloc(nb);
    }
    selection = news;
    selection_change();
  }
  else if (e.value == "2")  //supprimer
  {
    for (let i=0; i<selection.length; i++)
    {
      //on suprime les blocs du rendu
      let sb = rendu_get_superbloc(selection[i]);
      sb.parentNode.removeChild(sb);
      // et de la liste déroulante
      for (let j=0; j<document.getElementById("cr_bloc_liste").options.length; j++)
      {
        if (document.getElementById("cr_bloc_liste").options[j].value == selection[i].id)
        {
          document.getElementById("cr_bloc_liste").remove(j);
          break;
        }
      }
      // on supprime les blocs de la base
      for (let j=0; j<blocs.length; j++)
      {
        if (selection[i].id == blocs[j].id)
        {
          blocs.splice(j,1);
          break;
        }
      }
    }
    selection = [];
    selection_change();
  }
  if (e.value >= 0) g_sauver();
  maj_score();
  e.selectedIndex = 0;
}

function cr_record_start(e)
{
  let pre = e.id.substr(0,2);
  switch (e.getAttribute("etat"))
  {
    case "0":
      record_start(e);
      break;
    case "1":
      record_stop(e);
      break;
    case "2":
      document.getElementById(pre + "_record_audio").play();
      break;
  }
}
function cr_record_save(e)
{
  let pre = e.id.substr(0,2);
  //on sauvegarde le son enregistré
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      let audio_name = xhr.responseText;
      if (audio_name.startsWith("*"))
      {
        alert(audio_name.substr(1));
        document.getElementById(pre + "_audio_select").value = "";
        return;
      }
      // et la liste des images
      let option = document.createElement("option");
      option.text = audio_name;
      option.value = audio_name;
      document.getElementById(pre + "_audio_select").add(option);
      document.getElementById(pre + "_audio_select").value = audio_name;
      if (pre == "cr")
      {
        // on récupère le bloc sélectionné
        if (selection.length < 1) return;
        let bloc = selection[0];
        if (bloc.tpe != "audio") return;
        //on récupère les chemins
        bloc.audio_name = "sons/" + audio_name;
        
        bloc_create_html(bloc);
        //on sauvegarde
        g_sauver();
      }
      else if (pre == "ci")
      {
        infos.audio_name = "sons/" + audio_name;
        g_sauver_info();
      }     
      
      //et on remet tout l'affichage à zéro
      record.chunks = [];
      record.blob = null;
      record.recorder = null;
      document.getElementById(pre + "_record_div").style.display = "none";
    }
  };
  // We setup our request
  xhr.open("POST", "io.php?io=sauveaudioblob&cat=" + cat + "&livre=" + livre);
  xhr.send(record.blob);
}
function cr_record_delete(e)
{
  record.chunks = [];
  record.blob = null;
  record.recorder = null;
  record_ini(e);
}

function cr_bloc_liste_change(e)
{
  let b = bloc_get_from_id(e.value);
  if (b)
  {
    selection = [b];
    selection_change();
  }
  else
  {
    selection = [];
    selection_change();
  }
}
function cm_quitte(e)
{
  document.removeEventListener("mousedown", cm_quitte);
  let elems = document.getElementsByClassName("cm");
  for (let i=0; i<elems.length; i++)
  {
    elems[i].style.visibility = "hidden";
  }
}

function cm_show_aligne(e)
{
  if (selection.length == 0) return;
  let rect = e.getBoundingClientRect();
  let cm = document.getElementById("cm_aligne");
  let rect2 = cm.getBoundingClientRect();
  cm.style.top = (rect.top + rect.height - rect2.height) + "px";
  cm.style.left = rect.right+2 + "px";
  
  document.addEventListener("mousedown", cm_quitte);
  cm.style.visibility = "visible";
}
function cm_show_repart(e)
{
  if (selection.length <= 2) return;
  let rect = e.getBoundingClientRect();
  let cm = document.getElementById("cm_repart");
  let rect2 = cm.getBoundingClientRect();
  cm.style.top = (rect.top + rect.height - rect2.height) + "px";
  cm.style.left = rect.right+2 + "px";
  
  document.addEventListener("mousedown", cm_quitte);
  cm.style.visibility = "visible";
}
function cm_show_plans(e)
{
  if (selection.length == 0) return;
  let rect = e.getBoundingClientRect();
  let cm = document.getElementById("cm_plans");
  let rect2 = cm.getBoundingClientRect();
  cm.style.top = (rect.top + rect.height - rect2.height) + "px";
  cm.style.left = rect.right+2 + "px";
  
  document.addEventListener("mousedown", cm_quitte);
  cm.style.visibility = "visible";
}

function cm_imgs_quitte(e)
{
  if (e.target && e.target.id == "cm_imgs") return;
  document.removeEventListener("mousedown", cm_imgs_quitte);
  let cm = document.getElementById("cm_imgs");
  cm.style.visibility = "hidden";
}

function cr_img_select_mousedown(event, elem)
{
  event.preventDefault();
  event.stopPropagation();
  let cm = document.getElementById("cm_imgs");
  let rect = elem.getBoundingClientRect();
  let rect2 = cm.getBoundingClientRect();
  cm.style.top = rect.bottom + "px";
  cm.style.left = rect.right - rect2.width + "px";
  cm.style.visibility = "visible";
  document.addEventListener("mousedown", cm_imgs_quitte);
}

function cr_txt_ini_keypress(event)
{
  if (event.which == 13) event.preventDefault();
}
function cr_txt_ini_change(e)
{
  let v = e.value;
  let vv = v.split("\n");
  if (vv.length > 1)
  {
    //preemier blocs
    let b0 = selection[0];
    b0.txt = vv[0];
    e.value = vv[0];
    bloc_create_html(b0);
    rendu_add_bloc(b0);
    
    //on cré les suivants
    for (let i=1; i<vv.length; i++)
    {
      console.log(vv[i]);
      if (vv[i].trim() != "") bloc_new(b0.tpe, vv[i]);
    }
    
    g_sauver();
    return;
  }
  for (let i=0; i<selection.length; i++)
  {
    selection[i].txt = v;
    bloc_create_html(selection[i]);
    rendu_add_bloc(selection[i]);
  }
  g_sauver();
}
