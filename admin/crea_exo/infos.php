<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
$test = (file_exists("../../TEST"));

function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}

  include("../../core/maj.php");
  // on récupère les paramètres
  if (!isset($_GET['exo']) || !isset($_GET['cat']) || !isset($_GET['livre']))
  {
    exit("imossible de récupérer les paramètres...");
  }
  $exo = $_GET['exo'];
  $livre = $_GET['livre'];
  $cat = $_GET['cat'];
  $dos_l = "../../livres/";
  if ($cat != "") $dos_l .= "$cat/";
  $dos_l .= "$livre";
  
  //si besoin, on crée un nouvel exercice
  if ($exo == "")
  {
    if (!$test)
    {
      $exo = 1;
      while (file_exists("$dos_l/exos/$exo")) $exo++;
      $txt = file_get_contents("$dos_l/liste.txt");
      if ($txt != "") $txt .= "\n";
      $txt .= "$exo";
      file_put_contents("$dos_l/liste.txt", $txt);
    }
    else
    {
      echo "Action impossible en mode test !";
      exit;
    }
  }
  $dos_e = "$dos_l/exos/$exo";
  
  //on crée les fichier qu'il faut si besoin
  if (!$test)
  {
    if (!file_exists($dos_e)) mkdir("$dos_e", 0777, true);
    if (!file_exists("$dos_e/exo.inc.txt")) file_put_contents("$dos_e/exo.inc.txt", "");
    if (!file_exists("$dos_e/exo.txt")) file_put_contents("$dos_e/exo.txt", "");
    if (!file_exists("$dos_e/version")) copy("../../VERSION", "$dos_e/version");
  }
  else if (!file_exists($dos_e) || !file_exists("$dos_e/exo.inc.txt") || !file_exists("$dos_e/exo.txt") || !file_exists("$dos_e/version"))
  {
    echo "Action impossible en mode test !";
    exit;
  }
  
  //on s'occupe de la visibilité
  $hidden = array();
  if (file_exists("$dos_e/hide.txt")) $hidden = explode("\n", file_get_contents("$dos_e/hide.txt"));
  else file_put_contents("$dos_e/hide.txt", "");
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Création d'exercice</title>
  <link rel="stylesheet" href="infos.css">
  <link rel="shortcut icon" href="../../icons/gnome-palm.png" >
  <script type="text/javascript" src="../../libs/jscolor.min.js"></script>
  
  <script type="text/javascript" src="infos.js"></script>
</head>

<body onload="start('<?php echo "$cat"; ?>', '<?php echo "$livre"; ?>', '<?php echo "$exo"; ?>')">
  <div id="bandeau">
    <a href="../index.php"><img class="bimg" src="../../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="../creation.php"><img class="bimg2" src="../../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="../users.php"><img class="bimg" src="../../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>
  <div id="bandeau2">
    <a href="../livre.php?cat=<?php echo $cat ?>&livre=<?php echo $livre ?>"><img class="bimg" src="../../icons/edit-undo.svg" title="Retour au livre"/></a>
    <a href="exo.php?cat=<?php echo $cat ?>&livre=<?php echo $livre ?>&exo=<?php echo $exo ?>"><img class="bimg" src="../../icons/application-x-diagram.svg" title="Conception de l'exercice"/></a>
  </div>
  <div class="col">
    <div class="titre">Informations exercice</div>
  <div id="cr_pa_info">
    <p>
      Titre de l'exercice :
      <input type="text" id="cri_titre" onKeyUp="cri_titre_change(this)" value="Exercice"/>
      Couleur de fond :
      <input class="jscolor {hash:true}" type="text" id="cri_coul" value="#ffffff" onchange="cri_coul_change(this)" /><br>
      Visibilité :
<?php
    //case "personne" (hide = ALL)
    echo "<span class=\"visi\"><input onchange=\"exo_visi_change(this, true)\" id=\"visi__ALL_\" type=\"checkbox\" value=\"_ALL_\" ";
    if (in_array("_ALL_", $hidden)) echo "checked ";
    echo "/><label for=\"visi__ALL_\">PERSONNE</label></span>";
    
    //et les cases de chaque groupe
    if (file_exists("../../groupes.txt"))
    {
      $gg = explode("\n", file_get_contents("../../groupes.txt"));
      for ($i=0; $i<count($gg); $i++)
      {
        $vv = explode("|", $gg[$i]);
        if (count($vv)>2)
        {
          echo "<span class=\"visi\"><input onchange=\"exo_visi_change(this, true)\" id=\"visi_$vv[2]\" type=\"checkbox\" value=\"$vv[2]\" ";
          if (!in_array($vv[2], $hidden)) echo "checked ";
          echo "/><label for=\"visi_$vv[2]\">$vv[0]</label></span>";
        }
      }
    }
?>
    </p>
    <table class="cri_table">
      <tr>
        <td>Consigne (html) :</td>
        <td><textarea id="cri_consigne" onKeyUp="cri_consigne_change(this)"></textarea></td>
        <td>Consigne audio :</td>
        <td>
          <div class="cr_opt_ligne" id="ci_audio_get_div">
            <select id="ci_audio_select" onchange="cr_audio_select_change(this)">
              <option value="" selected></option>
              <option value="******">Enregistrer...</option>
              <option value="****">Parcourir...</option>
              <?php
                $sons = myglob("$dos_l/sons");
                for ($i=0; $i<count($sons); $i++)
                {
                  echo "<option value=\"".basename($sons[$i])."\">".basename($sons[$i])."</option>";
                }
              ?>
            </select>
            <form enctype="multipart/form-data">
              <input name="cr_audio_get[]" type="file" id="ci_audio_get" accept="audio/*" onchange="cr_audio_get_change(this)"/>
            </form>
            <div id="ci_record_div">
              <img id="ci_record_start" etat="0" src="../../icons/media-record.svg" onclick="cr_record_start(this)"/>
              <img id="ci_record_save" src="../../icons/document-save.svg" onclick="cr_record_save(this)"/>
              <img id="ci_record_delete" src="../../icons/window-close.svg" onclick="cr_record_delete(this)"/>
              <audio id="ci_record_audio"></audio>
            </div>
          </div>
        </td>
      </tr>
    </table>
    </p>
    <p>
      Apréciations :
      <table class="cri_table">
        <tr>
          <th>score min</th>
          <th>couleur</th>
          <th>texte (html)</th>
        </tr>
        <tr>
          <th><input type="number" id="1_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="1_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="1_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
        <tr>
          <th><input type="number" id="2_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="2_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="2_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
        <tr>
          <th><input type="number" id="3_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="3_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="3_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
        <tr>
          <th><input type="number" id="4_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="4_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="4_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
        <tr>
          <th><input type="number" id="5_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="5_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="5_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
        <tr>
          <th><input type="number" id="6_cri_a_min" value="0" min="0" onchange="cri_a_min_change(this)" title="score à partir duquel on affiche l'apréciation"/>%</th>
          <th>
            <select id="6_cri_a_coul" onChange="cri_a_coul_change(this)" title="couleur de l'apréciation et du drapeau">
              <option value="n">noir</option>
              <option value="r">rouge</option>
              <option value="j">jaune</option>
              <option value="b">bleu</option>
              <option value="v">vert</option>
              <option value="c">coupe</option>
            </select>
          </th>
          <th><textarea class="cri_a_txt" id="6_cri_a_txt" onChange="cri_a_txt_change(this)"></textarea></th>
        </tr>
      </table>
    </p>
    <p>
      Total des points (-1=auto) :
      <input type="number" id="cri_total" value="-1" min="-1" onchange="cri_total_change(this)" />
      arrondi <select id="cri_arrondi" onChange="cri_arrondi_change(this)"><option value="1" selected>unité</option><option value="0.5">0.5</option><option value="0.1">0.1</option><option value="0.01">0.01</option><option value="0">auncun</option></select>
      <input type="checkbox" id="cri_show_bilan" name="cri_show_bilan" onchange="cri_show_bilan_change(this)" />
      <label for="cri_show_bilan">Afficher dans le bilan</label>
    </p>
    <p>
      Nombre d'essais (0=infini) :
      <input type="number" id="cri_essais" value="1" min="0" onchange="cri_essais_change(this)" />
      Type de réessai :
      <select id="cri_reessai" onChange="cri_reessai_change(this)"><option value="2">effacer (reprendre à zéro)</option><option value="1">reprendre sans effacer</option><option value="3" selected>au choix (reprendre ou effacer)</option></select>
    </p>
    <p>
      <div class="cr_opt_ligne" id="ci_img_get_div">
        Image de l'exercice :
        <select id="ci_img_select" onchange="cr_img_select_change(this)" title="Affichée dans le panneau de gauche">
          <option value=""></option>
          <option value="livre" selected>image principale du livre</option>
          <option value="****">Parcourir...</option>
          <?php
            $imgs = myglob("$dos_l/img");
            for ($i=0; $i<count($imgs); $i++)
            {
              echo "<option value=\"".basename($imgs[$i])."\">".basename($imgs[$i])."</option>";
            }
          ?>
        </select>
        <form enctype="multipart/form-data">
          <input name="cr_img_get[]" type="file" id="ci_img_get" accept="image/*" onchange="cr_img_get_change(this)"/>
        </form>
        <input type="checkbox" id="cri_img_hover" name="cri_img_hover" onchange="cri_img_hover_change(this)" title="sinon, l'image sera montrée uniquement au survol de l'icone ampoule"/>
        <label for="cri_img_hover">Afficher l'image en permanence</label>
      </div>
    </p>
    <p id="cri_mod">
      Appliquer le modèle : 
      <select id="cri_mod_sel" onchange="cri_mod_sel_change(this)">
        <option value="****"></option>
        <option value="####">...Gestionnaire...</option>
        <?php
          $imgs = myglob("modeles");
          for ($i=0; $i<count($imgs); $i++)
          {
            echo "<option value=\"".basename($imgs[$i])."\">".basename($imgs[$i])."</option>";
          }
        ?>
      </select>
      <button id="cri_mod_save" onclick="cri_mod_save(this)">Sauver comme modèle</button>
      <a id="cri_mod_gestion" href="modeles.php" target="_blank"></a>
    </p>
  </div></div>
  <div class="exotice"><img src="../../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
