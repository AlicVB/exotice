 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

let blocs = new Array();  // c'est un tableau qui rescence les données de chaque bloc
let selection = new Array();  // c'est un tableau avec les blocs sélectionnés
let htmls = new Array();  // c'est un tableau avec les codes html de chaque bloc

let last_id = 0;    // dernier id utilisé

let cat = "";
let livre = "";
let exo = "";
let auteur = "";

let record = {};  //objet contenant tout ce qu'il faut pour enregistrer

let rendu = {}; //objet contenant la taille actuelle intérieure du rendu
let selection_rect = {}; //objet contenant les propriétés de la sélection rectangulaire à la souris

function change(e)
{
}
function txt_spaces(txt)
{
  return txt.replace(/ /g, "&nbsp;");
}

function rendu_autosize(e)
{
  let bodystyle = window.getComputedStyle(document.body);
  let vw = document.documentElement.clientWidth - - parseInt(bodystyle.marginLeft) - parseInt(bodystyle.marginRight);
  let vh = document.documentElement.clientHeight - parseInt(bodystyle.marginTop) - parseInt(bodystyle.marginBottom);
  let wmax = vw-60-450-32;
  let r = document.getElementById("cr_rendu");
  if (vh > wmax*641/453)
  {
    r.style.height = wmax*641/453 + "px";
    r.style.width = wmax + "px";
  }
  else
  {
    r.style.height = vh + "px";
    r.style.width = vh*453/641 + "px";
  }
  rendu.height = parseFloat(r.style.height)-10;
  rendu.width = parseFloat(r.style.width)-10;
  let rect = r.getBoundingClientRect();
  rendu.left = rect.left + 5;
  rendu.top = rect.top + 5;
  document.getElementById("cr_opt").style.height = (vh-10) + "px";
  if (document.getElementById("exotice"))
  {
    document.getElementById("exotice").style.height = 40*rendu.height/1000 + "px";
    document.getElementById("user").style.fontSize = 20*rendu.height/1000 + "px";
    document.getElementById("bysa").style.fontSize = 15*rendu.height/1000 + "px";
    document.getElementById("cc_img").style.height = 20*rendu.height/1000 + "px";
  }
  for (let i=0; i<blocs.length; i++)
  {
    rendu_add_bloc(blocs[i]);
  }
}
function start(_cat, _livre, _exo, _aut)
{
  //on initialise la sélection
  selection_rect.on = false;
  //on redimmensionne le rendu
  window.addEventListener("resize", rendu_autosize);
  rendu_autosize(null);
  //si pas de nom d'exo dans l'url, il faut le rajouter ! (pour refresh...)
  if (window.location.href.substr(-1,1) == "=")
  {
    history.replaceState('ajout exo', document.title, window.location.href + _exo);
  }
  cat = _cat;
  livre = _livre;
  exo = _exo;
  auteur = _aut;
  mv_rs_ini();
  g_restaurer(true);

  document.addEventListener("keydown", cr_keydown);
  
  //enregistrement
  record.chunks = [];
  record.stream = null;
  record.recorder = null;
  record.promise = null;
  record.blob = null;
  
  //menu images
  let elems = document.getElementById("cm_imgs").getElementsByTagName("img");
  for (let i=0; i<elems.length; i++)
  {
    cr_img_redim(elems[i]);
  }
  
  // drop dans le rendu
  let rd = document.getElementById("cr_rendu");
  rd.addEventListener("dragover", cr_rendu_dragover);
  rd.addEventListener("dragleave", cr_rendu_dragleave);
  rd.addEventListener("drop", cr_rendu_drop);
}

function hex2rgb(hex)
{
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  let r = parseInt(result[1], 16);
  let g = parseInt(result[2], 16);
  let b = parseInt(result[3], 16);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}
function hex2rgba(hex, alpha)
{
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  let r = parseInt(result[1], 16);
  let g = parseInt(result[2], 16);
  let b = parseInt(result[3], 16);
  return "rgba(" + r + ", " + g + ", " + b + ", " + (alpha/100) + ")";
}

function record_ini(e)
{
  let pre = e.id.substr(0,2);
  //on gère l'affichage
  document.getElementById(pre + "_record_start").style.display = "inline";
  document.getElementById(pre + "_record_save").style.display = "none";
  document.getElementById(pre + "_record_delete").style.display = "none";
  document.getElementById(pre + "_record_start").setAttribute("etat", "0");
  document.getElementById(pre + "_record_start").style.backgroundColor = "#D8D8D8";
  document.getElementById(pre + "_record_start").src = "../../icons/media-record.svg";
  document.getElementById(pre + "_record_div").style.display = "inline-block";

  if (record.promise) return; //on a déjà initialiser !
  
  record.promise = navigator.mediaDevices.getUserMedia({audio: true, video: false});
  record.promise.then(function(_str) {record.stream = _str; });
  record.promise.catch(function(err) { console.log(err.name + ": " + err.message); });
}
function record_start(el)
{
  let pre = el.id.substr(0,2);
  document.getElementById(pre + "_record_start").setAttribute("etat", "1");
  document.getElementById(pre + "_record_start").style.backgroundColor = "red";
  document.getElementById(pre + "_record_start").src = "../../icons/media-playback-stop.svg";
  record.recorder = new MediaRecorder(record.stream);
  record.recorder.ondataavailable = function(e) {record.chunks.push(e.data);};
  record.recorder.onstop = function(e) {record_fin(e, el);};
  
  record.recorder.start();
  console.log("recorder started : " + record.recorder.state);
}
function record_stop(e)
{
  let pre = e.id.substr(0,2);
  record.recorder.stop();
  console.log("recorder stopped " + record.recorder.state);
  document.getElementById(pre + "_record_start").setAttribute("etat", "2");
  document.getElementById(pre + "_record_start").style.backgroundColor = "#D8D8D8";
  document.getElementById(pre + "_record_start").src = "../../icons/media-playback-start.svg";
  document.getElementById(pre + "_record_save").style.display = "inline";
  document.getElementById(pre + "_record_delete").style.display = "inline";
}
function record_fin(e, el)
{
  let pre = el.id.substr(0,2);
  record.blob = new Blob(record.chunks, { 'type' : 'audio/ogg; codecs=opus' });
  let audioURL = window.URL.createObjectURL(record.blob);
  document.getElementById(pre + "_record_audio").src = audioURL;
}

function file_create_css()
{
  let txt = "";
  for (let i=0; i<blocs.length; i++)
  {
    let b = blocs[i];
    txt += "[id=\"" + b.id + "\"] {";
    //police
    txt += "font-family: \"" + b.font_fam + "\"; ";
    txt += "font-size: " + (b.font_size/10) + "vh; ";
    txt += "color: transparent; text-shadow: 0 0 " + b.font_coul + "; ";
    if (b.font_g == "1") txt += "font-weight: bold; ";
    if (b.font_i == "1") txt += "font-style: italic; ";
    if (b.font_s == "1") txt += "text-decoration: underline; ";
    if (b.font_b == "1") txt += "text-decoration: line-through; ";
    if (b.align == "1") txt += "justify-content: flex-start; ";
    else if (b.align == "3") txt += "justify-content: flex-end; ";
    else txt += "justify-content: center; ";
    //position-taille
    txt += "position: absolute; ";
    txt += "left: " + b.left*100/443 + "%; ";
    txt += "top: " + b.top*100/631 + "%; ";
    if (b.size == "manuel" || b.size == "ratio" || b.tpe == "ligne")
    {
      if (b.tpe == "cible")
      {
        txt += "min-width: " + b.width*100/443 + "%; ";
        txt += "min-height: " + b.height*100/631 + "%; ";
      }
      else
      {
        txt += "width: " + b.width*100/443 + "%; ";
        txt += "height: " + b.height*100/631 + "%; ";
      }
    }
    else
    {
      let rec = document.getElementById(b.id).getBoundingClientRect();
      txt += "width: " + rec.width*100/rendu.width + "%; ";
      txt += "height: " + rec.height*100/rendu.height + "%; ";
    }
    if (b.tpe == "ligne")
    {
      txt += "transform-origin: top center; ";
      txt += "width: 3px; ";
      txt += "height: " + b.height*100/631 + "%; ";
    }
    if (b.rotation != "0") txt += "transform: rotate(" + b.rotation + "deg); ";
    //bords
    if (b.bord != "hidden")
    {
      if (b.tpe == "cercle" || b.tpe == "ligne")
      {
        switch (b.bord)
        {
          case "dashed":
            txt += "stroke-dasharray: " + (2 + parseFloat(b.bord_size)*2) + " " + (parseFloat(b.bord_size)*2) + "; ";
            break;
          case "dotted":
            txt += "stroke-dasharray: 0 " + (parseFloat(b.bord_size)*1.5) + "; ";
            break;
        }
        txt += "stroke-width: " + b.bord_size + "px; ";
        txt += "stroke: " + b.bord_coul + "; ";
      }
      else
      {
        txt += "border-style: " + b.bord + "; ";
        txt += "border-color: " + b.bord_coul + "; ";
        txt += "border-width: " + b.bord_size + "px; ";
        txt += "border-radius: " + b.bord_rond + "px; ";
      }
    }
    //fond
    if (b.tpe == "cercle")
    {
      txt += "fill: " + hex2rgba(b.fond_coul, b.fond_alpha) + "; ";
    }
    else if (b.fond_alpha != 0)
    {
      txt += "background-color: unset; box-shadow: inset 0 0 0 2000px " + hex2rgba(b.fond_coul, b.fond_alpha) + "; ";
    }
    txt += "}\n";
  }
  return txt;
}

function file_sauve(fic, txt)
{
  let xhr = new XMLHttpRequest();
  console.log(encodeURIComponent(txt));
  let ligne = "io=sauve&cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&fic=" + fic + "&v=" + encodeURIComponent(txt);
  xhr.open("POST", "io.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
function maj_police(bloc)
{
  switch (bloc.font_fam)
  {
    case "serif":
      bloc.font_fam = "ex_serif";
      break;
    case "sans-serif":
      bloc.font_fam = "ex_sans";
      break;
    case "monospace":
      bloc.font_fam = "ex_mono";
      break;
    case "handfont":
      bloc.font_fam = "ex_handfont";
      break;
    case "staypuft":
      bloc.font_fam = "ex_staypuft";
      break;
    case "ABC cursive":
      bloc.font_fam = "ex_abccursive";
      break;
    default:
      return;
  }
  g_sauver();
}
function g_confirm_exit(e)
{
  e.returnValue = "Il y a des éléments non enregistrés !"
  return "Il y a des éléments non enregistrés !";
}
function g_exporter()
{
  //on désactive la page
  document.getElementById("cr_save_bloque").style.display = "flex";
  // on commence le décodage
  let txt = "<style>\n" + file_create_css() + "</style>\n\n";
  for (let i=0; i<blocs.length; i++)
  {
    txt += bloc_create_html(blocs[i], true) + "\n";
  }
  
  let xhr = new XMLHttpRequest();
   xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      // on récupère la valeur de retour
      let rep = xhr.responseText;
      //on enlève le bloquage de la page
      document.getElementById("cr_save_bloque").style.display = "none";
      if (rep.startsWith("*"))
      {
        alert(rep.substr(1));
      }
      else
      {
        document.getElementById("cr_btn_export").style.filter = "grayscale(100%) blur(0.5px)";
        window.removeEventListener("beforeunload", g_confirm_exit);
      }
    }
  };
  let ligne = "io=sauve&cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&fic=inc&v=" + encodeURIComponent(txt) + "&v2=" + encodeURIComponent(JSON.stringify(blocs));
  xhr.open("POST", "io.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function g_sauver()
{
  let txt = JSON.stringify(blocs);
  let pos = sessionStorage.getItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_pos");
  if (!pos) pos = -1;
  pos++;
  if (pos > 20)
  {
    //il faut supprimmer ceux du début
    sessionStorage.removeItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_" + (pos-20));
  }
  sessionStorage.setItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_" + pos, txt);
  sessionStorage.setItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_pos", pos);
  document.getElementById("cr_btn_export").style.filter = "none";
  window.addEventListener("beforeunload", g_confirm_exit);
}

function g_restaurer_hist(delta)
{
  let pos = sessionStorage.getItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_pos");
  if (!pos) pos = -1;
  pos = parseInt(pos) + delta;
  let txt = sessionStorage.getItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_" + pos);
  if (!txt || txt == "") return;
  sessionStorage.setItem("../../livres/" + cat + "/" + livre + "exo/" + exo + "-hist_pos", pos);
  //on nettoie
  rendu_ini();
  blocs = [];
  selection = [];
  htmls = [];
  document.getElementById("cr_bloc_liste").options.length = 1;
  last_id = 0;
  selection_change();
  // on met les bonnes valeurs aux bons endroits
  let b = null;
  b = JSON.parse(txt);
  if (b) blocs = b;
  htmls = new Array(blocs.length);
  for (let i=0; i<blocs.length; i++)
  {
    // dans la liste
    let option = document.createElement("option");
    option.text = blocs[i].id + " (" + blocs[i].tpe + ")";
    option.value = blocs[i].id;
    document.getElementById("cr_bloc_liste").add(option);
    // et au rendu
    bloc_create_html(blocs[i]);
    rendu_add_bloc(blocs[i]);
    if (blocs[i].id > last_id) last_id = blocs[i].id;
  }
  maj_score();
}
function g_restaurer(init)
{
  //on nettoie
  if (init) g_reinit();

  //on récupère les infos dans le fichier ad'hoc
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      // on met les bonnes valeurs aux bons endroits
      let rep = xhr.responseText;
      let b = null;
      if (rep != "") b = JSON.parse(rep);
      if (b) blocs = b;
      last_id = 0;
      htmls = new Array(blocs.length);
      for (let i=0; i<blocs.length; i++)
      {
        bloc_ensure_float(blocs[i]);
        // anciennes versions
        if (blocs[i].tpe == "texte" && (typeof blocs[i].texte_defaut === "undefined")) blocs[i].texte_defaut = "";
        if (blocs[i].hasOwnProperty("html")) delete blocs[i].html;
        maj_police(blocs[i]);
        // on ajoute le bloc à la liste déroulante
        let option = document.createElement("option");
        option.text = blocs[i].id + " (" + blocs[i].tpe + ")";
        option.value = blocs[i].id;
        document.getElementById("cr_bloc_liste").add(option);
        // et au rendu
        bloc_create_html(blocs[i]);
        rendu_add_bloc(blocs[i]);
        if (blocs[i].id > last_id) last_id = blocs[i].id;
      }
      //on met les scores à jour
      maj_score();
    }
  };
  xhr.open("POST", "io.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("io=charge&cat=" + cat + "&livre=" + livre + "&exo=" + exo + "&fic=sav");
}

function g_reinit()
{
  //on nettoie
  rendu_ini();
  blocs = [];
  selection = [];
  last_id = 0;
  document.getElementById("cr_bloc_liste").options.length = 1;
  selection_change();
}

function bloc_new(tpe, txt)
{
  let bloc = {};
  last_id++;
  bloc.id = last_id;
  bloc.tpe = tpe;
  bloc.txt = txt;
  bloc_ini(bloc);
  
  window[tpe + "_ini"](bloc);
  
  blocs.push(bloc);
  htmls.push("");
  bloc_create_html(bloc);
  
  // on ajoute le bloc pour le rendu
  rendu_add_bloc(bloc);
  // et dans la liste
  let option = document.createElement("option");
  option.text = bloc.id + " (" + bloc.tpe + ")";
  option.value = bloc.id;
  document.getElementById("cr_bloc_liste").add(option);
  
  //on met à jour le score
  maj_score();
  
  g_sauver();
  return bloc;
}
// on initialise les valeurs d'options comme il faut
function bloc_ini(bloc)
{
  //polices
  bloc.font_fam = "ex_sans";
  bloc.font_size = "25";
  bloc.font_coul = "#000000";
  bloc.font_g = "0";
  bloc.font_i = "0";
  bloc.font_s = "0";
  bloc.font_b = "0";
  bloc.align = "2";
  //taille-position
  bloc.left = 5;
  bloc.top = 300;
  bloc.width = 0;
  bloc.height = 0;
  bloc.size = "auto";
  bloc.rotation = 0;
  //bordures
  bloc.bord = "hidden";
  bloc.bord_size = "1";
  bloc.bord_coul = "#000000";
  bloc.bord_rond = "0";
  //fond
  bloc.fond_coul = "#ffffff";
  bloc.fond_alpha = "0";
  //points
  bloc.points = "1";
  //interactions
  bloc.inter = "0";
  bloc.relie_id = "";
  bloc.relie_cible_de = "";
}

function bloc_mousedown(elem, event)
{
  //d'abord on enlève le focus ailleurs
  if (document.activeElement) document.activeElement.blur();
  
  if (event.target && event.target.id == "cr_rendu")
  {
    // on déselectionne tout
    selection = [];
    selection_change();
    // on lance le rectangle de sélection
    event.preventDefault();
    selection_rect.on = true;
    selection_rect.x = event.clientX;
    selection_rect.y = event.clientY;
    let rect = document.getElementById("cr_selection_rect");
    rect.style.width = "0px";
    rect.style.height = "0px";
    rect.style.left = event.clientX + "px";
    rect.style.top = event.clientY + "px";
    rect.style.display = "block";
    return;
  }
  if (elem.id == "cr_rendu") return;
  
  // on récupère le bloc correspondant
  let bloc = bloc_get_from_id(elem.id.substr(9));
  
  //on regarde si il est déjà sélectionné
  let deja = -1;
  for (let i=0; i<selection.length; i++)
  {
    if (selection[i].id == bloc.id)
    {
      deja = i;
      break;
    }
  }
  if (event.ctrlKey || event.shiftKey)
  {
    // si elem est déjà sélectionné, on le déselectionne
    //sinon, on l'ajoute à la sélection
    if (deja>=0) selection.splice(deja,1);
    else selection.push(bloc);
    selection_change();
  }
  else
  {
    // si pas ctrl et pas sélectionné, alors on sélectionne
    if (deja == -1)
    {
      selection = [bloc];
      selection_change();
    }
  }
}
function bloc_mousemove(elem, event)
{
  if (!selection_rect.on) return;
  event.preventDefault();
  let rect = document.getElementById("cr_selection_rect");
  let rx = selection_rect.x;
  let ry = selection_rect.y;
  if (event.clientX < rx)
  {
    rect.style.left = event.clientX + "px";
    rect.style.width = rx - event.clientX + "px";
  }
  else
  {
    rect.style.left = rx + "px";
    rect.style.width = event.clientX - rx + "px";
  }
  if (event.clientY < ry)
  {
    rect.style.top = event.clientY + "px";
    rect.style.height = ry - event.clientY + "px";
  }
  else
  {
    rect.style.top = ry + "px";
    rect.style.height = event.clientY - ry + "px";
  }
}
function bloc_mouseup(elem, event)
{
  if (!selection_rect.on) return;
  let rect = document.getElementById("cr_selection_rect");
  let rt = rect.getBoundingClientRect();
  rect.style.display = "none";
  selection_rect.on = false;
  event.preventDefault();
  // on détermine quels blocs sont entièrement inclus dans la sélection
  for (let i=0; i<blocs.length; i++)
  {
    let r = rendu_get_superbloc(blocs[i]).getBoundingClientRect();
    if (r.left > rt.left && r.right < rt.right && r.top > rt.top && r.bottom < rt.bottom) selection.push(blocs[i]);
  }
  selection_change();
}

function bloc_get_from_id(id)
{
  for (let i=0; i<blocs.length; i++)
  {
    if (blocs[i].id == id) return blocs[i];
  }
}
function bloc_get_html(bloc)
{
  let pos = blocs.indexOf(bloc);
  if (pos>=0) return htmls[pos];
  else return null;
}
function bloc_create_html(bloc,exp=false)
{
  let html = window[bloc.tpe + "_create_html"](bloc, bloc.txt, exp);
  //si on n'est pas en export, on affecte la valeur de retour au bloc
  if (!exp)
  {
    let pos = blocs.indexOf(bloc);
    if (pos >= 0) htmls[pos] = html;
  }
  return html;
}
function bloc_get_size_part(bloc)
{
  return "sss=\"" + bloc.font_size + "|" + bloc.bord_size + "|" + bloc.bord_rond + "|" + bloc.bord + "\"";
}
function bloc_ensure_float(bloc)
{
  bloc.width = parseFloat(bloc.width);
  bloc.height = parseFloat(bloc.height);
  bloc.top = parseFloat(bloc.top);
  bloc.left = parseFloat(bloc.left);
  bloc.rotation = parseFloat(bloc.rotation);
  bloc.points = parseInt(bloc.points);
  bloc.font_size = parseFloat(bloc.font_size);
  bloc.bord_size = parseFloat(bloc.bord_size);
  bloc.bord_rond = parseFloat(bloc.bord_rond);
  bloc.fond_alpha = parseFloat(bloc.fond_alpha);
}
//on initialise la zone de rendu (uniquement le prénom)
function rendu_ini()
{
  let htm = "<span sss=\"20\" id=\"user\">Prénom : Exemple-Prénom</span>\n";
  htm += "<img id=\"exotice\" src=\"../../exotice.svg\" />";
  htm += "<div sss=\"15\" id=\"bysa\"><img id=\"cc_img\" src=\"../../icons/cc.svg\" /><span> " + auteur + "</span></div>";
  htm += "<img id=\"coin_img\" src=\"../../icons/go-next_coin.svg\" />";
  document.getElementById("cr_rendu").innerHTML = htm;
  document.getElementById("exotice").style.height = 40*rendu.height/1000 + "px";
  document.getElementById("user").style.fontSize = 20*rendu.height/1000 + "px";
  //on ajoute les points de controle
  for (let i=1; i<9; i++)
  {
    let div = document.createElement('div');
    div.className = "cr_sel_pt";
    div.id = "cr_sel" + i;
    document.getElementById("cr_rendu").appendChild(div);
  }
  mv_pt_cursors(true);
}

// on ajoute un bloc à la zone de rendu
function rendu_add_bloc(bloc)
{
  // 2 cas : soit le bloc est déjà dans le rendu (modif) soit il faut l'ajouter
  if (document.getElementById("cr_rendu_" + bloc.id))
  {
    let htm = bloc_get_html(bloc) + "\n<div class=\"couverture\" id=\"couverture_" + bloc.id + "\"></div>";
    document.getElementById("cr_rendu_" + bloc.id).innerHTML = htm;
  }
  else
  {
    // on crée le bloc
    let htm = "<div class=\"cr_rendu_bloc ";
    if (bloc.size == "ratio") htm += "mv rs\" ";
    else if (bloc.size == "manuel") htm += "mv rsl\" ";
    else htm += "mv\" ";
    htm += "id=\"cr_rendu_" + bloc.id + "\" onmousedown=\"bloc_mousedown(this, event)\" title=\"" + bloc.tpe + " (id=" + bloc.id + ")\">\n";
    htm += bloc_get_html(bloc);
    htm += "\n<div class=\"couverture\" id=\"couverture_" + bloc.id + "\"></div></div>\n";
    
    //on l'ajoute
    document.getElementById("exotice").insertAdjacentHTML("beforeBegin", htm);
  }
  
  //on modifie les styles
  let b = document.getElementById("cr_rendu_" + bloc.id);
  let e = document.getElementById(bloc.id);

  //police
  e.style.fontFamily = bloc.font_fam;
  e.style.fontSize = (parseFloat(bloc.font_size)*rendu.height/1000) + "px";
  e.style.color = bloc.font_coul;
  if (bloc.font_g == true) e.style.fontWeight = "bold";
  if (bloc.font_i == true) e.style.fontStyle = "italic";
  if (bloc.font_s == true) e.style.textDecoration = "underline";
  if (bloc.font_b == true) e.style.textDecoration = "line-through";
  if (bloc.align == "1") e.style.justifyContent = "start";
  else if (bloc.align == "2") e.style.justifyContent = "center";
  else if (bloc.align == "3") e.style.justifyContent = "end";
  //taille-position
  //pour les texte simples, on commence par initialiser les valeurs de tailles au texte
  if (bloc.tpe == "texte_simple" || bloc.tpe == "rect" || bloc.tpe == "ligne" || bloc.tpe == "cercle" || bloc.tpe == "video")
  {
    if (parseFloat(bloc.width) == 0) bloc.width = Math.max(15, e.offsetWidth + 4)*443/rendu.width;
    if (parseFloat(bloc.height) == 0) bloc.height = Math.max(10, e.offsetHeight)*631/rendu.height;
    b.style.width = bloc.width*100/443 + "%";
    e.parentNode.style.width = "100%";
    b.style.height = bloc.height*100/631 + "%";
    e.parentNode.style.height = "100%";
    e.style.width = "100%";
    e.style.height = "100%";
  }
  else if (bloc.size == "ratio")
  {
    b.style.width = bloc.width*100/443 + "%";
    e.style.width = "100%";
    // on triche un peu pour éviter les trucs bizarres (on initialise à un carré)
    if (bloc.height == "0") b.style.height = bloc.width*100/631 + "%";
    else b.style.height = bloc.height*100/631 + "%";
  }
  else if (bloc.size == "manuel")
  {
    b.style.width = bloc.width*100/443 + "%";
    e.style.width = "100%";
    b.style.height = bloc.height*100/631 + "%";
    e.style.height = "100%";
  }
  else
  {
    // le bloc est en ligne, on connait donc ses vraies dimensions
    bloc.width = e.offsetWidth*443/rendu.width;
    bloc.height = e.offsetHeight*631/rendu.height;
  }
  b.style.left = bloc.left*100/443 + "%";
  b.style.top = bloc.top*100/631 + "%";
  if (bloc.tpe == "ligne") b.style.transformOrigin = "top center";
  if (bloc.rotation != "0") b.style.transform = "rotate(" + bloc.rotation + "deg)";
  if (bloc.tpe == "cercle" || bloc.tpe == "ligne")
  {
    let svg = document.getElementById("svg_" + bloc.id);
    svg.style.fill = hex2rgba(bloc.fond_coul, bloc.fond_alpha);
    switch (bloc.bord)
    {
      case "dashed":
        svg.style.strokeDasharray = (2 + bloc.bord_size*rendu.width/443*2) + " " + (bloc.bord_size*rendu.width/443*2);
        break;
      case "dotted":
        svg.style.strokeDasharray = "0 " + (bloc.bord_size*rendu.width/443*1.5);
        break;
    }
    if (bloc.bord != "hidden")
    {
      svg.style.strokeWidth = bloc.bord_size*rendu.width/443 + "px";
      svg.style.stroke = bloc.bord_coul;
    }
  }
  else
  {
    //bordures
    e.style.borderStyle = bloc.bord;
    e.style.borderWidth = bloc.bord_size*rendu.width/443 + "px";
    e.style.borderColor = bloc.bord_coul;
    e.style.borderRadius = bloc.bord_rond*rendu.width/443 + "px";
    //fond
    e.style.backgroundColor = hex2rgba(bloc.fond_coul, bloc.fond_alpha);
  }
}

function rendu_select_blocs()
{
  // on enlève toutes les bordures
  let elems = document.getElementsByClassName('couverture');
  for (let i=0; i<elems.length; i++)
  {
    elems[i].style.border = "hidden";
  }
  mv_place_pt();
  // on rajoute celles sélectionnées
  for (let i=0; i<selection.length; i++)
  {
    document.getElementById("couverture_" + selection[i].id).style.border = "1px dashed red";
  }
  //et les carrés pour la ligne
  //on enlève les carrés existants
  let c1 = document.getElementById("extrema_1");
  let c2 = document.getElementById("extrema_2");
  if (c1) c1.parentNode.removeChild(c1);
  if (c2) c2.parentNode.removeChild(c2);
  if (selection.length == 1 && selection[0].tpe == "ligne")
  {
    let b = selection[0];
    c1 = "<div class=\"cr_sel_pt\" id=\"extrema_1\" ligne_id=\"" + b.id + "\" style=\"display: block;left: " + ((parseFloat(b.left)+1.5)*rendu.width/443-4) + "px;top: " + (b.top*rendu.height/631-4) + "px;\"></div>";
    c2 = "<div class=\"cr_sel_pt\" id=\"extrema_2\" ligne_id=\"" + b.id + "\" style=\"display: block;left: " + ((b.x2+1.5)*rendu.width/443-4) + "px;top: " + (b.y2*rendu.height/631-4) + "px;\"></div>";
    document.getElementById("cr_rendu").innerHTML += c1 + c2;
  }
}

function rendu_get_superbloc(bloc)
{
  return document.getElementById("cr_rendu_" + bloc.id);
}

function selection_is_homogene(tpe)
{
  for (let i=0; i<selection.length; i++)
  {
    if (selection[i].tpe != tpe) return false;
  }
  return true;
}
function selection_change()
{
  rendu_select_blocs();
  
  // on s'occupe de la liste déroulante
  for (let j=0; j<document.getElementById("cr_bloc_liste").options.length; j++)
  {
    if (document.getElementById("cr_bloc_liste").options[j].value == "#")
    {
      document.getElementById("cr_bloc_liste").remove(j);
      break;
    }
  }
  if (selection.length == 1) document.getElementById("cr_bloc_liste").value = selection[0].id;
  else if (selection.length == 0) document.getElementById("cr_bloc_liste").value = "";
  else
  {
    let option = document.createElement("option");
    option.text = "multiple";
    option.value = "#";
    document.getElementById("cr_bloc_liste").add(option);
    document.getElementById("cr_bloc_liste").value = "#";
  }

  selection_update();
}

// on met à jour le panels des options, ...
function selection_update()
{
  // désactivations gloables
  // on masque toutes les options
  let elems = document.getElementsByClassName('cr_coul');
  for (let i=0; i<elems.length; i++)
  {
    elems[i].style.display = 'none';
  }
  document.getElementById("cr_texte_div").style.display = 'none';

  document.getElementById("cr_txt_ini_div").style.display = "none";
  document.getElementById("cr_img_get_div").style.display = "none";
  document.getElementById("cr_video_get_div").style.display = "none";
  document.getElementById("cr_audio_get_div").style.display = "none";
  document.getElementById("cr_expl").innerHTML = "";
  
  if (selection.length == 0)
  {
    document.getElementById("cr_bloc_format").style.display = "none";
    document.getElementById("cr_bloc_inter").style.display = "none";
  }
  else
  {
    document.getElementById("cr_bloc_format").style.display = "block";
    document.getElementById("cr_bloc_inter").style.display = "block";
  }
  
  let bloc = null;
  if (selection.length == 0)
  {
    bloc = {};
    bloc.id = "-1";
    bloc.tpe = "";
    bloc.txt = "";
    bloc_ini(bloc);
  }
  else bloc = selection[0];
  
  // on règles les options générales
  // police
  document.getElementById("cr_font_fam").value = bloc.font_fam;
  document.getElementById("cr_font_size").value = bloc.font_size;
  document.getElementById("cr_font_coul").jscolor.fromString(bloc.font_coul);
  document.getElementById("cr_font_g").checked = (bloc.font_g == "1");
  document.getElementById("cr_font_i").checked = (bloc.font_i == "1");
  document.getElementById("cr_font_s").checked = (bloc.font_s == "1");
  document.getElementById("cr_font_b").checked = (bloc.font_b == "1");
  document.getElementById("cr_align_l").checked = (bloc.align == "1");
  document.getElementById("cr_align_c").checked = (bloc.align == "2");
  document.getElementById("cr_align_r").checked = (bloc.align == "3");
  //taille-position
  document.getElementById("cr_tp_l").value = bloc.left;
  document.getElementById("cr_tp_t").value = bloc.top;
  document.getElementById("cr_tp_w").value = bloc.width;
  document.getElementById("cr_tp_h").value = bloc.height;
  document.getElementById("cr_tp_r").value = bloc.rotation;
  
  //bordures
  document.getElementById("cr_bord").value = bloc.bord;
  document.getElementById("cr_bord_size").value = bloc.bord_size;
  document.getElementById("cr_bord_coul").jscolor.fromString(bloc.bord_coul);
  document.getElementById("cr_bord_rond").value = bloc.bord_rond;
  //fond
  document.getElementById("cr_fond_coul").jscolor.fromString(bloc.fond_coul);
  document.getElementById("cr_fond_alpha").value = bloc.fond_alpha;
  
  document.getElementById("cr_txt_ini").value = bloc.txt;
  
  //les interactions
  document.getElementById("cr_inter_0").disabled = true;
  document.getElementById("cr_inter_1").disabled = true;
  document.getElementById("cr_inter_2").disabled = true;
  document.getElementById("cr_relie_id").disabled = true;
  document.getElementById("cr_points").disabled = false;
  document.getElementById("cr_points").value = bloc.points;

  //on enabled tous les éléments classiques. Charge aux code specifiques de les désactiver
  elems = document.getElementsByClassName('cr_');
  for (let i=0; i<elems.length; i++)
  {
    elems[i].disabled = false;
  }
  if (!selection_is_homogene(bloc.tpe) || bloc.size == "auto")
  {
    document.getElementById("cr_tp_w").disabled = true;
    document.getElementById("cr_tp_h").disabled = true;
  }
  
  // on fait les réglages spécifiques
  for (let i=0; i<selection.length; i++)
  {
    window[selection[i].tpe + "_sel_update"]();
  }
  
  // on regarde aussi les icones d'alignement, ...
  document.getElementById("btn_aligne").style.filter = "grayscale(100%) blur(0.5px)";
  document.getElementById("btn_repart").style.filter = "grayscale(100%) blur(0.5px)";
  document.getElementById("btn_plans").style.filter = "grayscale(100%) blur(0.5px)";
  if (selection.length > 0)
  {
    document.getElementById("btn_aligne").style.filter = "";
    document.getElementById("btn_plans").style.filter = "";
  }
  if (selection.length > 2)
  {
    document.getElementById("btn_repart").style.filter = "";
  }
}

function selection_update_interactions()
{
  if (selection.length == 0) return;
  let bloc = selection[0];
  //interactions
  document.getElementById("cr_inter_0").disabled = false;
  document.getElementById("cr_inter_1").disabled = false;
  document.getElementById("cr_inter_2").disabled = false;
  
  switch (bloc.inter)
  {
    case "0":
      document.getElementById("cr_inter_0").checked = true;
      break;
    case "1":
      document.getElementById("cr_inter_1").checked = true;
      if (selection.length == 1) document.getElementById("cr_relie_id").disabled = false;
      document.getElementById("cr_points").disabled = false;
      break;
    case "2":
      document.getElementById("cr_inter_2").checked = true;
      break;
  }
  if (bloc.relie_cible_de != "")
  {
    // ça veut dire que le réglage a été fait ailleurs
    document.getElementById("cr_relie_id").value = bloc.relie_cible_de;
    document.getElementById("cr_inter_0").disabled = true;
    document.getElementById("cr_inter_1").disabled = true;
    document.getElementById("cr_inter_2").disabled = true;
    document.getElementById("cr_relie_id").disabled = true;
    document.getElementById("cr_points").disabled = true;
  }
  else document.getElementById("cr_relie_id").value = bloc.relie_id;
}

function check_new()
{
  //on demande le texte initial
  let txt = prompt("cases à cocher\n\nEncadrer les choix par '|' ; Le texte à cocher commence par * Les autres par $\n(Les chats sont |*des mammifères$des oiseaux*des félins|)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("check", txt);
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function check_ini(bloc)
{
  // rien de sécial à faire !
}
function check_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length != 1) return;
  let bloc = selection[0];
  
  document.getElementById("cr_expl").innerHTML = "<b>cases à cocher</b><br/>Encadrer les choix par '|' ; Le texte juste commence par * Les autres par $<br/>(Les chats sont |*des mammifères$des oiseaux*des félins|)";
  document.getElementById("cr_txt_ini_div").style.display = "block";
}
function check_create_html(bloc, txt)
{
  return radio_create_html(bloc, txt);
}
function radiobtn_new()
{
  //on demande le texte initial
  let txt = prompt("boutons choix\n\nEncadrer les choix par '|' ; Le texte juste commence par * Les autres par $\n(Les chats sont |$des plantes$des oiseaux*des félins|)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("radiobtn", txt);
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function radiobtn_ini(bloc)
{
  bloc.radiobtn_coul1 = "#D1CFCF";
  bloc.radiobtn_coul2 = "#888888";
}
function radiobtn_sel_update()
{
  // on affiche le texte de création
  if (selection.length == 1)
  {
    let bloc = selection[0];
    
    document.getElementById("cr_expl").innerHTML = "<b>boutons choix</b><br/>Encadrer les choix par '|' ; Le texte juste commence par * Les autres par $<br/>(Les chats sont |$des plantes$des oiseaux*des félins|)";
    document.getElementById("cr_txt_ini_div").style.display = "block";
  }
  //on regarde si la sélection est homogène
  if (selection.length>0 && selection_is_homogene("radiobtn"))
  {
    let elems = document.getElementsByClassName('cr_coul');
    elems[1].style.display = "block";
    elems[2].style.display = "block";
    document.getElementById("cr_coul1_barre").style.display = "none";
    document.getElementById("cr_coul1_maj").style.display = "none";
    document.getElementById("cr_coul1_suff").style.display = "none";
    document.getElementById("cr_coul1_suff_txt").style.display = "none";
    document.getElementById("cr_coul2_barre").style.display = "none";
    document.getElementById("cr_coul2_maj").style.display = "none";
    document.getElementById("cr_coul2_suff").style.display = "none";
    document.getElementById("cr_coul2_suff_txt").style.display = "none";
    document.getElementById("cr_coul1").jscolor.fromString(selection[0].radiobtn_coul1);
    document.getElementById("cr_coul2").jscolor.fromString(selection[0].radiobtn_coul2);
  }
}
function radiobtn_create_html(bloc, txt)
{
  return radio_create_html(bloc, txt);
}
function radio_new()
{
  //on demande le texte initial
  let txt = prompt("choix uniques\n\nEncadrer les choix par '|' ; Le texte à cocher commence par * Les autres par $\n(Les chats sont |$des plantes$des oiseaux*des félins|)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("radio", txt);
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function radio_ini(bloc)
{
  // rien de sécial à faire !
}
function radio_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length != 1) return;
  let bloc = selection[0];
  
  document.getElementById("cr_expl").innerHTML = "<b>choix unique</b><br/>Encadrer les choix par '|' ; Le texte juste commence par * Les autres par $<br/>(Les chats sont |$des plantes$des oiseaux*des félins|)";
  document.getElementById("cr_txt_ini_div").style.display = "block";
}
function radio_create_html(bloc, txt)
{
  // ça marche aussi pour les radiobtn et les check
  let tp = "radio";
  let cl = bloc.tpe;
  if (bloc.tpe == "check")
  {
    tp = "checkbox";
    cl = "radio";
  }

  let htm = "";
  if (bloc.tpe == "radiobtn")
  {
    htm += "<style>[id=\"" + bloc.id + "\"] label {background-color: " + bloc.radiobtn_coul1;
    htm += ";} [id=\"" + bloc.id + "\"] input[type=\"radio\"]:checked + label {background-color: " + bloc.radiobtn_coul2 + ";}</style>\n";
  }
  htm += "<div " + bloc_get_size_part(bloc) + " class=\"item lignef " + cl + "\" tpe=\"" + bloc.tpe + "\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.tpe == "radiobtn") htm += "options=\"" + bloc.radiobtn_coul1 + "|" + bloc.radiobtn_coul2 + "\" ";
  htm += ">\n";
  //on coupe suivant '|'
  let vals = txt.split("|");
  if (vals.length>0) htm += "  <div>" + txt_spaces(vals[0]) + "</div>\n";
  let item = 0;
  if (vals.length>1)
  {
    // le choix
    htm += "  <form>\n";
    let v = vals[1].replace(/\*/g,"|*").replace(/\$/g,"|$").split("|");
    for (let k=0; k<v.length; k++)
    {
      if (v[k].startsWith("*"))
      {
        if (k>1) htm += "    <br/>\n";
        htm += "    <input type=\"" + tp + "\" itemid=\"" + bloc.id + "\" class=\"exo\" onclick=\"change(this)\" value=\"1\" id=\"rb" + bloc.id + "_" + item + "\" ";
        if (tp == "radio") htm += "name=\"" + bloc.id + "\"";
        htm += ">";
        htm += "<label for=\"rb" + bloc.id + "_" + item + "\">" + txt_spaces(v[k].substr(1)) + "</label>\n";
      }
      else if (v[k].startsWith("$"))
      {
        if (k>1) htm += "   <br/>\n";
        htm += "    <input type=\"" + tp + "\" itemid=\"" + bloc.id + "\" class=\"exo\" onclick=\"change(this)\" value=\"0\" id=\"rb" + bloc.id + "_" + item + "\" ";
        if (tp == "radio") htm += "name=\"" + bloc.id + "\"";
        htm += ">";
        htm += "<label for=\"rb" + bloc.id + "_" + item + "\">" + txt_spaces(v[k].substr(1)) + "</label>\n";
      }
      item++;
    }
    htm += "  </form>\n";
  }
  if (vals.length>2) htm += "  <div>" + txt_spaces(vals[2]) + "</div>\n";
  htm += "</div>\n";
  
  return htm;
}

function combo_new()
{
  //on demande le texte initial
  let txt = prompt("liste déroulante\n\nEncadrer les choix par '|' ; Le texte juste commence par * Les autres par $\n(Les chats sont |$des arbres$des oiseaux*des félins|)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("combo", txt);
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function combo_create_html(bloc, txt)
{
  let htm = "";
  htm += "<div " + bloc_get_size_part(bloc) + " class=\"item lignef combo\" tpe=\"combo\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\">\n";
  //on coupe suivant '|'
  let vals = txt.split("|");
  if (vals.length>0) htm += "  <div>" + txt_spaces(vals[0]) + "</div>\n";
  if (vals.length>1)
  {
    // le choix
    htm += "  <div>\n";
    htm += "    <select id=\"combo_" + bloc.id + "\" itemid=\"" + bloc.id + "\" class=\"exo\" onchange=\"change(this)\">\n";
    htm += "      <option value=\"0\">--</option>\n";
    let v = vals[1].replace(/\*/g,"|*").replace(/\$/g,"|$").split("|");
    let juste = "";
    for (let k=0; k<v.length; k++)
    {
      if (v[k].startsWith("*"))
      {
        htm += "      <option value=\"1\">" + txt_spaces(v[k].substr(1)) + "</option>\n";
        juste = txt_spaces(v[k].substr(1))
      }
      else if (v[k].startsWith("$"))
      {
        htm += "      <option value=\"0\">" + txt_spaces(v[k].substr(1)) + "</option>\n";
      }
    }
    htm += "    </select>\n";
    htm += "    <div class=\"combo_corr\">" + txt_spaces(juste) + "</div>\n";
    htm += "  </div>\n";
  }
  if (vals.length>2) htm += "  <div>" + txt_spaces(vals[2]) + "</div>\n";
  htm += "</div>\n";
  
  return htm;
}
function combo_ini(bloc)
{
  // rien de sécial à faire !
}
function combo_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length != 1) return;
  let bloc = selection[0];
  
  document.getElementById("cr_expl").innerHTML = "<b>liste déroulante</b><br/>Encadrer les choix par '|' ; Le texte juste commence par * Les autres par $<br/>(Les chats sont |$des arbres$des oiseaux*des félins|)";
  document.getElementById("cr_txt_ini_div").style.display = "block";
}

function texte_new()
{
  //on demande le texte initial
  let txt = prompt("zone de texte\n\nEncadrer le texte juste par '|' ('#' = tout est juste)\n(La souris|a|peur du chat)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("texte", txt);
  
  //on détecte la taille de base du texte à rentrer
  let v = txt.split("|");
  if (v.length>1 && v[1] != "#")
  {
    bloc.texte_l = v[1].length + 1;
    bloc_create_html(bloc);
    rendu_add_bloc(bloc);
  }
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function texte_create_html(bloc, txt)
{
  // on récupère les infos de la zone de texte
  let l = bloc.texte_l;
  let h = bloc.texte_h;
  let tpe = bloc.texte_t;
  if (!tpe) tpe = "0";
  let enter = bloc.texte_e;
  let comp = bloc.texte_c;
  let corr = "texte_corr";
  if (bloc.texte_corr == "1") corr += "2";
  
  let htm = "";
  htm += "<div " + bloc_get_size_part(bloc) + " class=\"item lignef texte\" tpe=\"texte\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\" options=\"";
  htm += comp + "|" + enter + "|" + bloc.texte_corr + "\" >\n";
  //on coupe suivant '|'
  let vals = txt_spaces(txt).split("|");
  if (l == 0) htm += "  <div style=\"width: 100%;\">\n"
  if (vals.length>0) htm += "  <div>" + vals[0] + "<div class=\"" + corr + "\">&nbsp;</div></div>\n";
  if (vals.length>1)
  {
    htm += "  <div>\n";
    if (h > 1) htm += "   <textarea rows=\"" + h + "\" ";
    else if (tpe == "1") htm += "   <input type=\"number\" ";
    else htm += "   <input type=\"text\" ";
    htm += "class=\"exo\" id=\"texte_" + bloc.id + "\" itemid=\"" + bloc.id + "\" onKeyUp=\"change(this)\" value=\"" + bloc.texte_defaut + "\" ";
    if (enter==0 && h > 1) htm += "onkeydown=\"texte_stop_enter(event)\" ";
    if (l==0) htm += "style=\"box-sizing: border-box; width:100%;\">";
    else if (tpe=="1") htm += "style=\"box-sizing: border-box; width:"+ (parseInt(l)*0.6+0.4) +"em;\">";
    else if (h>1) htm += "cols=\"" + l + "\">";
    else htm += "size=\"" + l + "\">";
    if (h>1) htm += "</textarea>";
    htm += "\n    <div class=\"" + corr + "\">" + vals[1] + "</div>\n";
    htm += "  </div>\n";
  }
  if (vals.length>2) htm += "  <div>" + vals[2] + "<div class=\"" + corr + "\">&nbsp;</div></div>\n";
  if (l == 0) htm += "  </div>\n";
  htm += "</div>\n";
  
  return htm;
}
function texte_ini(bloc)
{
  bloc.texte_l = 10;
  bloc.texte_h = 1;
  bloc.texte_e = 0;
  bloc.texte_c = 0;
  bloc.texte_t = 0;
  bloc.texte_corr = 0;
  bloc.texte_defaut = "";
}
function texte_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    document.getElementById("cr_expl").innerHTML = "<b>zone de texte</b><br/>Encadrer le texte juste par '|' ('#' = tout est juste)<br/>(La souris|a|peur du chat)";
    document.getElementById("cr_txt_ini_div").style.display = "block";
  }
  if (selection.length > 0 && selection_is_homogene("texte"))
  {
    let bloc = selection[0];
    document.getElementById("cr_texte_defaut").value = bloc.texte_defaut;
    document.getElementById("cr_texte_l").value = bloc.texte_l;
    document.getElementById("cr_texte_h").value = bloc.texte_h;
    document.getElementById("cr_texte_e").value = bloc.texte_e;
    document.getElementById("cr_texte_c").value = bloc.texte_c;
    document.getElementById("cr_texte_t").value = bloc.texte_t;
    document.getElementById("cr_texte_corr").checked = (bloc.texte_corr == "1");
    document.getElementById("cr_texte_div").style.display = "block";
  }
}

function multi_new()
{
  //on demande le texte initial
  let txt = prompt("blocs multi-positions\n\nEncadrer les blocs par '|' ; Les blocs à colorer commencent par le numéro de la couleur\n(1Le chat|2mange|les souris.)", "");
  if (!txt) return;
  
  //on crée le nouveau bloc
  let bloc = bloc_new("multi", txt);
  //on règle le nombre de couleurs par défaut
  let v = txt.split("|");
  let nb = 0;
  for (let i=0; i<v.length; i++)
  {
    let n = parseInt(v[i].substr(0,1));
    if (!isNaN(n)) nb = Math.max(nb, n);
  }
  nb = Math.max(Math.min(5, nb), 1); // doit être en 1 et 5
  bloc.multi_coul.length = nb;
  bloc.multi_maj.length = nb;
  bloc.multi_barre.length = nb;
  bloc.multi_suff.length = nb;
  bloc.points = nb;
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function multi_create_html(bloc, txt, exp)
{
  let htm = "";
  htm += "<div " + bloc_get_size_part(bloc) + " class=\"item ligne2f multi\" tpe=\"multi\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\"";
  let opts = "";
  let maj = "";
  let suff = "";
  let barre = "";
  for (let i=0; i<bloc.multi_coul.length; i++)
  {
    if (i>0)
    {
      opts += "|";
      maj += "|";
      suff += "|";
      barre += "|";
    }
    opts += hex2rgb(bloc.multi_coul[i]);
    maj += bloc.multi_maj[i];
    suff += bloc.multi_suff[i];
    barre += bloc.multi_barre[i];
  }
  htm += " options=\"" + opts + "\" maj=\"" + maj + "\" suff=\"" + suff + "\" barre=\"" + barre + "\">\n";
  //on coupe suivant '|'
  let vals = txt.split("|");
  let htm2 = "";
  for (let i=0; i<vals.length; i++)
  {
    if (vals[i].length>0)
    {
      let tx = vals[i];
      htm += "<span id=\"multi_" + bloc.id + "_" + i + "\" class=\"exo\" itemid=\"" + bloc.id + "\" onclick=\"change(this)\" ";
      htm2 += "<span ";
      let idjuste = parseInt(tx.substr(0,1));
      let tx2 = tx;
      if (idjuste < 6)
      {
        htm += "juste=\"" + idjuste + "\" ";
        htm2 += "style=\"background-color: " + bloc.multi_coul[idjuste - 1] + ";";
        if (bloc.multi_barre[idjuste - 1] == "1") htm2 += "text-decoration: line-through;";
        htm2 += "\" ";
        tx = tx.substr(1);
        tx2 = tx;
        if (bloc.multi_maj[idjuste - 1] == "1") tx2 = tx.substr(0,1).toUpperCase() + tx.substr(1);
        if (bloc.multi_suff[idjuste - 1] != "") tx2 = tx + bloc.multi_suff[idjuste - 1];
      }
      htm += ">" + txt_spaces(tx) + "<span class=\"multi_ini\">" + txt_spaces(tx) +"</span></span>";
      htm2 += ">" + txt_spaces(tx2) + "</span>";
    }
  }
  htm += "<img class=\"multi_cim\" ";
  if (exp) htm += "src=\"../icons/vide.svg\" />\n";
  else htm += "src=\"../../icons/vide.svg\" />\n";
  htm += "  <br/>\n";
  htm += "  <span class=\"multi_corr\" itemid=\"" + bloc.id + "\">\n";
  htm += htm2;
  htm += "  </span>\n";
  htm += "</div>\n";
  
  return htm;
}
function multi_ini(bloc)
{
  bloc.multi_coul = ["#00ff00", "#ff0000", "#0000ff", "#ffff00", "#00ffff"];
  bloc.multi_maj = ["0", "0", "0", "0", "0"];
  bloc.multi_suff = ["", "", "", "", ""];
  bloc.multi_barre = ["0", "0", "0", "0", "0"];
}
function multi_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    document.getElementById("cr_expl").innerHTML = "<b>blocs multi-positions</b><br/>Encadrer les blocs par '|' ; Les blocs à colorer commencent par le numéro de la couleur<br/>(1Le chat|2mange|les souris.)";
    document.getElementById("cr_txt_ini_div").style.display = "block";
  }
  
  if (selection.length > 0 && selection_is_homogene("multi"))
  {
    let bloc = selection[0];
    document.getElementById("cr_coul_nb").value = bloc.multi_coul.length;
    for (let i=0; i<bloc.multi_coul.length; i++)
    {
      let tx = "cr_coul" + (i+1);
      document.getElementById(tx).jscolor.fromString(bloc.multi_coul[i]);
      document.getElementById(tx + "_barre").checked = (bloc.multi_barre[i] == "1");
      document.getElementById(tx + "_maj").checked = (bloc.multi_maj[i] == "1");
      document.getElementById(tx + "_suff").checked = (bloc.multi_suff[i] != "");
      document.getElementById(tx + "_suff_txt").value = bloc.multi_suff[i];
    }
    document.getElementById("cr_coul_nb").style.display = "block";
    cr_coul_nb_change(document.getElementById("cr_coul_nb"), false);
  }
}

function cible_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("cible", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function cible_create_html(bloc, txt)
{
  // on récupère les infos de la zone de texte
  let l = bloc.texte_l;
  let h = bloc.texte_h;
  let enter = bloc.texte_e;
  let comp = bloc.texte_c;
  
  let htm = "<div " + bloc_get_size_part(bloc);
  htm += " class=\"item lignef cible exo\" tpe=\"cible\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" juste=\"" + txt + "\" points=\"" + bloc.points + "\">\n";
  htm += "</div>\n";
  
  return htm;
}
function cible_ini(bloc)
{
  bloc.width = 50;
  bloc.height = 50;
  bloc.size = "manuel";
  bloc.fond_coul = "#6B6B6B";
  bloc.fond_alpha = 20;
}
function cible_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length != 1) return;
  let bloc = selection[0];
  
  document.getElementById("cr_expl").innerHTML = "<b>zone cible</b><br/>Identifiant des objets qui peuvent être posés<br/>séparer les identifiant par '|'";
  document.getElementById("cr_txt_ini_div").style.display = "block";
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
}

function image_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("image", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function image_create_html(bloc, txt, exp)
{
  let htm = "";
  htm += "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <img ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += "ondragstart=\"return false;\" ";
  htm += bloc_get_size_part(bloc) + " class=\"item exo image\" tpe=\"image\" item=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  if (exp) htm += "src=\"../livres/" + cat + "/" + livre + "/" + bloc.img_rpath;
  else htm += "src=\"../../livres/" + cat + "/" + livre + "/" + bloc.img_rpath;
  htm += "\" id=\"" + bloc.id + "\" />\n</div>";
  return htm;
}
function image_ini(bloc)
{
  // rien à faire
  bloc.img_rpath = "";
  bloc.img_vpath = "";
  bloc.img_name = "";
  bloc.width = 50;
  bloc.height = 50;
  bloc.size = "ratio";
  bloc.points = 0;
}
function image_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    
    document.getElementById("cr_img_select").value = bloc.img_name;
    
    document.getElementById("cr_expl").innerHTML = "<b>image</b><br/>Sélectionner l'image choisie.";
    document.getElementById("cr_img_get_div").style.display = "block";
  }
  if (selection.length > 0 && selection_is_homogene("image")) selection_update_interactions();
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
}

function video_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("video", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function video_create_html(bloc, txt, exp)
{
  let htm = "";
  htm += "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <div ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += "ondragstart=\"return false;\" ";
  htm += bloc_get_size_part(bloc) + " class=\"item exo video\" tpe=\"video\" item=\"" + bloc.id + "\" points=\"" + bloc.points + "\" id=\"" + bloc.id + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  htm += ">";
  
  //on inclu la vidéo suivant le type
  switch (bloc.video_type)
  {
    case 1:
      htm += "<iframe src=\"https://www.youtube.com/embed/" + bloc.video_name + "\" frameborder=\"0\" allowfullscreen></iframe>";
      break;
    case 2:
      htm += "<iframe src=\"//www.dailymotion.com/embed/video/" + bloc.video_name + "\" frameborder=\"0\" allowfullscreen></iframe>";
      break;
    case 3:
      htm += "<iframe src=\"https://player.vimeo.com/video/" + bloc.video_name + "\" frameborder=\"0\" allowfullscreen></iframe>";
      break;
    case 99:
      htm += "<video controls><source ";
      if (exp) htm += "src=\"../livres/" + cat + "/" + livre + "/" + bloc.video_name;
      else htm += "src=\"../../livres/" + cat + "/" + livre + "/" + bloc.video_name;
      htm += "\" /></video>";
      break;
  }
  
  htm += "</div></div>";
  return htm;
}
function video_ini(bloc)
{
  // rien à faire
  bloc.video_name = "";
  bloc.video_type = 0;
  bloc.width = 250;
  bloc.height = 100;
  bloc.size = "manuel";
  bloc.points = 0;
}
function video_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    
    let sel = "";
    document.getElementById("cr_video_txt").value = "";
    if (bloc.video_type == 0) document.getElementById("cr_video_txt").disabled = true;
    if (bloc.video_type < 99)
    {
      sel = bloc.video_type;
      document.getElementById("cr_video_txt").disabled = false;
      document.getElementById("cr_video_txt").value = bloc.video_name;
    }
    else if (bloc.video_type == 99)
    {
      sel = bloc.video_name.substr(7);
      document.getElementById("cr_video_txt").disabled = true;
    }
    
    document.getElementById("cr_video_select").value = sel;
    
    document.getElementById("cr_expl").innerHTML = "<b>vidéo</b><br/>Sélectionner la vidéo.";
    document.getElementById("cr_video_get_div").style.display = "block";
  }
  if (selection.length > 0 && selection_is_homogene("video")) selection_update_interactions();
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
}

function texte_simple_new(vide)
{
  //on demande le texte initial
  let txt = "";
  if (!vide) txt = prompt("texte\n\nEntrer le texte\nSyntaxe des liens : |nom du lien|adresse internet|\nExemple : Plus d'infos sur |ce site|http://www.mon-site.com|", "");
  if (!txt )
  {
    //forcer charactère espace pour éviter cadre vide
    txt = "";
  }
  
  //on crée le nouveau bloc
  let bloc = null;
  if (vide) bloc = bloc_new("rect", txt);
  else bloc = bloc_new("texte_simple", txt);
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function texte_simple_create_html(bloc, txt)
{
  let htm = "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <div ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += bloc_get_size_part(bloc) + " class=\"item lignef texte_simple exo\" tpe=\"texte_simple\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  htm += ">\n";
  // onsépare pour voir l'éventuel lien
  txt = txt.replace(/(?:\r\n|\r|\n)/g, '<br />')
  let vals = txt.split("|");
  htm += txt_spaces(vals[0]);
  if (vals.length == 3)
  {
    htm += "<a href=\"" + vals[1] + "\">" + vals[1] + "</a>" + txt_spaces(vals[2]);
  }
  else if (vals.length > 3)
  {
    htm += "<a href=\"" + vals[2] + "\">" + txt_spaces(vals[1]) + "</a>" + txt_spaces(vals[3]);
  }
  htm += "</div>\n</div>\n";
  
  return htm;
}
function texte_simple_ini(bloc)
{
  // rien à faire
  bloc.points = 0;
  bloc.size = "manuel";
  if (bloc.tpe == "rect")
  {
    bloc.width = 40;
    bloc.height = 40;
    bloc.fond_coul = "#4AC1D8";
    bloc.fond_alpha = 100;
  }
}
function texte_simple_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    document.getElementById("cr_expl").innerHTML = "<b>texte simple</b><br/>Entrer le texte<br/>Syntaxe des liens : |nom du lien|adresse internet|<br/>Exemple : Plus d'infos sur |ce site|http://www.mon-site.com|";
    document.getElementById("cr_txt_ini_div").style.display = "block";
  }
  
  if (selection.length > 0 && selection_is_homogene(selection[0].tpe)) selection_update_interactions();
}

function rect_create_html(bloc, txt)
{
  return texte_simple_create_html(bloc, txt);
}
function rect_ini(bloc)
{
  texte_simple_ini(bloc);
}
function rect_sel_update()
{
  texte_simple_sel_update();
}

function audio_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("audio", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function audio_create_html(bloc, txt, exp)
{
  let htm = "";
  htm += "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <img ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += bloc_get_size_part(bloc) + " class=\"item exo audio\" tpe=\"audio\" item=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  if (exp) htm += "src=\"../icons/audacity.svg\" ";
  else htm += "src=\"../../icons/audacity.svg\" ";
  htm += "onclick=\"audio_play(this)\"";
  htm += " id=\"" + bloc.id + "\" />\n";
  htm += "<audio class=\"audio_src\" id=\"audio_" + bloc.id + "\" ";
  if (exp) htm += "src=\"../livres/" + cat + "/" + livre + "/" + bloc.audio_name + "\"";
  htm += "></audio>";
  htm += "</div>";
  return htm;
}
function audio_ini(bloc)
{
  bloc.audio_name = "";
  bloc.width = 32;
  bloc.height = 32;
  bloc.size = "ratio";
  bloc.points = 0;
}
function audio_sel_update()
{
  // on récupère le bloc sélectionné
  if (selection.length == 1)
  {
    let bloc = selection[0];
    
    document.getElementById("cr_audio_select").value = bloc.audio_name.substr(5);
    
    document.getElementById("cr_expl").innerHTML = "<b>audio</b><br/>Sélectionner un fichier sonore.";
    document.getElementById("cr_audio_get_div").style.display = "flex";
    document.getElementById("cr_record_start").setAttribute("etat", "2");
    document.getElementById("cr_record_start").style.backgroundColor = "#D8D8D8";
    document.getElementById("cr_record_start").src = "../../icons/media-playback-start.svg";
    document.getElementById("cr_record_start").style.display = "inline";
    document.getElementById("cr_record_save").style.display = "none";
    document.getElementById("cr_record_delete").style.display = "none";
    document.getElementById("cr_record_audio").src = "../../livres/" + cat + "/" + livre + "/" + bloc.audio_name;
  }
  if (selection.length > 0 && selection_is_homogene("audio")) selection_update_interactions();
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
}

function cercle_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("cercle", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function cercle_create_html(bloc, txt)
{
  //on calcule tous les paramètres
  let htm = "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <svg ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += bloc_get_size_part(bloc) + " preserveAspectRatio=\"none\" viewbox=\"0 0 100 100\" class=\"item lignef svg exo\" tpe=\"cercle\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  htm += ">\n";
  htm += "<ellipse vector-effect=\"non-scaling-stroke\" cx=\"50\" cy=\"50\" rx=\"50\" ry=\"50\" id=\"svg_" + bloc.id + "\" />";
  htm += "</svg>\n</div>\n";
  
  return htm;
}
function cercle_ini(bloc)
{
  // rien à faire
  bloc.points = 0;
  bloc.size = "manuel";
  bloc.height = 40;
  bloc.width = 40;
  bloc.fond_coul = "#4AC1D8";
  bloc.fond_alpha = 100;
}
function cercle_sel_update()
{
  if (selection.length > 0 && selection_is_homogene("cercle")) selection_update_interactions();
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
  //coins arrondis
  document.getElementById("cr_bord_rond").disabled = true;
}

function ligne_new()
{
  //on crée le nouveau bloc
  let bloc = bloc_new("ligne", "");
  
  //on le sélectionne
  selection = [bloc];
  selection_change();
}
function ligne_calc(bloc, x1, y1, x2, y2)
{
  bloc.left = x1;
  bloc.top = y1;
  bloc.x2 = x2;
  bloc.y2 = y2;
  bloc.height = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    
  let angle = 180 / 3.1415 * Math.acos((y2 - y1) / bloc.height);
  if(x2 > x1) angle *= -1;
  bloc.rotation = angle;
}
function ligne_create_html(bloc, txt)
{
  //on calcule tous les paramètres
  let htm = "<div";
  if (bloc.inter == 2) htm += " id=\"cible_" + bloc.id + "\"";
  htm += ">\n  <svg ";
  if (bloc.inter == 2) htm += "draggable=true ";
  htm += bloc_get_size_part(bloc) + " class=\"item lignef svg exo\" preserveAspectRatio=\"none\" viewbox=\"0 0 100 100\" tpe=\"ligne\" item=\"" + bloc.id + "\" id=\"" + bloc.id + "\" points=\"" + bloc.points + "\" ";
  if (bloc.inter == 1)
  {
    htm += "line=\"1\" ";
    if (bloc.relie_id != "*") htm += "lineok=\"" + bloc.relie_id + "\" ";
  }
  htm += ">\n";
  htm += "<line vector-effect=\"non-scaling-stroke\" x1=\"50\" y1=\"0\" x2=\"50\" y2=\"100\" id=\"svg_" + bloc.id + "\" />";
  htm += "</svg>\n</div>\n";
  
  return htm;
}
function ligne_ini(bloc)
{
  // rien à faire
  bloc.points = 0;
  bloc.size = "auto";
  bloc.x2 = bloc.left + 50;
  bloc.y2 = bloc.top + 50;
  bloc.width = 3;
  bloc.height = 70.71;
  bloc.rotation = -45;
  bloc.bord_coul = "#4AC1D8";
  bloc.bord = "solid";
  bloc.bord_size = 4;
}
function ligne_sel_update()
{
  if (selection.length > 0 && selection_is_homogene("ligne")) selection_update_interactions();
  //pas de texte...
  document.getElementById("cr_font_fam").disabled = true;
  document.getElementById("cr_font_size").disabled = true;
  document.getElementById("cr_font_g").disabled = true;
  document.getElementById("cr_font_i").disabled = true;
  document.getElementById("cr_font_s").disabled = true;
  document.getElementById("cr_font_b").disabled = true;
  document.getElementById("cr_font_coul").disabled = true;
  //coins arrondis
  document.getElementById("cr_bord_rond").disabled = true;
  //fond
  document.getElementById("cr_fond_coul").disabled = true;
  document.getElementById("cr_fond_alpha").disabled = true;
}

function maj_score()
{
  let tot = 0;
  for (let i=0; i<blocs.length; i++)
  {
    tot += parseInt(blocs[i].points);
  }
  document.getElementById("cr_score").innerHTML = tot;
}
