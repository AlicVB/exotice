 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
let mv = {};

function mv_rs_ini()
{
  // ini de l'objet général
  mv.on = false;
  mv.x = 0;
  mv.y = 0;
  mv.pt = null;
  mv.pt_num = -1;
  mv.rect = {};
  mv.rectr = {};
  mv.extrema = null;
  mv.ok = false;
  mv.change = false;
  
  // on ajoute les évennements de déplacement
  document.addEventListener("mousedown", mv_mousedown);
  document.addEventListener("mousemove", mv_mousemove);
  document.addEventListener("mouseup", mv_mouseup);
}

function mv_mousedown(event)
{
  if (!event.target) return;
  if (!event.target.parentNode) return;
  if (!event.target.classList) return;
  if (!event.target.classList.contains("cr_sel_pt") && !event.target.parentNode.classList.contains("mv")) return;
  event.preventDefault();
  mv.on = true;
  mv.x = event.clientX;
  mv.y = event.clientY;
  mv.pt = null;
  mv.extrema = null;
  mv.change = false;
  if (event.target.id.substr(0,6) == "cr_sel")
  {
    mv.pt = event.target;
    mv.pt_num = parseInt(event.target.id.substr(6));
  }
  else if (event.target.id.substr(0,8) == "extrema_")
  {
    mv.extrema = event.target;
  }
}

function mv_mousemove(event)
{
  if (!mv.on) return;
  event.preventDefault();
  if (mv.pt)
  {
    let pt = document.getElementById("cr_sel" + mv.pt_num).getBoundingClientRect();
    mv.x = pt.left + 4;
    mv.y = pt.top + 4;
  }
  let edx = event.clientX - mv.x;
  let edy = event.clientY - mv.y;
  mv.change = true;
  // on ne veut pas qu'il y ait un élément qui sorte du rendu !
  let rd = document.getElementById("cr_rendu").getBoundingClientRect();
  if (mv.extrema)
  {
    let r = mv.extrema.getBoundingClientRect();
    edx = Math.max(edx, rd.left + 5 - r.left);
    edx = Math.min(edx, rd.right - 5 - r.right);
    edy = Math.max(edy, rd.top + 5 - r.top);
    edy = Math.min(edy, rd.bottom - 5 - r.bottom);
  }
  else if (mv.pt)
  {
    let r = mv.pt.getBoundingClientRect();
    edx = Math.max(edx, rd.left + 5 - r.left);
    edx = Math.min(edx, rd.right - 5 - r.right);
    edy = Math.max(edy, rd.top + 5 - r.top);
    edy = Math.min(edy, rd.bottom - 5 - r.bottom);
  }
  else
  {
    for (let i=0; i<selection.length; i++)
    {
      let r = rendu_get_superbloc(selection[i]).getBoundingClientRect();
      edx = Math.max(edx, rd.left + 5 - r.left);
      edx = Math.min(edx, rd.right - 5 - r.right);
      edy = Math.max(edy, rd.top + 5 - r.top);
      edy = Math.min(edy, rd.bottom - 5 - r.bottom);
    }
  }
  mv.x = mv.x + edx;
  mv.y = mv.y + edy;
  let dx = edx*443/rendu.width;
  let dy = edy*631/rendu.height;
  
  if (mv.extrema)
  {
    let bloc = selection[0];
    //on déplace l'extrema
    mv.extrema.style.top = parseFloat(mv.extrema.style.top) + edy + "px";
    mv.extrema.style.left = parseFloat(mv.extrema.style.left) + edx + "px";
    //on modifie la ligne comme il faut
    if (mv.extrema.id == "extrema_1")
    {
      ligne_calc(bloc, bloc.left + dx, bloc.top + dy, bloc.x2, bloc.y2);
    }
    else
    {
      ligne_calc(bloc, bloc.left, bloc.top, bloc.x2 + dx, bloc.y2 + dy);
    }
    // on modifie le style de la ligne en conséquence
    let sb = rendu_get_superbloc(bloc);
    sb.style.left = bloc.left*100/443 + "%";
    sb.style.top = bloc.top*100/631 + "%";
    sb.style.height = bloc.height*100/631 + "%";
    sb.style.transform = "rotate(" + bloc.rotation + "deg)";
    ligne_create_html(bloc, "");
  }
  else if (mv.pt)
  {
    mv_resize(edx, edy, dx, dy);
  }
  else
  {
    for (let i=0; i<selection.length; i++)
    {
      let bloc = selection[i];
      
      bloc.top = parseFloat(bloc.top) + dy;
      bloc.left = parseFloat(bloc.left) + dx;
      let sb = rendu_get_superbloc(bloc);
      sb.style.top = bloc.top*100/631 + "%";
      sb.style.left = bloc.left*100/443 + "%";
      if (i==0) // on affiche juste les valeurs du premier élément
      {
        document.getElementById("cr_tp_l").value = bloc.left;
        document.getElementById("cr_tp_t").value = bloc.top;
      }
      if (i==0 && bloc.tpe == "ligne")
      {
        let e = document.getElementById("extrema_1");
        e.style.left = parseFloat(e.style.left) + edx + "px";
        e.style.top = parseFloat(e.style.top) + edy + "px";
        e = document.getElementById("extrema_2");
        e.style.left = parseFloat(e.style.left) + edx + "px";
        e.style.top = parseFloat(e.style.top) + edy + "px";
        bloc.x2 += dx;
        bloc.y2 += dy;
      }
    }
    mv_place_pt();
  }
}

function mv_mouseup(event)
{
  if (!mv.on) return;
  event.preventDefault();
  mv.on = false;
  selection_update();
  if (mv.change) g_sauver();
}

function mv_resize(edx, edy, dx, dy)
{
  //attention, il faut que tous les éléments soient redimensionnable !
  let ratio = false;  // si un seul des éléments doit conserver le ration, alors c'est le cas pour tous !
  for(let i=0; i<selection.length; i++)
  {
    if (selection[i].size == "auto" || selection[i].rotation != 0)
    {
      return;
    }
    if (selection[i].size == "ratio") ratio = true;
  }
  //on détermine les pourcentages à appliquer aux dimensions/positions
  let nr = {};
  let rw, rh;
  switch (mv.pt_num)
  {
    case 1:
      rw = Math.max(5, mv.rect.width - dx)/mv.rect.width;
      rh = Math.max(5, mv.rect.height - dy)/mv.rect.height;
      if (ratio)
      {
        if (rh > rw)
        {
          // il faut changer la formule pour left
          nr.left = Math.min(mv.rect.right - 5, mv.rect.left + mv.rect.width*(1.0-rh))/mv.rect.left;
          nr.width = rh;
          nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + dy)/mv.rect.top;
          nr.height = rh;
        }
        else
        {
          // il faut changer la formule pour top
          nr.left = Math.min(mv.rect.right - 5, mv.rect.left + dx)/mv.rect.left;
          nr.width = rw;
          nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + mv.rect.height*(1.0-rw))/mv.rect.top;
          nr.height = rw;
        }
      }
      else
      {
        nr.left = Math.min(mv.rect.right - 5, mv.rect.left + dx)/mv.rect.left;
        nr.width = rw;
        nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + dy)/mv.rect.top;
        nr.height = rh;
      }
       
      break;
    case 2:
      nr.height = Math.max(5, mv.rect.height - dy)/mv.rect.height;
      if (ratio) nr.width = nr.height;
      else nr.width = 1;
      nr.left = 1.0;
      nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + dy)/mv.rect.top;
      break;
    case 3:
      rw = Math.max(5, mv.rect.width + dx)/mv.rect.width;
      rh = Math.max(5, mv.rect.height - dy)/mv.rect.height;
      nr.left = 1.0;
      if (ratio)
      {
        if (rh > rw)
        {
          nr.width = rh;
          nr.height = rh;
          nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + dy)/mv.rect.top;
        }
        else
        {
          // là il faut changer la formule pour top, car c'est le rapport width qui est appliqué...
          nr.width = rw;
          nr.height = rw;
          nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + mv.rect.height*(1.0-rw))/mv.rect.top;
        }
      }
      else
      {
        nr.width = rw;
        nr.height = rh;
        nr.top = Math.min(mv.rect.bottom - 5, mv.rect.top + dy)/mv.rect.top;
      }
      break;
    case 4:
      nr.left = 1.0;
      nr.top = 1.0;
      nr.width = Math.max(5, mv.rect.width + dx)/mv.rect.width;
      if (ratio) nr.height = nr.width;
      else nr.height = 1.0;
      break;
    case 5:
      nr.left = 1.0;
      nr.top = 1.0;
      rw = Math.max(5, mv.rect.width + dx)/mv.rect.width;
      rh = Math.max(5, mv.rect.height + dy)/mv.rect.height;
      if (ratio) rw = Math.max(rw, rh); // on choisi la plus grande des 2 valeurs
      nr.width = rw;
      if (ratio) nr.height = rw;
      else nr.height = rh;
      break;
    case 6:
      nr.left = 1.0;
      nr.top = 1.0;
      nr.height = Math.max(5, mv.rect.height + dy)/mv.rect.height;
      if (ratio) nr.width = nr.height;
      else nr.width = 1.0;
      break;
    case 7:
      rw = Math.max(5, mv.rect.width - dx)/mv.rect.width;
      rh = Math.max(5, mv.rect.height + dy)/mv.rect.height;
      nr.top = 1.0;
      if (ratio)
      {
        if (rw > rh)
        {
          nr.left = Math.min(mv.rect.right - 5, mv.rect.left + dx)/mv.rect.left;
          nr.width = rw;
          nr.height = rw;
        }
        else
        {
          // il faut changer la formule pour left
          nr.left = Math.min(mv.rect.right - 5, mv.rect.left + mv.rect.width*(1.0-rh))/mv.rect.left;
          nr.width = rh;
          nr.height = rh;
        }
      }
      else
      {
        nr.left = Math.min(mv.rect.right - 5, mv.rect.left + dx)/mv.rect.left;
        nr.width = rw;
        nr.height = rh;
      }
      
      break;
    case 8:
      nr.left = Math.min(mv.rect.right - 5, mv.rect.left + dx)/mv.rect.left;
      nr.width = Math.max(5, mv.rect.width - dx)/mv.rect.width;
      nr.top = 1;
      if (ratio) nr.height = nr.width;
      else nr.height = 1;
      break;
  }
  
  // et on applique les modifications aux éléments
  for(let i=0; i<selection.length; i++)
  {
    let bloc = selection[i];
    let sb = rendu_get_superbloc(bloc);
    
    bloc.height *= nr.height;
    bloc.width *= nr.width;
    bloc.left = mv.rect.left*nr.left + nr.width*(bloc.left-mv.rect.left);
    bloc.top = mv.rect.top*nr.top + nr.height*(bloc.top-mv.rect.top);

    //on déplace/redim le bloc
    sb.style.left = bloc.left*100/443 + "%";
    sb.style.top = bloc.top*100/631 + "%";
    sb.style.width = bloc.width*100/443 + "%";
    sb.style.height = bloc.height*100/631 + "%";
  }
  //on remet les points en place
  mv_place_pt();
}

function mv_place_pt()
{
  if (selection.length == 0 || (selection.length == 1 && selection[0].tpe == "ligne"))
  {
    for (let i=1; i<9; i++) document.getElementById("cr_sel" + i).style.display = "none";
    return;
  }
  let mv_ok = true;
  if (selection[0].size == "auto")
  {
    // il faut recalculer les tailles au cas où quelque chose ait changé depuis le chargement
    let e = document.getElementById(selection[0].id);
    selection[0].width = e.offsetWidth*443/rendu.width;
    selection[0].height = e.offsetHeight*631/rendu.height;
    if(selection.length == 1) mv_ok = false;
  }
  //cas particulier des objets tournés en sélection unique
  if (selection.length == 1 && selection[0].rotation != 0)
  {
    let alpha = Math.atan2(selection[0].height, selection[0].width);
    let rot = selection[0].rotation*3.1415/180;
    let ray = 0.5*Math.sqrt(selection[0].height*selection[0].height + selection[0].width*selection[0].width);
    let x = Math.cos(rot+alpha)*ray;
    let y = Math.sin(rot+alpha)*ray;
    let mx = selection[0].left + selection[0].width/2;
    let my = selection[0].top + selection[0].height/2;
    let pt = document.getElementById("cr_sel1");
    pt.style.left = (mx-x)*rendu.width/443 - 4 + "px";
    pt.style.top = (my-y)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel2");
    pt.style.left = (mx-x+(y+x)/2)*rendu.width/443 - 4 + "px";
    pt.style.top = (my-y+(y-x)/2)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel3");
    pt.style.left = (mx+y)*rendu.width/443 - 4 + "px";
    pt.style.top = (my-x)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel4");
    pt.style.left = (mx+y+(x-y)/2)*rendu.width/443 - 4 + "px";
    pt.style.top = (my-x+(x+y)/2)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel5");
    pt.style.left = (mx+x)*rendu.width/443 - 4 + "px";
    pt.style.top = (my+y)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel6");
    pt.style.left = (mx+x-(y+x)/2)*rendu.width/443 - 4 + "px";
    pt.style.top = (my+y+(x-y)/2)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel7");
    pt.style.left = (mx-y)*rendu.width/443 - 4 + "px";
    pt.style.top = (my+x)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    pt = document.getElementById("cr_sel8");
    pt.style.left = (mx-y+(y-x)/2)*rendu.width/443 - 4 + "px";
    pt.style.top = (my+x-(x+y)/2)*rendu.height/631 - 4 + "px";
    pt.style.display = "block";
    mv_pt_cursors(false);
    return;
  }
  
  if (selection[0].rotation != 0 && selection[0].tpe == "ligne")
  {
    let r = document.getElementById(selection[0].id).getBoundingClientRect();
    let rr = document.getElementById("cr_rendu").getBoundingClientRect();
    let w = r.width*443/rendu.width;
    let h = r.height*631/rendu.height;
    let l = (r.left-rr.left-5)*443/rendu.width;
    let t = (r.top-rr.top-5)*631/rendu.height;
    mv.rect.left = l;
    mv.rect.top = t;
    mv.rect.right = l + w;
    mv.rect.bottom = t + h;
  }
  else if (selection[0].rotation != 0)
  {
    let r = document.getElementById(selection[0].id).getBoundingClientRect();
    let w = r.width/2*443/rendu.width;
    let h = r.height/2*631/rendu.height;
    mv.rect.left = selection[0].left + selection[0].width/2 - w;
    mv.rect.top = selection[0].top + selection[0].height/2 - h;
    mv.rect.right = selection[0].left + selection[0].width/2 + w;
    mv.rect.bottom = selection[0].top + selection[0].height/2 + h;
  }
  else
  {
    mv.rect.left = selection[0].left;
    mv.rect.top = selection[0].top;
    mv.rect.right = selection[0].left + selection[0].width;
    mv.rect.bottom = selection[0].top + selection[0].height;
  }
  
  for (let i=1; i<selection.length; i++)
  {
    let b = selection[i];
    if (b.size == "auto")
    {
      // il faut recalculer les tailles au cas où quelque chose ait changé depuis le chargement
      let e = document.getElementById(b.id);
      b.width = e.offsetWidth*443/rendu.width;
      b.height = e.offsetHeight*631/rendu.height;
      mv_ok = false;
    }
    if (b.rotation != 0 && b.tpe == "ligne")
    {
      let r = document.getElementById(b.id).getBoundingClientRect();
      let rr = document.getElementById("cr_rendu").getBoundingClientRect();
      let w = r.width*443/rendu.width;
      let h = r.height*631/rendu.height;
      let l = (r.left-rr.left-5)*443/rendu.width;
      let t = (r.top-rr.top-5)*631/rendu.height;
      mv.rect.left = Math.min(mv.rect.left, l);
      mv.rect.top = Math.min(mv.rect.top, t);
      mv.rect.right = Math.max(mv.rect.right, l + w);
      mv.rect.bottom = Math.max(mv.rect.bottom, t + h);
    }
    else if (b.rotation != 0)
    {
      let r = document.getElementById(b.id).getBoundingClientRect();
      let w = r.width/2*443/rendu.width;
      let h = r.height/2*631/rendu.height;
      mv.rect.left = Math.min(mv.rect.left, b.left + b.width/2 - w);
      mv.rect.top = Math.min(mv.rect.top, b.top + b.height/2 - h);
      mv.rect.right = Math.max(mv.rect.right, b.left + b.width/2 + w);
      mv.rect.bottom = Math.max(mv.rect.bottom, b.top + b.height/2 + h);
    }
    else
    {
      mv.rect.left = Math.min(mv.rect.left, b.left);
      mv.rect.right = Math.max(mv.rect.right, b.left + b.width);
      mv.rect.top = Math.min(mv.rect.top, b.top);
      mv.rect.bottom = Math.max(mv.rect.bottom, b.top + b.height);
    }
  }
  mv.rect.width = mv.rect.right - mv.rect.left;
  mv.rect.height = mv.rect.bottom - mv.rect.top;
  mv.rectr.left = mv.rect.left*rendu.width/443;
  mv.rectr.right = mv.rect.right*rendu.width/443;
  mv.rectr.width = mv.rect.width*rendu.width/443;
  mv.rectr.top = mv.rect.top*rendu.height/631;
  mv.rectr.bottom = mv.rect.bottom*rendu.height/631;
  mv.rectr.height = mv.rect.height*rendu.height/631;
  
  let pt = document.getElementById("cr_sel1");
  pt.style.left = mv.rectr.left - 4 + "px";
  pt.style.top = mv.rectr.top - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel2");
  pt.style.left = mv.rectr.left + mv.rectr.width/2 - 4 + "px";
  pt.style.top = mv.rectr.top - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel3");
  pt.style.left = mv.rectr.right - 4 + "px";
  pt.style.top = mv.rectr.top - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel4");
  pt.style.left = mv.rectr.right - 4 + "px";
  pt.style.top = mv.rectr.top + mv.rectr.height/2 - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel5");
  pt.style.left = mv.rectr.right - 4 + "px";
  pt.style.top = mv.rectr.bottom - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel6");
  pt.style.left = mv.rectr.left + mv.rectr.width/2 - 4 + "px";
  pt.style.top = mv.rectr.bottom - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel7");
  pt.style.left = mv.rectr.left - 4 + "px";
  pt.style.top = mv.rectr.bottom - 4 + "px";
  pt.style.display = "block";
  pt = document.getElementById("cr_sel8");
  pt.style.left = mv.rectr.left - 4 + "px";
  pt.style.top = mv.rectr.top + mv.rectr.height/2 - 4 + "px";
  pt.style.display = "block";
  mv_pt_cursors(mv_ok);
}

function mv_pt_cursors(ok)
{
  if (mv.ok == ok) return;
  mv.ok = ok;
  
  if (ok)
  {
    let pt = document.getElementById("cr_sel1");
    pt.style.cursor = "nw-resize";
    pt = document.getElementById("cr_sel2");
    pt.style.cursor = "ns-resize";
    pt = document.getElementById("cr_sel3");
    pt.style.cursor = "ne-resize";
    pt = document.getElementById("cr_sel4");
    pt.style.cursor = "ew-resize";
    pt = document.getElementById("cr_sel5");
    pt.style.cursor = "se-resize";
    pt = document.getElementById("cr_sel6");
    pt.style.cursor = "ns-resize";
    pt = document.getElementById("cr_sel7");
    pt.style.cursor = "sw-resize";
    pt = document.getElementById("cr_sel8");
    pt.style.cursor = "ew-resize";
  }
  else
  {
    let elems = document.getElementsByClassName('cr_sel_pt');
    for (let i=0; i<elems.length; i++)
    {
      elems[i].style.cursor = "not-allowed";
    }
  }
}
