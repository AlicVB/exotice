 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

function info_change(e, quoi, id)
{
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
    }
  };
  let ligne = "change&user=" + id + "&quoi=" + quoi + "&val=" + e.value;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function supprime(e, id)
{
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else
      {
        //on vire la ligne
        let l = e.parentNode.parentNode;
        l.parentNode.removeChild(l);
      }
    }
  };
  let ligne = "supprime&user=" + id;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function move(e, sens, id)
{
  //on vérifie que le mouvement est possible
  let l = e.parentNode.parentNode;
  if (sens == "up")
  {
    if (!l.previousElementSibling || !l.previousElementSibling.previousElementSibling) return;
  }
  else if (sens == "down")
  {
    if (!l.nextSibling || !l.nextSibling.nextSibling) return;
  }
  else return;
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else
      {
        //on déplace la ligne
        if (sens == "up") l.parentNode.insertBefore(l, l.previousSibling);
        else l.parentNode.insertBefore(l, l.nextSibling.nextSibling);
      }
    }
  };
  let ligne = "move&user=" + id + "&sens=" + sens;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function ajouter()
{
  //on récupère les valeurs
  let nom = document.getElementById("add_nom").value.trim();
  let grpe = document.getElementById("add_grpe").value.trim();
  let coul = document.getElementById("add_coul").value.trim();
  if (nom == "" || grpe == "")
  {
    alert("le nom et le groupe ne doivent pas être vide !");
    return;
  }
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim().startsWith("*") != "") alert(xhr.responseText);
      else if (xhr.responseText.trim() != "")
      {
        let id = xhr.responseText.trim();
        //on crée la nouvelle ligne
        let lg = document.createElement("tr");
        let td = document.createElement("td");
        td.className = "td2";
        let ip = document.createElement("input");
        ip.type = "text";
        ip.size = "15";
        ip.value = nom;
        ip.title = id;
        ip.onchange = function() {info_change(ip, 'nom', id)};
        td.appendChild(ip);
        lg.appendChild(td);
        
        td = document.createElement("td");
        td.className = "td2";
        ip = document.getElementById("add_grpe").cloneNode(true);
        ip.id = "";
        ip.value = grpe;
        ip.title = id;
        ip.onchange = function() {info_change(ip, 'groupe', id)};
        td.appendChild(ip);
        lg.appendChild(td);
        
        td = document.createElement("td");
        td.className = "td2";
        ip = document.createElement("input");
        ip.className = "jscolor {hash:true}";
        ip.type = "text";
        ip.size = "15";
        ip.value = coul;
        ip.title = id;
        ip.onchange = function() {info_change(ip, 'couleur', id)};
        let ic = new jscolor(ip);
        ic.fromString(coul);
        td.appendChild(ip);
        lg.appendChild(td);
        
        td = document.createElement("td");
        td.className = "td3";
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/window-close.svg";
        ip.title = "supprimer l'utilisateur";
        ip.onclick = function() {supprime(ip, id)};
        td.appendChild(ip);
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/go-up.svg";
        ip.title = "monter l'utilisateur";
        ip.onclick = function() {move(ip, 'up', id)};
        td.appendChild(ip);
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/go-down.svg";
        ip.title = "descendre l'utilisateur";
        ip.onclick = function() {move(ip, 'down', id)};
        td.appendChild(ip);
        lg.appendChild(td);
        
        //on l'insère avant la dernière ligne
        let last = document.getElementById("add_nom").parentNode.parentNode;
        last.parentNode.insertBefore(lg, last);
        
        //on vide les cases d'ajout (juste le nom pour que ce soit plus facile d'ajouter plusieurs enfants d'un meme groupe)
        document.getElementById("add_nom").value = "";       
      }
      else alert("erreur lors de l'ajout : id vide");
    }
  };
  let ligne = "ajoute&nom=" + nom + "&groupe=" + grpe + "&couleur=" + coul;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function add_grpe_change(e)
{
  //on cherche la couleur par défaut
  let els = document.querySelectorAll(".table_grpe input[title=" + e.value + "]");
  if (els.length > 0)
  {
    let c = els[0].parentNode.nextSibling.firstChild.value;
    document.getElementById("add_coul").value = c;
    document.getElementById("add_coul").jscolor.fromString(c);
  }
}

function grpe_info_change(e, quoi, id)
{
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else if (quoi == "nom")
      {
        //il faut mettre à jour les listes côté utilisateurs
        let els = document.querySelectorAll(".sel_grpe > option[value=" + id + "]");
        for (let i = 0; i < els.length; i++) els[i].innerHTML = e.value;
      }
    }
  };
  let ligne = "grpe_change&gid=" + id + "&quoi=" + quoi + "&val=" + e.value;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function grpe_supprime(e, id)
{
  //on ne supprime pas le groupe de test !
  if (id == "_TEST_") return;
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else
      {
        //on vire la ligne
        let l = e.parentNode.parentNode;
        l.parentNode.removeChild(l);
        
        //on met à jour les combobox utilisateurs
        let els = document.querySelectorAll(".sel_grpe > option[value=" + id + "]");
        for (let i = 0; i < els.length; i++) els[i].parentNode.removeChild(els[i]);
      }
    }
  };
  let ligne = "grpe_supprime&gid=" + id;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function grpe_move(e, sens, id)
{
  //on vérifie que le mouvement est possible
  let l = e.parentNode.parentNode;
  if (sens == "up")
  {
    if (!l.previousElementSibling || !l.previousElementSibling.previousElementSibling) return;
  }
  else if (sens == "down")
  {
    if (!l.nextSibling || !l.nextSibling.nextSibling) return;
  }
  else return;
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else
      {
        //on déplace la ligne
        if (sens == "up") l.parentNode.insertBefore(l, l.previousSibling);
        else l.parentNode.insertBefore(l, l.nextSibling.nextSibling);
        
        //on met à jour les combobox côté utilisateurs
        let els = document.querySelectorAll(".sel_grpe > option[value=" + id + "]");
        for (let i = 0; i < els.length; i++)
        {
          if (sens == "up") els[i].parentNode.insertBefore(els[i], els[i].previousSibling);
          else els[i].parentNode.insertBefore(els[i], els[i].nextSibling.nextSibling);
        }
      }
    }
  };
  let ligne = "grpe_move&gid=" + id + "&sens=" + sens;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function grpe_ajouter()
{
  //on récupère les valeurs
  let nom = document.getElementById("grpe_add_nom").value.trim();
  let coul = document.getElementById("grpe_add_coul").value.trim();
  if (nom == "")
  {
    alert("le nom du groupe ne doit pas être vide !");
    return;
  }
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim().startsWith("*") != "") alert(xhr.responseText);
      else if (xhr.responseText.trim() != "")
      {
        let id = xhr.responseText.trim();
        //on crée la nouvelle ligne
        let lg = document.createElement("tr");
        let td = document.createElement("td");
        td.className = "td2";
        let ip = document.createElement("input");
        ip.type = "text";
        ip.size = "15";
        ip.value = nom;
        ip.title = id;
        ip.onchange = function() {grpe_info_change(ip, 'nom', id)};
        td.appendChild(ip);
        lg.appendChild(td);
        
        td = document.createElement("td");
        td.className = "td2";
        ip = document.createElement("input");
        ip.className = "jscolor {hash:true}";
        ip.type = "text";
        ip.size = "15";
        ip.value = coul;
        ip.title = id;
        ip.onchange = function() {grpe_info_change(ip, 'couleur', id)};
        let ic = new jscolor(ip);
        ic.fromString(coul);
        td.appendChild(ip);
        lg.appendChild(td);
        
        td = document.createElement("td");
        td.className = "td3";
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/window-close.svg";
        ip.title = "supprimer le groupe";
        ip.onclick = function() {grpe_supprime(ip, id)};
        td.appendChild(ip);
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/go-up.svg";
        ip.title = "monter le groupe";
        ip.onclick = function() {grpe_move(ip, 'up', id)};
        td.appendChild(ip);
        ip = document.createElement("img");
        ip.className = "eimg";
        ip.src = "../icons/go-down.svg";
        ip.title = "descendre le groupe";
        ip.onclick = function() {grpe_move(ip, 'down', id)};
        td.appendChild(ip);
        lg.appendChild(td);
        
        //on l'insère avant la dernière ligne
        let last = document.getElementById("grpe_add_nom").parentNode.parentNode;
        last.parentNode.insertBefore(lg, last);
        
        //on vide les cases d'ajout
        document.getElementById("grpe_add_nom").value = "";
        document.getElementById("grpe_add_coul").value = "#ffffff";
        document.getElementById("grpe_add_coul").jscolor.fromString("#ffffff");
        
        //on met à jour toutes les combobox des utilisateurs
        let els = document.getElementsByClassName("sel_grpe");
        for (let i = 0; i < els.length; i++)
        {
          let op = document.createElement("option");
          op.value = id;
          op.innerHTML = nom;
          els[i].appendChild(op);
        }
      }
      else alert("erreur lors de l'ajout : id vide");
    }
  };
  let ligne = "grpe_ajoute&nom=" + nom + "&couleur=" + coul;
  xhr.open("POST", "users_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
