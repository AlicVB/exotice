<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
  if (file_exists("../TEST"))
  {
    echo "*MODE TEST : acces refuse !";
    exit;
  }
  
  include("../core/maj.php");
  
  if (isset($_POST['maj']))
  {
    MAJ();
    exit;
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- administration</title>
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <link rel="stylesheet" href="admin.css">
  <script type="text/javascript">
    function lance_MAJ()
    {
      //on désactive la page
      document.getElementById("cr_save_bloque").style.display = "flex";
      
      let xhr = new XMLHttpRequest();
       xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
          // on récupère la valeur de retour
          let rep = xhr.responseText;
          //on enlève le bloquage de la page
          document.getElementById("cr_save_bloque").style.display = "none";
          if (rep.startsWith("*"))
          {
            alert(rep.substr(1));
          }
          else
          {
            alert("Mise à jour réussie !");
            location.href = "index.php";
          }
        }
      };
      let ligne = "maj";
      xhr.open("POST", "update.php" , true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.send(ligne);
    }
  </script>
</head>

<body>
  <div id="bandeau">
    <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  </div>
  <div id="btns"><button onclick='lance_MAJ();'>Mettre à jour</button></div>
  
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div>
  <div id="cr_save_bloque">
    <div id="cr_save_centre">
      <img src="../icons/view-refresh.svg" /><br/>
      Mise à jour en cours...
    </div>
  </div>
</body>
</html>
