<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  $test = (file_exists("../TEST"));

  if ($test)
  {
    echo "*action impossible en mode test !";
    exit;
  }

  if (isset($_POST['efface']))
  {
    if (isset($_POST['uid']) && isset($_POST['exo']) && isset($_POST['cat']) && isset($_POST['livre']))
    {
      // on cherche le chemin du livre
      $livre = $_POST['livre'];
      $uid = $_POST['uid'];
      $exo = $_POST['exo'];
      $cat = $_POST['cat'];
      $d = "../livres/$cat/$livre";
      if (file_exists("$d/livre.txt"))
      {
        if (file_exists("../log_exo/$uid/$cat/$livre/$exo.txt")) unlink("../log_exo/$uid/$cat/$livre/$exo.txt");
        if (file_exists("../log_exo/$uid/$cat/$livre/$exo.nb.txt")) unlink("../log_exo/$uid/$cat/$livre/$exo.nb.txt");
      }
      else echo "*livre $cat - $livre introuvable !";
      // on vide le log
      $f = "../log_exo/$uid/$cat/$livre.txt";
      if (file_exists("$f"))
      {
        $lg = explode("\n", file_get_contents($f));
        $tx = "";
        for ($i=0; $i<count($lg); $i++)
        {
          $v = explode("|", $lg[$i]);
          if (count($v)<2 || basename($v[1]) != $exo) $tx .= "$lg[$i]\n";
        }
        file_put_contents($f, $tx);
      }
    }
  }

  if (isset($_POST['log']))
  {
    if (isset($_POST['uid']))
    {
      $uid = $_POST['uid'];
      $ret = array();
      if (file_exists("../livres/liste.txt")) $cats = explode("\n", file_get_contents("../livres/liste.txt"));
      else $cats = array();
      for ($m=0; $m<count($cats); $m++)
      {
        $cat = $cats[$m];
        //si pas de logs pour cette catégorie, on la saute
        if (!file_exists("../log_exo/$uid/$cat")) continue;
        //on ajoute une entrée dans le tableau final
        $rcat = array();
        $rcat['id'] = $cat;
        $rcat['nom'] = $cat;
        $rcat['couleur'] = "#000000";
        if (file_exists("../livres/$cat/cat.txt"))
        {
          $catt = explode("\n", file_get_contents("../livres/$cat/cat.txt"));
          $rcat['nom'] = $catt[0];
          $rcat['couleur'] = $catt[1];
          if ($rcat['couleur'] == "transparent" || $rcat['couleur'] == "#878787") $rcat['couleur'] = "black";
        }
        // on parcoure les livres de la catégorie
        $rlivres = array();
        if (file_exists("../livres/$cat/liste.txt")) $livres = explode("\n", file_get_contents("../livres/$cat/liste.txt"));
        else $livres = array();
        for ($j=0; $j<count($livres); $j++)
        {
          $livre = $livres[$j];
          //on ajoute une entrée dans le tableau
          $rl = array();
          $rl['id'] = $livre;
          $rl['nom'] = $livre;
          $rl['couleur'] = "#000000";
          if (file_exists("../livres/$cat/$livre/livre.txt"))
          {
            $livret = explode("\n", file_get_contents("../livres/$cat/$livre/livre.txt"));
            $rl['nom'] = $livret[0];
            $rl['couleur'] = $livret[1];
            if ($rl['couleur'] == "transparent" || $rl['couleur'] == "#878787") $rl['couleur'] = "black";
            $rl['photo'] = trim($livret[5]);
          }
          
          //echo "<th class=\"lnom\">$livre_txt</th>\n";
          $rexos = array();
          
          // on parcoure tous les exos existants
          $rl['photo_nb'] = 0;
          $rl['photo_ok'] = 0;
          $rl['photo_td'] = 0;
          $exos = explode("\n", file_get_contents("../livres/$cat/$livre/liste.txt"));
          for ($l=0; $l<count($exos); $l++)
          {
            $rex = array();
            $rex['id'] = $exos[$l];
            $rex['nom'] = $exos[$l];
            $rex['date'] = "";
            if (file_exists("../livres/$cat/$livre/exos/$exos[$l]/exo.txt"))
            {
              $exnamet = explode("\n", file_get_contents("../livres/$cat/$livre/exos/$exos[$l]/exo.txt"));
              $rex['nom'] = $exnamet[0];
            }
            //si c'est un photobook, on veut juste les stats globales
            if ($rl['photo'] != "" && trim($exos[$l]) != "")
            {
              $rl['photo_nb'] += 1;
              if (file_exists("../log_exo/$uid/$cat/$livre/$exos[$l].txt"))
              {
                $ll = explode("\n", file_get_contents("../log_exo/$uid/$cat/$livre/$exos[$l].txt"));
                if ($ll[0] = "??") $rl['photo_td'] += 1;
                else if ($ll[0] = "OK") $rl['photo_ok'] += 1;
              }
            }
            else $rexos[] = $rex;
          }
          //et on rempli avec les logs
          if ($rl['photo'] == "")
          {
            if (!file_exists("../log_exo/$uid/$cat/$livre.txt")) continue;
            $exos_logs = explode("\n", file_get_contents("../log_exo/$uid/$cat/$livre.txt"));
            for ($l=0; $l<count($exos_logs); $l++)
            {
              $vals = explode("|", $exos_logs[$l]);
              if (count($vals) > 3)
              {
                $rexid = -1;
                for ($k=0; $k<count($rexos); $k++)
                {
                  if ($rexos[$k]['id'] == $vals[1])
                  {
                    $rexid = $k;
                    break;
                  }
                }
                if ($rexid >=0)
                {
                  //le nombre d'essais
                  $rexos[$rexid]['essais'] = 1;
                  if (file_exists("../log_exo/$uid/$cat/$livre/$vals[1].nb.txt"))
                  {
                    $essais = explode("\n", file_get_contents("../log_exo/$uid/$cat/$livre/$vals[1].nb.txt"));
                    $rexos[$rexid]['essais'] = $essais[0];
                  }
                  //et le score
                  $rexos[$rexid]['score'] = $vals[2];
                  $rexos[$rexid]['total'] = $vals[3];
                  $rexos[$rexid]['date'] = date("d/m/y H:i", $vals[4]);
                }
              }
            }
          }
          //on ajoute la liste des exos au livre
          $rl['exos'] = $rexos;
          //et le livre à la liste des livres
          $rlivres[] = $rl;
        }
        //on ajoute la liste des livres à la catégorie
        $rcat['livres'] = $rlivres;
        //et la catégorie au tableau final
        $ret[] = $rcat;
      }
      //on convertit le tableau dans un truc qu'on peut passer au JavaScript
      //et on l'echo pour qu'il puisse être reçu
      echo json_encode($ret);
    }
  }
?>
