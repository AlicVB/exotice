/*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

//initialisation de tout ce qu'il faut
function start()
{
  //on nettoie l'url
  if (window.location.href.indexOf("?") != -1)
  {
    let pos = window.location.href.indexOf("?");
    history.replaceState('suppr action', document.title, window.location.href.substr(0, pos));
  }
}

//bascule tout montrer/tout cacher
function showall_change(e)
{
  let show = false;
  if (e.value==0)
  {
    e.value=1;
    e.innerHTML = "tout cacher";
    show = true;
  }
  else
  {
    e.value=0;
    e.innerHTML = "tout montrer";
  }

  //on affiche/masque en modifiant tous les checkbox
  let els = document.getElementsByClassName("user");
  for (let i = 0; i < els.length; i++)
  {
    if (show) deplie_user(els[i], true);
    else plie_user(els[i]);
  }
}

//on plie (ou deplie) un utilisateur
function user_click(e, event)
{
  if (!e.hasAttribute("uid") || !e.hasAttribute("pli")) return;

    if (e.getAttribute("pli")=="1") //on déplie
    {
      //on déplie l'utilisateur cliqué
      deplie_user(e, false)
      
      //on replie tous les autres
      if (!event.ctrlKey)
      {
        let els = document.getElementsByClassName("user");
        for (let i = 0; i < els.length; i++)
        {
          if (els[i] == e) continue;
          plie_user(els[i]);
        }
      }
    }
    else //on replie
    {
      //on supprime le div de log si il existe
      plie_user(e);
    }
}

//on affiche les logs détaillés pour un utilisateur
function deplie_user(e, tout)
{
  if (!e.hasAttribute("uid") || !e.hasAttribute("pli")) return;
  if (e.getAttribute("pli")=="0") return;
  
  //on récupère le nom de l'utilisateur
  let uid = e.getAttribute("uid");
  //on récupère le tableau de log
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "")
      {
        console.log(xhr.responseText);
        //on récupère le tableau de logs
        let log = JSON.parse(xhr.responseText);
        //on crée un div qui englobera tout le log en question
        let dv = document.createElement("div");
        dv.id = "dv" + e.id;
        dv.className = "details";
        //on crée toutes les lignes de logs
        for (let i = 0; i < log.length; i++)
        {
          //ligne catégorie
          let cat = log[i];
          let catlb = document.createElement("label");
          catlb.className = "cat";
          catlb.htmlFor = "cb" + e.id + "_" + i;
          catlb.title = "Cliquer pour Afficher/Masquer";
          catlb.style.color = cat['couleur'];
          catlb.innerHTML = cat['nom'];
          dv.appendChild(catlb);
          let catcb = document.createElement("input");
          catcb.type = "checkbox";
          catcb.className = "cb_pli";
          catcb.id = "cb" + e.id + "_" + i;
          if (tout) catcb.checked = "true";
          dv.appendChild(catcb);
          let ldiv = document.createElement("div");

          //on parcoure les livres
          for (let j = 0; j < cat['livres'].length; j++)
          {
            let livre = cat['livres'][j];
            let ldiv2 = document.createElement("div");
            ldiv2.className = "livre";
            let ltb = document.createElement("table");
            ltb.className = "ltable";
            let ltr = document.createElement("tr");
            // le nom du livre
            let lth = document.createElement("th");
            lth.className = "lnom";
            lth.innerHTML = livre['nom'];
            ltr.appendChild(lth);
            // le score (à compléter plus tard)
            let lscore = document.createElement("th");
            lscore.className = "lscore";
            
            let lexos = null;
            if (livre['photo'] == "")
            {
              // les exos
              lexos = document.createElement("th");
              lexos.className = "lexos";
  
              //on parcoure tous les exos
              let s = 0;
              let t = 0;
              for (let k = 0; k < livre['exos'].length; k++)
              {
                let exo = livre['exos'][k];
                //le cadre
                let spex = document.createElement("span");
                spex.className = "exo";
                if (exo['date'] == "") spex.title = exo['nom'];
                else spex.title = exo['nom'] + " : " + exo['essais'] + " essais ; " + exo['date'];
                let p = 100*parseFloat(exo['score'])/parseFloat(exo['total']);
                let coul = "#E7E7E7";
                if (exo['date'] == "") coul = "transparent";
                else if (p<=25) coul = "red";
                else if (p<=50) coul = "#FFA500";
                else if (p<=75) coul = "#00A500";
                spex.style.background = coul;
                //et le score
                let aex = document.createElement("a");
                aex.href = "../core/livre.php?cat=" + cat['id'] + "&livre=" + livre['id'] + "&uid=" + uid + "&exoid=" + exo['id'];
                aex.target = "_blank";
                if (exo['date'] == "") aex.innerHTML = "---";
                else aex.innerHTML = Math.round(exo['score'],2) + "/" + exo['total'];
                spex.appendChild(aex);
                if (exo['date'] != "") s += parseFloat(exo['score']);
                if (exo['date'] != "") t += parseFloat(exo['total']);
                //et la gomme
                if (exo['date'] != "")
                {
                  let igo = document.createElement("img");
                  igo.className = "erimg";
                  igo.src = "../icons/draw-eraser.svg";
                  igo.onclick = function() {efface_exo(igo, uid, cat['id'], livre['id'], exo['id'])};
                  spex.appendChild(igo);
                }
                
                lexos.appendChild(spex);
              }
              let sct = "--";
              if (t > 0) sct = Math.ceil((100*s)/t) + "%";
              lscore.innerHTML = sct;
              lscore.title = s + "/" + t;
            }
            else
            {
              //les stats
              lexos = document.createElement("th");
              lexos.className = "lphoto";
              let aex = document.createElement("a");
              aex.href = "log_photos.php?cat=" + cat['id'] + "&livre=" + livre['id'] + "&uid=" + uid;
              aex.target = "_blank";
              aex.innerHTML = livre['photo_td']+" à corriger - "+livre['photo_ok']+" OK --- sur "+livre['photo_nb'];
              lexos.appendChild(aex);
              let sct = "--";
              if (livre['photo_nb'] > 0) sct = Math.ceil((100*livre['photo_ok'])/livre['photo_nb']) + "%";
              lscore.innerHTML = sct;
              lscore.title = livre['photo_ok'] + "/" + livre['photo_nb'];
            }
            ltr.appendChild(lscore);
            ltr.appendChild(lexos);
            ltb.appendChild(ltr);
            
            ldiv2.appendChild(ltb);
            ldiv.appendChild(ldiv2);
          }
          dv.appendChild(ldiv);
        }
        //on ajoute le div de log sous la ligne utilisateur
        e.parentNode.insertBefore(dv, e.nextSibling);
        e.setAttribute("pli", "0");
      }
    }
  };
  xhr.open("POST", "logs_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send("log&uid=" + uid);
}

// on masque les logs de l'utilisateur (seul la bande avec le nom reste visible)
function plie_user(e)
{
  if (!e.hasAttribute("uid") || !e.hasAttribute("pli")) return;
  let dv = document.getElementById("dv"+e.id);
  if (dv) e.parentNode.removeChild(dv);
  e.setAttribute("pli", "1");
}

// on remet à zéro un exo spécifique (gomme)
function efface_exo(e, uu, cc, ll, ee)
{
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      if (xhr.responseText.trim() != "") alert(xhr.responseText);
      else
      {
        //on modifie le texte de score
        let sc = e.parentNode.firstChild;
        if (sc)
        {
          sc.innerHTML = "---";
          e.parentNode.style.backgroundColor = "transparent";
        }
        //on supprime la gomme
        e.parentNode.removeChild(e);
      }
    }
  };
  let ligne = "efface&uid=" + uu + "&cat=" + cc + "&livre=" + ll + "&exo=" + ee;
  xhr.open("POST", "logs_actions.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
