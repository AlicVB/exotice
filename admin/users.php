<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

include("../core/maj.php");
$test = (file_exists("../TEST"));
  
  // on récupère la liste des utilisateurs
  $fic = "../utilisateurs.txt";
  if (!file_exists($fic)) file_put_contents($fic, "__TEST__|_TEST_|#C6C6C6|__TEST__\n");
  
  //on "lit" le fichier
  $users = array();
  $vv = explode("\n", file_get_contents($fic));
  for ($i=0; $i<count($vv); $i++)
  {
    $vvv = explode("|", $vv[$i]);
    if (count($vvv) == 3)
    {
      $vvv[] = $vvv[0];
    }
    if (count($vvv) == 4)
    {
      $users[] = $vvv;
    }
  }
  
  // on récupère la liste des groupes
  $fic = "../groupes.txt";
  if (!file_exists($fic))
  {
    $groupes = array();
    // on ajoute les groupes qui ne seraient pas dans la liste
    for ($i=0; $i<count($users); $i++)
    {
      $ok = false;
      for ($j=0; $j<count($groupes); $j++)
      {
        if ($groupes[$j][2] == $users[$i][1])
        {
          $ok = true;
          break;
        }
      }
      if (!$ok && $users[$i][1] != "" && $users[$i][1] != "_TEST_") $groupes[] = array($users[$i][1], $users[$i][2], $users[$i][1]);
    }
    
    //on écrit le fichier de groupes
    $txt = "";
    for ($j=0; $j<count($groupes); $j++)
    {
      $txt .= $groupes[$j][0]."|".$groupes[$j][1]."|".$groupes[$j][2]."\n";
    }
    file_put_contents($fic, $txt);
  }
   
  
  //on "lit" le fichier
  $groupes = array();
  $vv = explode("\n", file_get_contents($fic));
  for ($i=0; $i<count($vv); $i++)
  {
    $vvv = explode("|", $vv[$i]);
    if (count($vvv) == 3)
    {
      $groupes[] = $vvv;
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- utilisateurs</title>
  <link rel="stylesheet" href="livre.css">
  <script type="text/javascript" src="../libs/jscolor.min.js"></script>
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <script type="text/javascript" src="users.js"></script>
</head>

<body onload="">
  <div id="bandeau">
    <div id="bandeau2">&nbsp;</div>
    <a href="index.php"><img class="bimg" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg2" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>
  <div class="col">
    <div class="titre">groupes</div>
    <table class="table_grpe">
      <tr class="trt">
        <td>Nom</td>
      </tr>
      <tr>
        <td class="td2"><input type="text" value="_TEST_" size="15" title="groupe pour faire des tests. Les utilisateurs du groupe voient même les livres cachés." disabled /></td>
        <td class="td2"><input class="jscolor {hash:true}" type="text" name="coul" value="#c6c6c6"  disabled /></td>
      </tr>
<?php
    for ($i=0; $i<count($groupes); $i++)
    {
      //on lit les détails de l'exo
      if (count($groupes[$i]) == 3)
      {
        $nom = $groupes[$i][0];
        $coul = $groupes[$i][1];
        $id = $groupes[$i][2];
        // on écrit le bloc qui correspond
        echo "<tr>";
        echo "<td class=\"td2\"><input type=\"text\" name=\"nom\" value=\"$nom\" size=\"15\" title=\"$id\" onchange=\"grpe_info_change(this, 'nom', '$id')\"></td>";
        echo "<td class=\"td2\"><input class=\"jscolor {hash:true}\" type=\"text\" name=\"coul\" value=\"$coul\" onchange=\"grpe_info_change(this, 'couleur', '$id')\"/></td>";
        echo "<td class=\"td3\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer le groupe\" onclick=\"grpe_supprime(this, '$id')\"/>";
        echo "<img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter le groupe\" onclick=\"grpe_move(this, 'up', '$id')\"/>";
        echo "<img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre le groupe\" onclick=\"grpe_move(this, 'down', '$id')\"/>";
        echo "</td></tr>";
      }
    }
?>
      <tr class="trnew">
        <td class="td2"><input type="text" name="nom" value="" size="15" id="grpe_add_nom"></td>
        <td class="td2"><input class="jscolor {hash:true}" type="text" name="coul" value="" id="grpe_add_coul"/></td>
        <td class="td3"><img class="eimg" src="../icons/list-add.svg" title="ajouter le groupe" onclick="grpe_ajouter()"/>
      </tr>
    </table>
  </div>
  <div class="col users">
    <div class="titre">utilisateurs</div>
    <table>
      <tr class="trt">
        <td>Nom</td><td>Groupe</td>
      </tr>
<?php
    for ($i=0; $i<count($users); $i++)
    {
      //on lit les détails de l'exo
      if (count($users[$i]) == 4)
      {
        $nom = $users[$i][0];
        $grpe = $users[$i][1];
        $coul = $users[$i][2];
        $id = $users[$i][3];
        // on écrit le bloc qui correspond
        echo "<tr>";
        echo "<td class=\"td2\"><input type=\"text\" name=\"nom\" value=\"$nom\" size=\"15\" title=\"$id\" onchange=\"info_change(this, 'nom', '$id')\"></td>";
        echo "<td class=\"td2\"><select class=\"sel_grpe\" onchange=\"info_change(this, 'groupe', '$id')\">";
        for ($j=0; $j<count($groupes); $j++)
        {
          if ($groupes[$j][2] == $grpe) echo "<option value=\"".$groupes[$j][2]."\" selected>".$groupes[$j][0]."</option>";
          else echo "<option value=\"".$groupes[$j][2]."\">".$groupes[$j][0]."</option>";
        }
        if ($grpe == "_TEST_") echo "<option value=\"_TEST_\" selected>_TEST_</option>";
        else echo "<option value=\"_TEST_\">_TEST_</option>";
        echo "</select></td>";
        echo "<td class=\"td2\"><input class=\"jscolor {hash:true}\" type=\"text\" name=\"coul\" value=\"$coul\" onchange=\"info_change(this, 'couleur', '$id')\"/></td>";
        echo "<td class=\"td3\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer l'utilisateur\" onclick=\"supprime(this, '$id')\"/>";
        echo "<img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter l'utilisateur\" onclick=\"move(this, 'up', '$id')\"/>";
        echo "<img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre l'utilisateur\" onclick=\"move(this, 'down', '$id')\"/>";
        echo "</td></tr>";
      }
    }
?>
      <tr class="trnew">
        <td class="td2"><input type="text" name="nom" value="" size="15" id="add_nom"></td>
        <td class="td2"><select id="add_grpe" class="sel_grpe" onchange="add_grpe_change(this)">
<?php
            for ($j=0; $j<count($groupes); $j++)
            {
              if ($j == 0) echo "<option value=\"".$groupes[$j][2]."\" selected>".$groupes[$j][0]."</option>";
              else echo "<option value=\"".$groupes[$j][2]."\">".$groupes[$j][0]."</option>";
            }
?>
        </select></td>
        <td class="td2"><input class="jscolor {hash:true}" type="text" name="coul" value="<?php echo $groupes[0][1]; ?>" id="add_coul"/></td>
        <td class="td3"><img class="eimg" src="../icons/list-add.svg" title="ajouter l'utilisateur" onclick="ajouter()"/>
      </tr>
    </table>
  </div>

  <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
