<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  include("../core/maj.php");
  $test = (file_exists("../TEST"));
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- administration</title>
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <link rel="stylesheet" href="admin.css">
  <script type="text/javascript" src="logs.js"></script>
</head>

<body onload="start()">
  <div id="bandeau">
    <div id="bandeau2">&nbsp;</div>
    <a href="index.php"><img class="bimg2" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
    <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  </div>
  <div id="btns"><button value=0 onclick='showall_change(this)'>tout montrer</button></div>
  <div id="logs">
    <?php
      // on affiche un bandeau par utilisateur ayant des logs enregistrés
      if (file_exists("../utilisateurs.txt")) $users = explode("\n", file_get_contents("../utilisateurs.txt"));
      else $users = array();
      $cbi = 0;
      for ($i=0; $i<count($users); $i++)
      {
        $usert = explode("|", $users[$i]);
        if (count($usert) < 4) continue;
        $user = $usert[0];
        if (!file_exists("../log_exo/$usert[3]")) continue;
        echo "<div class=\"ligne user\" id=\"u$cbi\" pli=\"1\" style=\"background-color: $usert[2];\" title=\"cliquer pour afficher/masquer\" uid=\"$usert[3]\" onclick=\"user_click(this, event)\">$user</div>";
        $cbi++;
      }
    ?>
  </div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div>
</body>
</html>
