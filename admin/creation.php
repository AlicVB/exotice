<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

include("../core/maj.php");
$test = (file_exists("../TEST"));

function myglob($dos, $tpe="fic")
{
  $ret = array();
  if (!file_exists($dos)) return $ret;
  
  $dd = opendir("$dos");
  while (($file = readdir($dd)) !== false)
  {
    if ($tpe == "fic" && !is_dir("$dos/$file")) $ret[] = "$dos/$file";
    else if ($tpe == "dos" && is_dir("$dos/$file") && $file != "." && $file != "..") $ret[] = "$dos/$file";
    else if ($tpe == "fic+dos" && $file != "." && $file != "..") $ret[] = "$dos/$file";
  }
  closedir($dd);
  return $ret;
}
function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ($file = readdir($dir)))
    {
      if (( $file != '.' ) && ( $file != '..' ))
      {
        if ( is_dir($src.'/'.$file) )
        {
          recurse_copy($src.'/'.$file, $dst.'/'.$file);
        }
        else
        {
          copy($src.'/'.$file, $dst.'/'.$file);
        }
      }
    }
    closedir($dir);
}
  
  $dos = "../livres";
  if (!$test)
  {
    if (!file_exists("$dos")) mkdir("$dos", 0777, true);
    
    //si la liste n'existe pas, on la crée
    if (!file_exists("$dos/liste.txt"))
    {
      $vv = myglob("$dos" , "dos");
      $txt = "";
      for ($i=0; $i<count($vv); $i++)
      {
        if ($txt != "") $txt .= "\n";
        $txt .= basename($vv[$i]);
      }
      file_put_contents("$dos/liste.txt", $txt);
    }
  }
  else if (!file_exists("$dos") || !file_exists("$dos/liste.txt"))
  {
    echo "mode TEST : création de catégorie impossible !";
    exit;
  }
  
  //on traite d'abord des actions à faire
  if (isset($_GET['action']) && isset($_GET['cat']))
  {
    if (!$test)
    {
      $cat = $_GET['cat'];
  
      switch ($_GET['action'])
      {
        case "copy":
          $n = 1;
          while (file_exists("$dos/$cat"."_".$n)) $n++;
          recurse_copy("$dos/$cat", "$dos/$cat"."_".$n);
          $txt = file_get_contents("$dos/liste.txt");
          if ($txt != "") $txt .= "\n";
          $txt .= $cat."_".$n;
          file_put_contents("$dos/liste.txt", $txt);
          break;
        case "remove":
          if (file_exists("$dos/$cat")) recursiveRemoveDirectory("$dos/$cat");
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $txt = "";
          for ($i=0; $i<count($livres); $i++)
          {
            if ($livres[$i] != $cat)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
          }
          file_put_contents("$dos/liste.txt", $txt);
          break;
        case "up":
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $pos = array_search($cat, $livres);
          if ($pos > 0)
          {
            $txt = "";
            for ($i=0; $i<$pos-1; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $cat;
            $txt .= "\n".$livres[$pos-1];
            for ($i=$pos+1; $i<count($livres); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            file_put_contents("$dos/liste.txt", $txt);
          }
          break;
        case "down":
          $livres = explode("\n", file_get_contents("$dos/liste.txt"));
          $pos = array_search($cat, $livres);
          if ($pos < count($livres)-1)
          {
            $txt = "";
            for ($i=0; $i<$pos; $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            if ($txt != "") $txt .= "\n";
            $txt .= $livres[$pos+1];
            $txt .= "\n".$cat;          
            for ($i=$pos+2; $i<count($livres); $i++)
            {
              if ($txt != "") $txt .= "\n";
              $txt .= $livres[$i];
            }
            file_put_contents("$dos/liste.txt", $txt);
          }
          break;
      }
    }
    else
    {
      echo "mode TEST : impossible d'effectuer l'action : ".$_GET['action']." !";
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- categories</title>
  <link rel="stylesheet" href="livre.css">
  <link rel="shortcut icon" href="../icons/gnome-palm.png" >
  <script type="text/javascript" src="creation.js"></script>
</head>

<body onload="start()">
  <div id="bandeau">
    <div id="bandeau2">&nbsp;</div>
    <a href="index.php"><img class="bimg" src="../icons/edit-find-replace.svg" title="logs des exercices"/></a>
    <a href="creation.php"><img class="bimg2" src="../icons/applications-accessories.svg" title="gestion des livres et exercices"/></a>
    <a href="users.php"><img class="bimg" src="../icons/stock_people.svg" title="gestion des utilisateurs"/></a>
  </div>
  <div class="col users">
    <div class="titre">catégories</div>
    <table>
<?php
    $cats = explode("\n", file_get_contents("$dos/liste.txt"));
    for ($i=0; $i<count($cats); $i++)
    {
      //on lit les détails de l'exo
      if (file_exists("$dos/$cats[$i]/cat.txt"))
      {
        $cat = $cats[$i];
        $et = "";
        $ecoul = "transparent";
        $infos = explode("\n", file_get_contents("$dos/$cat/cat.txt"));
        if (count($infos)>0) $et = $infos[0];
        if (count($infos)>1) $ecoul = $infos[1];
        // on écrit le bloc qui correspond
        echo "<tr style=\"background-color:$ecoul;\">\n";
        echo "<td class=\"td2\"><a class=\"ea\" href=\"cat.php?cat=$cat\"><b>$et</b> (livres/$cat)</a></td>\n";
        echo "<td class=\"td3\"><img class=\"eimg\" src=\"../icons/window-close.svg\" title=\"supprimer la catégorie\" onclick=\"cat_supprime('$cat')\"/>";
        echo "<a href=\"creation.php?cat=$cat&action=copy\"><img class=\"eimg\" src=\"../icons/tab-new.svg\" title=\"dupliquer la catégorie\"/></a>\n";
        echo "<a href=\"creation.php?cat=$cat&action=up\"><img class=\"eimg\" src=\"../icons/go-up.svg\" title=\"monter la catégorie\"/></a>\n";
        echo "<a href=\"creation.php?cat=$cat&action=down\"><img class=\"eimg\" src=\"../icons/go-down.svg\" title=\"descendre la catégorie\"/></a>\n";

        //on regarde la visibilité
        $hide = 0;
        if (file_exists("$dos/$cats[$i]/hide.txt"))
        {
          $vv = explode("\n", file_get_contents("$dos/$cat/hide.txt"));
          for ($j=0; $j<count($vv); $j++)
          {
            if (trim($vv[$j]) == "_ALL_") $hide = 2;
            else if (trim($vv[$j]) != "") $hide = 1;
            else continue;
            break;
          }
        }
        if ($hide == 0) echo "<img value=\"0\" onclick=\"cat_visi_switch(this, '$cat')\" class=\"eimg\" src=\"../icons/show_ok.svg\" title=\"visible pour tous -- cliquer pour masquer à tous\"/>";
        else if ($hide == 1) echo "<img onclick=\"cat_visi_switch(this, '$cat')\" value=\"1\" class=\"eimg\" src=\"../icons/show_semi.svg\" title=\"visible pour certains -- cliquer pour rendre visible à tous\"/>";
        else echo "<img onclick=\"cat_visi_switch(this, '$cat')\" value=\"2\" class=\"eimg\" src=\"../icons/show_no.svg\" title=\"visible pour personne -- cliquer pour rendre visible à tous\"/>";
        
        echo "</td></tr>\n";
      }
    }
?>
    </table>
    <div class="enew"><button onclick="cat_new()">nouvelle catégorie</button></div>
  </div>
  <div class="exotice"><img src="../exotice.svg" /> <span>v <?php echo VERSION() ?></span></div>
  <div class="copyright"><img src="../icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div> 
</body>
</html>
