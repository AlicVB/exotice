<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  //on vérifie directement que les structures sont bien à jour, pour éviter tout problème
  //et on ne redirige vers la page de msie à jour que si on est dans le dossier d'administration !
  if (!test_MAJ())
  {
    $d = basename(getcwd());
    if ($d != "admin")
    {
      echo "Les structures du programme ne sont pas à jour. Se connecter dans la console d'administration pour faire les mises à jour...";
      exit;
    }
    //on redirige vers la page de mise à jour (sauf si on y est déjà !)
    if (basename($_SERVER['PHP_SELF']) != "update.php")
    {
      header('location: update.php');
      exit;
    }
    //on est sur la page de mise à jour... c'est à elle de tout déclencher !
  }
  
  function recursiveRemoveDirectory($directory)
  {
      foreach(glob("{$directory}/*") as $file)
      {
          if(is_dir($file)) { 
              recursiveRemoveDirectory($file);
          } else {
              unlink($file);
          }
      }
      rmdir($directory);
  }

  function _BASE()
  {
    $f = "";
    if (file_exists("./VERSION")) $f = "./";
    else if (file_exists("../VERSION")) $f = "../";
    else if (file_exists("../../VERSION")) $f = "../../";
    else if (file_exists("../../../VERSION")) $f = "../../../";
    else if (file_exists("../../../../VERSION")) $f = "../../../../";
    else if (file_exists("../../../../../VERSION")) $f = "../../../../../";
    return $f;
  }
  function VERSION()
  {
    $f = _BASE();
    if ($f == "") return "??????";
    $f .= "VERSION";
    
    if (file_exists(str_replace("VERSION", "TEST", $f))) $test = "_TEST";
    else $test = "";
    
    $v = explode("\n", file_get_contents("$f"));
    $v1 = (int)($v[0]/100000);
    $v2 = (int)(($v[0]%100000)/1000);
    $v3 = (int)(($v[0]%100000)%1000);
    if (count($v)>1) return $v1.".".$v2.".".$v3."_".$v[1].$test;
    else return $v1.".".$v2.".".$v3.$test;
  }

  //est-ce que les structures générales des dossiers/fichiers sont à jour ???
  function test_MAJ()
  {
    //on récupère le fichier de MAJ
    $d = _BASE();
    if ($d == "") return false;;
    $fv = $d."VERSION_MAJ";
    $fvo = $d."VERSION_MAJ_OLD";
    
    //si le fichier n'existe pas, on en est à la version 0
    if (!file_exists($fv)) file_put_contents($fv, "0");
    $v = intval(file_get_contents($fv));
    
    //on repère la version actuelle (si le fichier n'existe pas, c'est une nouvelle installation, donc à jour)
    if (!file_exists($fvo)) file_put_contents($fvo, $v);
    $vo = intval(file_get_contents($fvo));
    
    return ($v <= $vo);
  }
  
  //mise à jour des structures générales des dossiers/fichiers
  function MAJ()
  {
    //on récupère le fichier de MAJ
    $d = _BASE();
    if ($d == "") return false;;
    $fv = $d."VERSION_MAJ";
    $fvo = $d."VERSION_MAJ_OLD";
    
    //si le fichier n'existe pas, on en est à la version 0
    if (!file_exists($fv)) file_put_contents($fv, "0");
    $v = intval(file_get_contents($fv));
    
    //on repère la version actuelle (si le fichier n'existe pas, c'est une nouvelle installation, donc à jour)
    //if (!file_exists($fvo)) file_put_contents($fvo, $v);
    if (!file_exists($fvo)) file_put_contents($fvo, "0"); //Pour le moment on initialise à zéro pour être sûr que les anciennes versions fassent les mises à jour
    $vo = intval(file_get_contents($fvo));
    
    //on fait les tests et on applique les mises à jour
    while ($vo < $v)
    {
      $ff = "_update_".$vo;
      $vv = $ff();
      
      if ($vv <= $vo)
      {
        echo "*Erreur de mise à jour depuis la version $vo !";
        return;
      }
      $vo = $vv;
      file_put_contents($fvo, $vo);
    }
  }
  
  //fonction spécifiques de mise à jour
  // il doit y avoir une fonction par niveau de version
  function _update_0()
  {
    //on fait tout ce qu'on doit faire
    //là, il s'agit de basculer les logs utilisateur avec leur id plutot que leur nom
    $d = _BASE();
    if (file_exists("../utilisateurs.txt")) $users = explode("\n", file_get_contents("../utilisateurs.txt"));
    else return 1;
    
    //on créé un dossier temporaire
    if (file_exists($d."log_exo_tmp")) recursiveRemoveDirectory($d."log_exo_tmp");
    mkdir($d."log_exo_tmp", 0777, true);
    for ($i=0; $i<count($users); $i++)
    {
      $usert = explode("|", $users[$i]);
      if (count($usert) < 4) continue;
      if (is_dir($d."log_exo/".$usert[0]))
      {
        rename($d."log_exo/".$usert[0], $d."log_exo_tmp/".$usert[3]);
      }
    }
    recursiveRemoveDirectory($d."log_exo");
    rename($d."log_exo_tmp", $d."log_exo");
    
    //on renvoi la nouvelle version de la structure
    return 1;
  }

?>
