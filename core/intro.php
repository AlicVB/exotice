<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  // on vérifie qu'on a bien toutes les valeurs
  if (!isset($_GET["uid"]) || !isset($_GET["cat"]) || !isset($_GET["livre"]))
  {
    echo "l'url de cette page est mal formée...";
    exit;
  }
  
  include("maj.php");
  
  // on récupère les paramètres
  $uid = $_GET["uid"];
  $catid = $_GET["cat"];
  $livreid = $_GET["livre"];

  //on récupère le nom de l'utilisateur et son groupe
  $txt_users = file_get_contents("../utilisateurs.txt");
  $vals = explode("\n", $txt_users);
  $user = $uid;
  $grpe = "";
  for ($i=0; $i<count($vals); $i++)
  {
    $vv = explode("|", $vals[$i]);
    if (count($vv) >=4 && $vv[3] == $uid)
    {
      $user = $vv[0];
      $grpe = $vv[1];
      break;
    }
  }

  // infos sur le livre
  $dos_l = "../livres/$catid/$livreid";
  if (!file_exists("$dos_l/livre.txt"))
  {
    echo "$dos_l/livre.txt<br/>";
    echo "impossible de trouver le livre $livreid dans la catégorie $catid...";
    exit;
  }
  
  // on récupère le titre du livre
  $infos = explode("\n", file_get_contents("$dos_l/livre.txt"));
  if (count($infos) < 4)
  {
    echo "Le fichier $dos_l/livre.txt semble vide ou corrompu...";
    exit;
  }
  $titre_livre = $infos[0];
  
  // les autres infos
  $coul = $infos[1];
  $aut = $infos[2];
  $img = "$dos_l/$infos[3]";
  
  //et les détails
  $details = "";
  for ($i=11; $i<count($infos); $i++)
  {
    if ($infos[$i] != "")
    {
      if ($i > 11) $details .= "<br/>";
      $details .= $infos[$i];
    }    
  }

  // on récupère la liste des exos en excluant ceux cachés
  $exos = array();
  $vals = explode("\n", file_get_contents("$dos_l/liste.txt"));
  for ($i=0; $i<count($vals); $i++)
  {
    if (is_file("../livres/$catid/$livreid/exos/$vals[$i]/exo.txt"))
    {
      if ($grpe != "_TEST_" && file_exists("../livres/$catid/$livreid/exos/$vals[$i]/hide.txt"))
      {
        $vv = explode("\n", file_get_contents("../livres/$catid/$livreid/exos/$vals[$i]/hide.txt"));
        if (in_array("_ALL_", $vv)) continue;
        if (in_array($grpe, $vv)) continue;
      }
      $ex = array();
      $ex['id'] = $vals[$i];
      //et les détails de l'exo
      $txt_exo = file_get_contents("../livres/$catid/$livreid/exos/$vals[$i]/exo.txt");
      $vs = explode("\n", $txt_exo);
      if (count($vs)<11)
      {
        echo "Le fichier livres/$catid/$livreid/exos/$vals[$i]/exo.txt semble vide ou corrompu...";
        continue;
      }
      $ex['nom'] = $vs[0];
      $ex['essais_max'] = intval($vs[9]);
      $ex['essais'] = 0;
      $exos[] = $ex;
    }
  }
  
  //on récupère l'exo en cours si il existe
  if (!is_file("../log_exo/$uid/$catid/$livreid")) mkdir("../log_exo/$uid/$catid/$livreid", 0777, true);
  $exo_c = 0;
  $essais = 0;
  $affiche_corr = 0;
  if (!$test)
  {
    // on recherche la dernière position
    if (is_file("../log_exo/$uid/$catid/$livreid/pos.txt"))
    {
      $infos = explode("\n", file_get_contents("../log_exo/$uid/$catid/$livreid/pos.txt"));
      $exo_c = $infos[0];
      if ($exo_c >= count($exos)) $exo_c=0;
    }
  }
  
  //on récupère les scores et états des différents exos
  if (is_file("../log_exo/$uid/$catid/$livreid.txt"))
  {
    $exos_logs = explode("\n", file_get_contents("../log_exo/$uid/$catid/$livreid.txt"));
    for ($l=0; $l<count($exos_logs); $l++)
    {
      $vals = explode("|", $exos_logs[$l]);
      if (count($vals) > 3)
      {
        $exid = -1;
        for ($k=0; $k<count($exos); $k++)
        {
          if ($exos[$k]['id'] == $vals[1])
          {
            $exid = $k;
            break;
          }
        }
        if ($exid >=0)
        {
          //le nombre d'essais
          $exos[$exid]['essais'] = 1;
          if (file_exists("../log_exo/$uid/$catid/$livreid/$vals[1].nb.txt"))
          {
            $essais = explode("\n", file_get_contents("../log_exo/$uid/$catid/$livreid/$vals[1].nb.txt"));
            $rexos[$exid]['essais'] = $essais[0];
          }
          //et le score
          $exos[$exid]['score'] = $vals[2];
          $exos[$exid]['total'] = $vals[3];
          $exos[$exid]['date'] = date("d/m/y H:i", $vals[4]);
        }
      }
    }
  }

  $tab = 0;
  if(stripos($_SERVER['HTTP_USER_AGENT'],'android') !== false) $tab = 1;
?> 
 
<!DOCTYPE html>
<html>
  <head>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo "$titre_livre" ?></title>
    <script type="text/javascript" src="tab.js"></script>
    <script type="text/javascript" src="livre.js"></script>
    <link rel="shortcut icon" href="../icons/gnome-palm.png" >
    <link rel="stylesheet" href="../fonts/polices.css">
    <?php
      if ($tab) echo "<link rel=\"stylesheet\" href=\"livre_tab.css\">";
      else echo "<link rel=\"stylesheet\" href=\"livre.css\">";
    ?>
  </head>
  <body onresize="intro_img_load()" onload="intro_ini()">
    <div id="c2">
      <div id="titrelivre"><?php echo $titre_livre ?></div>
      <br/><div id="details"><?php echo $details ?></div>
      <div id="introbas">
        <div id="intro_bysa" title="livre créé par <?php echo $aut ?> -- licence Creative Commons CC-BY-SA"><img src="../icons/cc.svg" /><span> <?php echo $aut ?></span></div>
      </div>
      <div id="exitdiv">
        <a href="../sommaire.php?uid=<?php echo $uid ?>"><img id="exitimg" src="../icons/edit-undo.svg" /></a>
        <img onclick="page_print()" id="exitimg" src="../icons/printer.svg" />
      </div>
      <img id="introimg" src="<?php echo $img ?>" onload="intro_img_load()"/>
    </div>
    
    <div id="c1">
      <span sss="20" id="user">Prénom : <?php echo $user ?></span>
      <table id="sommaire">
      <?php
        for ($i=0; $i<count($exos); $i++)
        {
          $exo = $exos[$i];
          $aa = "<a href=\"livre.php?cat=$catid&livre=$livreid&uid=$uid&exoid=".$exo['id']."\" title='".$exo['essais']." essai sur ".$exo['essais_max']."'>";
          
          if (intval($exo['essais']) >= intval($exo['essais_max']) && intval($exo['essais_max']) > 0)
            echo "<tr class='sommaire_finit'>";
          else if (intval($exo['essais']) > 0)
            echo "<tr class='sommaire_cours'>";
          else
            echo "<tr class='sommaire_vide'>";
          
          if ($exo_c == $i) echo "<td >".$aa."<img src='../icons/go-next.svg'/></a></td>";
          else echo "<td>&nbsp;</td>";
          
          echo "<td>".$aa.$exo['nom']."</a></td>";
          if ($exo['essais']>0) echo "<td class='sommaire_score'>".$aa.$exo['score']."/".$exo['total']."</a></td>";
          else echo "<td>&nbsp;</td>";
          
          echo "</tr>";
        }      
      ?>
      </table>
      <div id="basdiv">
        <div id="gotxt">On y va !</div>
        <a href="livre.php?<?php echo "cat=$catid&livre=$livreid&uid=$uid" ?>"><img id="goimg" src="../icons/go-next.svg" /></a>
      </div>
      <img title="v&nbsp;&nbsp;<?php echo VERSION() ?>" id="exotice" src="../exotice.svg" />
    </div>
  </body>
</html>
