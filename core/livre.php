<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  // on vérifie qu'on a bien toutes les valeurs
  if (!isset($_GET["uid"]) || !isset($_GET["cat"]) || !isset($_GET["livre"]))
  {
    echo "l'url de cette page est mal formée...";
    exit;
  }

  include("maj.php");
  $test = (is_file("../TEST"));

  // on récupère les paramètres
  $uid = $_GET["uid"];
  $catid = $_GET["cat"];
  $livreid = $_GET["livre"];

  $dos_l = "../livres/$catid/$livreid";
  if (!is_file("$dos_l/livre.txt"))
  {
    echo "impossible de trouver le livre $livreid dans la catégorie $catid...";
    exit;
  }

  //on récupère le nom de l'utilisateur et son groupe
  $txt_users = file_get_contents("../utilisateurs.txt");
  $vals = explode("\n", $txt_users);
  $user = $uid;
  $grpe = "";
  for ($i=0; $i<count($vals); $i++)
  {
    $vv = explode("|", $vals[$i]);
    if (count($vv) >=4 && $vv[3] == $uid)
    {
      $user = $vv[0];
      $grpe = $vv[1];
      break;
    }
  }
  
  // infos sur le livre
  $infos = explode("\n", file_get_contents("$dos_l/livre.txt"));
  if (count($infos) < 4)
  {
    echo "Le fichier $dos_l/livre.txt semble vide ou corrompu...";
    exit;
  }
  $titre_livre = $infos[0];
  $coul_livre = $infos[1];
  $aut_livre = $infos[2];
  $img_livre = $infos[3];

  // on récupère la liste des exos en excluant ceux cachés
  $exos = array();
  $vals = explode("\n", file_get_contents("$dos_l/liste.txt"));
  for ($i=0; $i<count($vals); $i++)
  {
    if (file_exists("../livres/$catid/$livreid/exos/$vals[$i]/exo.txt"))
    {
      if ($grpe != "_TEST_" && file_exists("../livres/$catid/$livreid/exos/$vals[$i]/hide.txt"))
      {
        $vv = explode("\n", file_get_contents("../livres/$catid/$livreid/exos/$vals[$i]/hide.txt"));
        if (in_array("_ALL_", $vv)) continue;
        if (in_array($grpe, $vv)) continue;
      }
      $exos[] = $vals[$i];
    }
  }

  // on cherche l'exo en cours
  if (!is_file("../log_exo/$uid/$catid/$livreid")) mkdir("../log_exo/$uid/$catid/$livreid", 0777, true);
  $exo = 0;
  $lien_pre = "";
  $lien_next = "";
  $essais = 0;
  $affiche_corr = 0;
  if (isset($_GET["exo"])) // on entre par lu numéro de l'exercice
  {
    $exo = $_GET["exo"];
    // on sauvegarde
    if (!$test) file_put_contents("../log_exo/$uid/$catid/$livreid/pos.txt",$exo);
  }
  else if (isset($_GET["exoid"])) // on entre par le nom de l'exercice
  {
    $exo = array_search($_GET["exoid"], $exos);
    if (false === $exo) $exo = 0;
    // on sauvegarde
    if (!$test) file_put_contents("../log_exo/$uid/$catid/$livreid/pos.txt",$exo);
  }
  else if (!$test)
  {
    // on recherche la dernière position
    if (is_file("../log_exo/$uid/$catid/$livreid/pos.txt"))
    {
      $infos = explode("\n", file_get_contents("../log_exo/$uid/$catid/$livreid/pos.txt"));
      $exo = $infos[0];
      if ($exo >= count($exos)) $exo=0;
    }
    else file_put_contents("../log_exo/$uid/$catid/$livreid/pos.txt",$exo);
  }
  if ($exo > 0)
  {
    $nb = $exo - 1;
    $lien_pre = "livre.php?cat=$catid&livre=$livreid&uid=$uid&exo=$nb";
  }
  if ($exo < count($exos)-1)
  {
    $nb = $exo + 1;
    $lien_next = "livre.php?cat=$catid&livre=$livreid&uid=$uid&exo=$nb";
  }
  else if ($exo == count($exos)-1) $lien_next = "bilan.php?cat=$catid&livre=$livreid&uid=$uid&t=".time();
  //on lit le compteur
  $f = $exos[$exo];
  if (is_file("../log_exo/$uid/$catid/$livreid/$f.nb.txt"))
  {
    $vv = explode("\n", file_get_contents("../log_exo/$uid/$catid/$livreid/$f.nb.txt"));
    $essais = $vv[0];
    if (count($vv)>1) $affiche_corr = $vv[1];
  }
  // on supprime les trucs enregistrés si besoin
  if (isset($_GET["erase"]) && !$test)
  {
    //on augmente le compteur
    $essais++;
    $affiche_corr = 0;
    file_put_contents("../log_exo/$uid/$catid/$livreid/$f.nb.txt", "$essais\n0");
    //on efface tout
    if ($_GET["erase"] == 1 && is_file("../log_exo/$uid/$catid/$livreid/$f.txt")) unlink("../log_exo/$uid/$catid/$livreid/$f.txt");
  }
  
  // on récupère les infos sur l'exercice en cours
  $txt_exo = file_get_contents("../livres/$catid/$livreid/exos/$exos[$exo]/exo.txt");
  $vals = explode("\n", $txt_exo);
  if (count($vals)<11)
  {
    echo "Le fichier livres/$catid/$livreid/exos/$exos[$exo]/exo.txt semble vide ou corrompu...";
    exit;
  }
  $titre_exo = $vals[0];
  $consigne = $vals[1];
  $exo_coul = $vals[10];
  $exo_audio = "";
  if (count($vals)>11 && $vals[11] != "") $exo_audio = "$dos_l/$vals[11]";
  $exo_image = "";
  $exo_image_hover = "0";
  if (count($vals)>13)
  {
    $vv = explode("|", $vals[13]);
    if (count($vv)>1)
    {
      if ($vv[0] != "" && is_file("$dos_l/img/".$vv[0])) $exo_image = "$dos_l/img/".$vv[0];
      else if ($vv[0] == "livre") $exo_image = "$dos_l/$img_livre";
      $exo_image_hover = $vv[1];
    }
  }
  $txt = htmlentities(str_replace("\n", "§", addslashes($txt_exo)), ENT_QUOTES, "UTF-8");

  $tab = 0;
  if(stripos($_SERVER['HTTP_USER_AGENT'],'android') !== false) $tab = 1;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo "$titre_livre -- $titre_exo" ?></title>
    <script type="text/javascript" src="exo.js"></script>
    <script type="text/javascript" src="tab.js"></script>
    <script type="text/javascript" src="livre.js"></script>
    <link rel="shortcut icon" href="../icons/gnome-palm.png" >
    <link rel="stylesheet" href="../fonts/polices.css">
    <?php
      if ($tab) echo "<link rel=\"stylesheet\" href=\"livre_tab.css\">";
      else echo "<link rel=\"stylesheet\" href=\"livre.css\">";
    ?>
    <link rel="stylesheet" href="exo.css">
    <link rel="stylesheet" href="livre_print.css" media="print">
  </head>
  <body onload="livre_ini('<?php echo $uid ?>', '<?php echo $catid ?>', '<?php echo $livreid ?>', '<?php echo $exos[$exo] ?>', '<?php echo $exo ?>', '<?php echo $txt ?>', '<?php echo $essais ?>', '<?php echo $affiche_corr ?>', '<?php echo $tab ?>');" style="background-color: <?php echo $coul_livre ?>;">
    <div id="c2" style="background-color: <?php echo $exo_coul ?>;">
      <div id="titrelivre"><?php echo $titre_livre ?></div>
      <div id="titreexo"><?php echo $titre_exo ?></div>
      <div id="consigne">
        <?php
          if ($exo_audio != "") echo "<img src=\"../icons/audacity.svg\" onclick=\"consigne_play()\"/><audio id=\"consigne_audio\" src=\"$exo_audio\"></audio>";
          echo $consigne;
        ?>
      </div>
      <?php
        if ($exo_image_hover == "0" && is_file($exo_image))
        {
          echo "<div id=\"aideimg\"><img src=\"../icons/help-hint.svg\" onmouseover=\"affiche_aide(true);\" onmouseout=\"affiche_aide(false);\" /></div>";
        }

        if (!$tab)  // en mode normal la correction est dans le bloc de gauche
        {
          echo "<div id=\"corr\">";
          echo "<table id=\"ctable\">";
          echo "  <tr><th colspan=\"3\" id=\"cscore\">Sujets : 10/10 -- Verbes : 10/10</th></tr>";
          echo "  <tr>";
          echo "    <th id=\"cflag\"><img id=\"cflagimg\" src=\"../icons/flag-red.svg\" /></th>";
          echo "    <th id=\"ctxt\">--</th>";
          echo "    <th id=\"cbtn\">";
          echo "      <div id=\"essaisdiv\"></div>";
          echo "      <img id=\"cbtnimg\" src=\"../icons/corrige.svg\" onclick=\"affiche_score(true);\" />";
          echo "    </th>";
          echo "  </tr>";
          echo "</table>";
          echo "</div>";
          echo "<div id=\"bas\">";
              if ($lien_pre != "") echo "<a href=\"$lien_pre\"><img style=\"height: 3vh; vertical-align: bottom;\" src=\"../icons/go-previous.svg\" /></a>\n";
              for ($i=0; $i<count($exos); $i++)
              {
                $j = $i + 1;
                $cl = "page";
                if ($i < $exo) $cl = "pageavant";
                else if ($i == $exo) $cl = "pagecours";
                echo "<a class=\"$cl\" href=\"livre.php?cat=$catid&livre=$livreid&uid=$uid&exo=$i\">$j</a>\n";
              }
              echo "<a class=\"pagebilan\" href=\"#\" onclick=\"document.location.href='bilan.php?cat=$catid&livre=$livreid&uid=$uid&t='+new Date().getTime(); return false;\">B</a>\n";
              if ($lien_next != "") echo "<a href=\"$lien_next\"><img style=\"height: 3vh; vertical-align: bottom;\" src=\"../icons/go-next.svg\" /></a>\n";
          echo "</div>";
        }

      if ($exo_image_hover == "0" && is_file($exo_image)) echo "<img id=\"aide\" src=\"$exo_image\"/>"; ?>
      <div id="exitdiv">
        <a href="../sommaire.php?uid=<?php echo $uid ?>"><img id="exitimg" src="../icons/edit-undo.svg" /></a>
        <img onclick="page_print()" id="exitimg" src="../icons/printer.svg" />
      </div>
    </div>
    <?php if ($exo_image_hover != "0" && is_file($exo_image)) echo "<img id=\"aide\" src=\"$exo_image\"/>"; ?>
    <div id="c1" style="background-color: <?php echo $exo_coul ?>;">
      <span sss="20" id="user">Prénom : <?php echo $user ?></span>
      <?php echo file_get_contents("../livres/$catid/$livreid/exos/$exos[$exo]/exo.inc.txt") ?>
      <?php
        // coins page
        if ($lien_next != "")
        {
          echo "<a href=\"$lien_next\"><img class=\"coin_d\" id=\"coinimg\" src=\"../icons/go-next_coin.svg\" /></a>\n";
        }
      ?>
      <img title="v&nbsp;&nbsp;<?php echo VERSION() ?>" id="exotice" src="../exotice.svg" />
      <div sss="15" id="bysa"><img id="cc_img" src="../icons/cc.svg" /><span> <?php echo $aut_livre ?></span></div>
    </div>
    <?php //on affiche le bloc de correction en dessous en mode tablette
      if ($tab)
      {
        echo "<div id=\"c3\" style=\"background-color: $exo_coul ;\">";
        echo "<div id=\"corr\">";
        echo "<table id=\"ctable\">";
        echo "  <tr><th colspan=\"3\" id=\"cscore\">Sujets : 10/10 -- Verbes : 10/10</th></tr>";
        echo "  <tr>";
        echo "    <th id=\"cflag\"><img id=\"cflagimg\" src=\"../icons/flag-red.svg\" /></th>";
        echo "    <th id=\"ctxt\">--</th>";
        echo "    <th id=\"cbtn\">";
        echo "      <div id=\"essaisdiv\"></div>";
        echo "      <img id=\"cbtnimg\" src=\"../icons/corrige.svg\" onclick=\"affiche_score(true);\" />";
        echo "    </th>";
        echo "  </tr>";
        echo "</table>";
        echo "</div>";
        echo "<div id=\"bas\">";
            if ($lien_pre != "") echo "<a href=\"$lien_pre\"><img style=\"height: 3vh; vertical-align: bottom;\" src=\"../icons/go-previous.svg\" /></a>\n";
            for ($i=0; $i<count($exos); $i++)
            {
              $j = $i + 1;
              $cl = "page";
              if ($i < $exo) $cl = "pageavant";
              else if ($i == $exo) $cl = "pagecours";
              echo "<a class=\"$cl\" href=\"livre.php?cat=$catid&livre=$livreid&uid=$uid&exo=$i\">$j</a>\n";
            }
            echo "<a class=\"pagebilan\" href=\"#\" onclick=\"document.location.href='bilan.php?cat=$catid&livre=$livreid&uid=$uid&t='+new Date().getTime(); return false;\">B</a>\n";
            if ($lien_next != "") echo "<a href=\"$lien_next\"><img style=\"height: 3vh; vertical-align: bottom;\" src=\"../icons/go-next.svg\" /></a>\n";
        echo "</div>";
        echo "</div>";
      }
    ?>
  </body>
</html>
