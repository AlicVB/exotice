<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

  // on vérifie qu'on a bien toutes les valeurs
  if (!isset($_GET["uid"]) || !isset($_GET["cat"]) || !isset($_GET["livre"]))
  {
    echo "l'url de cette page est mal formée...";
    exit;
  }

  include("maj.php");
  $test = (is_file("../TEST"));

  // on récupère les paramètres
  $uid = $_GET["uid"];
  $catid = $_GET["cat"];
  $livreid = $_GET["livre"];

  $dos_l = "../livres/$catid/$livreid";
  if (!is_file("$dos_l/livre.txt"))
  {
    echo "impossible de trouver le livre $livreid dans la catégorie $catid...";
    exit;
  }

  //on récupère le nom de l'utilisateur et son groupe
  $txt_users = file_get_contents("../utilisateurs.txt");
  $vals = explode("\n", $txt_users);
  $user = $uid;
  $grpe = "";
  for ($i=0; $i<count($vals); $i++)
  {
    $vv = explode("|", $vals[$i]);
    if (count($vv) >=4 && $vv[3] == $uid)
    {
      $user = $vv[0];
      $grpe = $vv[1];
      break;
    }
  }
  
  // infos sur le livre
  $infos = explode("\n", file_get_contents("$dos_l/livre.txt"));
  if (count($infos) < 6)
  {
    echo "Le fichier $dos_l/livre.txt semble vide ou corrompu...";
    exit;
  }
  $titre_livre = $infos[0];
  $coul_livre = $infos[1];
  $aut_livre = $infos[2];
  $nb_essais = $infos[5];
  
  $doslog="../log_exo/$uid/$catid/$livreid/";
  if (!is_file($doslog)) mkdir($doslog, 0777, true);
  
  //si il y a des actions à faire, c'est ici
  if (isset($_GET["action"]))
  {
    if ($_GET["action"]=="saveimg" && isset($_POST["image"]))
    {
      $dest = $doslog.$_POST["image"].".jpg";
      if (file_exists($dest)) unlink($dest);
      copy($_FILES['iimg']['tmp_name'], $dest);
      $nb = 1;
      if (file_exists($doslog.$_POST["image"].".txt"))
      {
        $vals = explode("\n", file_get_contents($doslog.$_POST["image"].".txt"));
        $nb = trim($vals[1]);
        if (!is_numeric($nb)) $nb = 0;
        if (trim($vals[0]) == "") $nb++;
      }
      file_put_contents($doslog.$_POST["image"].".txt", "??\n".$nb);
    }
  }

  // on récupère la liste des photos
  $exos = array();
  $vals = explode("\n", file_get_contents("$dos_l/liste.txt"));
  for ($i=0; $i<count($vals); $i++)
  {
    if (trim($vals[$i]) != "") $exos[] = trim($vals[$i]);
  }

  // on cherche l'état des photos
  $etat = array();
  $essais = array();
  
  for ($i=0; $i<count($exos); $i++)
  {
    if (file_exists($doslog.$exos[$i].".txt"))
    {
      $vals = explode("\n", file_get_contents($doslog.$exos[$i].".txt"));
      if (trim($vals[0]) == "OK") $etat[] = 2; //correction
      else if (trim($vals[0]) == "??") $etat[] = 1;
      else $etat[] = 0;
      
      $nb = trim($vals[1]);
      if (!is_numeric($nb)) $nb = 0;
      $essais[] = $nb;
    }
    else
    {
      $etat[] = -1;
      $essais[] = 0;
    }
  }
  
  $tab = 0;
  if(stripos($_SERVER['HTTP_USER_AGENT'],'android') !== false) $tab = 1;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta name="mobile-web-app-capable" content="yes">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo "$titre_livre" ?></title>
    <script type="text/javascript" src="tab.js"></script>
    <script type="text/javascript" src="livre.js"></script>
    <link rel="shortcut icon" href="../icons/gnome-palm.png" >
    <link rel="stylesheet" href="../fonts/polices.css">
    <link rel="stylesheet" href="photo.css">
    <script>
      function iimg_change(e)
      {
        e.form.submit();        
      }
    </script>
  </head>
  <body style="background-color: <?php echo $coul_livre ?>;">
    <div id="photo_c">
      <div id="user">
        <div id="exitdiv">
          <a href="../sommaire.php?uid=<?php echo $uid ?>"><img id="exitimg" src="../icons/edit-undo.svg" /></a>
        </div>
        <img title="v&nbsp;&nbsp;<?php echo VERSION() ?>" id="exotice" src="../exotice.svg" />
        Prénom : <?php echo $user ?>
      </div>
      <div id="titrelivre"><?php echo $titre_livre ?></div>     
      <div id='photo_liste'>
      <?php
        for ($i=0; $i<count($exos); $i++)
        {
          if (file_exists("$dos_l/img/$exos[$i].jpg")) $fimg = $exos[$i].".jpg";
          else if (file_exists("$dos_l/img/$exos[$i].png")) $fimg = $exos[$i].".png";
          else if (file_exists("$dos_l/img/$exos[$i].svg")) $fimg = $exos[$i].".svg";
          else $fimg = "";

          echo "<div class='photo_".$etat[$i]."'>";
          if ($etat[$i] != 2 && ($nb_essais == 0 || $essais[$i] < $nb_essais || $etat[$i] == 1))
          {
            echo "<form id='photo_form_".$i."' class='photo_form' action=\"photo.php?uid=$uid&cat=$catid&livre=$livreid&action=saveimg\" method=\"POST\" enctype=\"multipart/form-data\">";
            echo "<input type='hidden' name='image' value='".$exos[$i]."'/>";
            echo "<label for='iimg".$i."'>";
          }
          //les images
          if ($etat[$i]>=0) echo "<img class='photo_reponse' src=\"".$doslog.$exos[$i].".jpg?dummy=".time()."\" />";
          if ($fimg != "") echo "<img class='photo_modele' src=\"$dos_l/img/".$fimg."\" />";
          else echo "<div class='photo_modele'>$exos[$i]</div>";
          //et le nombre d'essais
          echo "<div class='essais_div' title='$essais[$i] -- $nb_essais'>";
          if ($nb_essais == 0)
          {
            echo "<img src='../icons/penholder.svg' alt='pas de limite' />";
          }
          else
          {
            for ($j=0; $j<$essais[$i]; $j++)
            {
              echo "<img class='essais_gris' src='../icons/draw-freehand.svg' />";
            }
            for ($j=$essais[$i]; $j<$nb_essais; $j++)
            {
              echo "<img src='../icons/draw-freehand.svg' />";
            }
          }
          echo "</div>";
          //les icones par dessus si besoin
          if ($etat[$i] == 2)
          {
            echo "<img class='photo_icone' src='../icons/dialog-apply.svg' />";
          }
          else if ($etat[$i] == 0)
          {
            echo "<img class='photo_icone' src='../icons/window-close.svg' />";
          }
          
          if ($etat[$i] != 2 && ($nb_essais == 0 || $essais[$i] < $nb_essais || $etat[$i] == 1))
          {
            echo "</label>";
            echo "<input type=\"file\" name=\"iimg\" id='iimg".$i."' accept='image/*' capture onchange='iimg_change(this)'/>";
            echo "</form>";
          }
          echo "</div>";
        }      
      ?>
      </div>     
      <div id="bysa"><img id="cc_img" src="../icons/cc.svg" /><span> <?php echo $aut_livre ?></span></div>
    </div>
  </body>
</html>
