 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--
    
    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/

"use strict";

/*
 * principe : on lance un test toutes les secondes. Si la tablette est mise en veille, les tests n'ont plus lieu
 * du coup la différence de temps entre 2 tests en > à 1s. Auquel cas, on retourne à la page d'acceuil
*/

let last = -1;

function _tab_timer()
{
  let d = new Date();
  let t = d.getTime();
  if (t-last >= 1500)
  {
    //on retourne à la page d'acceuil
    if (location.href.indexOf("/core/") == -1) location.href = "./index.php";
    else location.href = "../index.php";
  }
  last = t;
}

function tab_check_refresh()
{
  let d = new Date();
  last = d.getTime();
  setInterval(_tab_timer, 1000);
}
