<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
if (!isset($_GET['uid']))
{
  echo "pas d'utilisateur passe en parametre !";
  exit;
}

  include("core/maj.php");

  $base = "livres";
  if(isset($_GET['base']))
  {
    $base = $_GET['base'];
    $base = "livres/$base";
  }
  $nom_base = "";
  if (file_exists("$base/cat.txt"))
  {
    $nom_base = explode("\n", file_get_contents("$base/cat.txt"));
    $nom_base = $nom_base[0];
  }
  
  //on récupère le nom de l'utilisateur et son groupe
  $uid = $_GET['uid'];
  $txt_users = file_get_contents("utilisateurs.txt");
  $vals = explode("\n", $txt_users);
  $user = $uid;
  $grpe = "";
  for ($i=0; $i<count($vals); $i++)
  {
    $vv = explode("|", $vals[$i]);
    if (count($vv) >=4 && $vv[3] == $uid)
    {
      $user = $vv[0];
      $grpe = $vv[1];
      break;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>exotice -- Sommaire</title>
  <link rel="shortcut icon" href="icons/gnome-palm.png" >
  <link rel="stylesheet" href="fonts/polices.css">
  <link rel="stylesheet" href="sommaire.css">
  <script type="text/javascript" src="core/tab.js"></script>
</head>

<body onload="tab_check_refresh()">

  <div id="titre">Bienvenue <b><?php echo $user;?></b></div>
  <div id="listelivre">Voici la liste des activités <b><?php echo $nom_base;?></b> :</div>
  <table id="table">
    <?php
      // on récupère la liste des sous-dossiers

      $dirs = explode("\n", file_get_contents("$base/liste.txt"));
      for ($i=0; $i<count($dirs); $i++)
      {
        if (trim($dirs[$i]) == "") continue;
        $d = "$base/".$dirs[$i];
        //on vérifie la visibilité (livre ou catégorie)
        if ($grpe != "_TEST_" && file_exists("$d/hide.txt"))
        {
          $vv = explode("\n", file_get_contents("$d/hide.txt"));
          if (in_array("_ALL_", $vv)) continue;
          if (in_array($grpe, $vv)) continue;
        }
        // on regarde si c'est un livre ou une catégorie
        echo "<tr class=\"tligne\">\n";
        if (file_exists("$d/livre.txt"))
        {
          // on récupère le titre du livre
          $infos = explode("\n", file_get_contents("$d/livre.txt"));
          $nom = $infos[0];
          $coul = "transparent";
          if (count($infos)>1) $coul = $infos[1];
          $photobook = false;
          if (count($infos)>5 && trim($infos[5]) != "") $photobook = true;
          // et on construit le lien
          $livre = basename($d);
          $cat = basename(dirname($d));
          if (!$photobook) $lien = "core/intro.php?cat=$cat&livre=$livre&uid=$uid";
          else $lien = "core/photo.php?cat=$cat&livre=$livre&uid=$uid";

          if (!$photobook) echo "<th class=\"tico\"><a href=\"$lien\"><img class=\"ico\" src=\"icons/livre.svg\" /></a></th>\n";
          else echo "<th class=\"tico\"><a href=\"$lien\"><img class=\"ico\" src=\"icons/camera-photo.svg\" /></a></th>\n";
          echo "<th class=\"tnom\"><a class=\"lien\" href=\"$lien\" style=\"background-color:$coul;\">$nom</a></th>\n";
        }
        else if (file_exists("$d/cat.txt"))
        {
          // on affiche la catégorie
          $infos = explode("\n", file_get_contents("$d/cat.txt"));
          $nom = $infos[0];
          $coul = "transparent";
          if (count($infos)>1) $coul = $infos[1];
          $lien = "sommaire.php?uid=$uid&base=".basename($d);
          echo "<th class=\"tico\"><a href=\"$lien\"><img class=\"ico\" src=\"icons/folder.svg\" /></a></th>\n";
          echo "<th class=\"tnom\"><a class=\"lien\" href=\"$lien\" style=\"background-color:$coul;\">$nom</a></th>\n";
        }
        echo "</tr>\n";
      }
    ?>
  </table>
  <div id="gpl"><br/><img id="gplimg" src="icons/gpl-v3-logo-nb.svg" /> © A. RENAUDIN 2016</div>
  <div id="exotice_div"><img id="exotice" src="exotice.svg" /><br/>v <?php echo VERSION() ?></div>
</body>
</html>
